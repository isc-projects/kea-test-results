var topics =
[
    [ "Callout manager library handles", "d7/d96/group__calloutManagerLibraryHandles.html", null ],
    [ "Methods are used by data consumers.", "dd/d00/group__consumer__methods.html", "dd/d00/group__consumer__methods" ],
    [ "Methods are used by data producers.", "d9/d58/group__producer__methods.html", "d9/d58/group__producer__methods" ],
    [ "Methods are used to handle commands.", "df/d05/group__command__methods.html", "df/d05/group__command__methods" ],
    [ "Specifies supported observation types.", "d2/d0c/group__stat__samples.html", "d2/d0c/group__stat__samples" ],
    [ "Specifies where a given MAC/hardware address was", "da/dae/group__hw__sources.html", "da/dae/group__hw__sources" ],
    [ "Storage for supported observations", "d5/dcb/group__samples__storage.html", null ],
    [ "Typedefs for OptionInt class.", "d4/d71/group__option__int__defs.html", "d4/d71/group__option__int__defs" ],
    [ "Typedefs for OptionIntArray class.", "dd/d56/group__option__int__array__defs.html", "dd/d56/group__option__int__array__defs" ],
    [ "memfile backend versions", "df/d8c/group__v4.html", "df/d8c/group__v4" ],
    [ "memfile backend versions", "d0/d51/group__v6.html", "d0/d51/group__v6" ]
];