var ha__service__states_8h =
[
    [ "stateToString", "d0/dbb/ha__service__states_8h.html#adfbd71f13534161eb6ef9c38f230a017", null ],
    [ "stringToState", "d0/dbb/ha__service__states_8h.html#afbb601d6dc88dd9d797792b7966ca741", null ],
    [ "HA_BACKUP_ST", "d0/dbb/ha__service__states_8h.html#ab30a2a0fadeb79b86de367c7e5a302e6", null ],
    [ "HA_COMMUNICATION_RECOVERY_ST", "d0/dbb/ha__service__states_8h.html#a515ec0f9fd0a179fd83dcbf12727bf2b", null ],
    [ "HA_HOT_STANDBY_ST", "d0/dbb/ha__service__states_8h.html#a4b26b6dce7ea88ac8ae65fd25abb33e4", null ],
    [ "HA_IN_MAINTENANCE_ST", "d0/dbb/ha__service__states_8h.html#a725ddfd31cd8d74d921fe4bf951f92ba", null ],
    [ "HA_LOAD_BALANCING_ST", "d0/dbb/ha__service__states_8h.html#a78ff01f9dbbc39173b2dfc384cce19c2", null ],
    [ "HA_PARTNER_DOWN_ST", "d0/dbb/ha__service__states_8h.html#a906d56b09aef445f9416735c16d73e4e", null ],
    [ "HA_PARTNER_IN_MAINTENANCE_ST", "d0/dbb/ha__service__states_8h.html#aa2fd2e2b5457317d79e5ea1e52c54ca2", null ],
    [ "HA_PASSIVE_BACKUP_ST", "d0/dbb/ha__service__states_8h.html#a417d3c04c5d45dc3a18a57d4c087450e", null ],
    [ "HA_READY_ST", "d0/dbb/ha__service__states_8h.html#ae501bd5196b33d673ebd2ef10b89bd2a", null ],
    [ "HA_SYNCING_ST", "d0/dbb/ha__service__states_8h.html#ac7ef1d9433b05c14d878fd7c9ecf113e", null ],
    [ "HA_TERMINATED_ST", "d0/dbb/ha__service__states_8h.html#a62dc830b838a12c95d13d2430daedd31", null ],
    [ "HA_UNAVAILABLE_ST", "d0/dbb/ha__service__states_8h.html#a5b7e5614f92c10589a5e2ecc15150040", null ],
    [ "HA_WAITING_ST", "d0/dbb/ha__service__states_8h.html#ab0cb4675be9b7023a2c73832c032b7fc", null ]
];