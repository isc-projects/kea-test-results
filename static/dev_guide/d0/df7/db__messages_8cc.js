var db__messages_8cc =
[
    [ "CONFIG_BACKENDS_REGISTERED", "d0/df7/db__messages_8cc.html#a265a8ae00ba3d08ab2ae55ec678a8b84", null ],
    [ "DATABASE_INVALID_ACCESS", "d0/df7/db__messages_8cc.html#a4efa8033e4386aa810688deb2b32a474", null ],
    [ "DATABASE_MYSQL_COMMIT", "d0/df7/db__messages_8cc.html#a0e011d14dcbd67a5d2ca1e690d0d7e9b", null ],
    [ "DATABASE_MYSQL_FATAL_ERROR", "d0/df7/db__messages_8cc.html#af543056dd6024fd4b86c4842a4fba345", null ],
    [ "DATABASE_MYSQL_INITIALIZE_SCHEMA", "d0/df7/db__messages_8cc.html#a227e4804e558662c60c35934b42b7ce1", null ],
    [ "DATABASE_MYSQL_ROLLBACK", "d0/df7/db__messages_8cc.html#a8e8ae615412598737881e06c17622245", null ],
    [ "DATABASE_MYSQL_START_TRANSACTION", "d0/df7/db__messages_8cc.html#afb97e54f2551b8e1a710ded5e5ab58db", null ],
    [ "DATABASE_PGSQL_COMMIT", "d0/df7/db__messages_8cc.html#a2e82eff7c4f6d293131e8d198068a3c8", null ],
    [ "DATABASE_PGSQL_CREATE_SAVEPOINT", "d0/df7/db__messages_8cc.html#a4b7ca49659a97027ed8843b552c4c5d3", null ],
    [ "DATABASE_PGSQL_DEALLOC_ERROR", "d0/df7/db__messages_8cc.html#abd28688b6ccfb44ea97841fc45c6ae11", null ],
    [ "DATABASE_PGSQL_FATAL_ERROR", "d0/df7/db__messages_8cc.html#a0ad4e7b91fd9ba4f7bcab38c6b14a8ef", null ],
    [ "DATABASE_PGSQL_INITIALIZE_SCHEMA", "d0/df7/db__messages_8cc.html#a2675dfcfe5909a21cde2b2a76d96395b", null ],
    [ "DATABASE_PGSQL_ROLLBACK", "d0/df7/db__messages_8cc.html#af2d1ae12de75d0b246ad981c84445e02", null ],
    [ "DATABASE_PGSQL_ROLLBACK_SAVEPOINT", "d0/df7/db__messages_8cc.html#a75d8ad297f15c8b85358d49eec2acc6d", null ],
    [ "DATABASE_PGSQL_START_TRANSACTION", "d0/df7/db__messages_8cc.html#adb5452cacc85c54b4926fcf825b09153", null ],
    [ "DATABASE_PGSQL_TCP_USER_TIMEOUT_UNSUPPORTED", "d0/df7/db__messages_8cc.html#aef1f8a2743c4ccf9fc9ccb8297819d10", null ],
    [ "DATABASE_TO_JSON_BOOLEAN_ERROR", "d0/df7/db__messages_8cc.html#af65a781069f1e0a995595be5972d5d89", null ],
    [ "DATABASE_TO_JSON_INTEGER_ERROR", "d0/df7/db__messages_8cc.html#aa03e41340787a6065756e45af860bf7f", null ],
    [ "DATABASE_TO_JSON_UNKNOWN_TYPE_ERROR", "d0/df7/db__messages_8cc.html#a3ebc158791230dfd9cd122bfd1942aec", null ]
];