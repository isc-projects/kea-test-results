var exceptions_2exceptions_8h =
[
    [ "isc::BadValue", "d3/d22/classisc_1_1BadValue.html", "d3/d22/classisc_1_1BadValue" ],
    [ "isc::Exception", "d5/d15/classisc_1_1Exception.html", "d5/d15/classisc_1_1Exception" ],
    [ "isc::InvalidOperation", "d9/d6c/classisc_1_1InvalidOperation.html", "d9/d6c/classisc_1_1InvalidOperation" ],
    [ "isc::InvalidParameter", "d1/db6/classisc_1_1InvalidParameter.html", "d1/db6/classisc_1_1InvalidParameter" ],
    [ "isc::MultiThreadingInvalidOperation", "df/d71/classisc_1_1MultiThreadingInvalidOperation.html", "df/d71/classisc_1_1MultiThreadingInvalidOperation" ],
    [ "isc::NotFound", "da/d58/classisc_1_1NotFound.html", "da/d58/classisc_1_1NotFound" ],
    [ "isc::NotImplemented", "d8/db5/classisc_1_1NotImplemented.html", "d8/db5/classisc_1_1NotImplemented" ],
    [ "isc::OutOfRange", "d5/de8/classisc_1_1OutOfRange.html", "d5/de8/classisc_1_1OutOfRange" ],
    [ "isc::Unexpected", "df/d4e/classisc_1_1Unexpected.html", "df/d4e/classisc_1_1Unexpected" ],
    [ "isc_throw", "d0/d24/exceptions_2exceptions_8h.html#aa9b42cf3182830744aeda5f0524e93b3", null ],
    [ "isc_throw_1", "d0/d24/exceptions_2exceptions_8h.html#aa9fc9926c6e3c29b262c4e8a6e1ea991", null ],
    [ "isc_throw_2", "d0/d24/exceptions_2exceptions_8h.html#a13f34ae8766778ac6c5050220d5a8213", null ],
    [ "isc_throw_3", "d0/d24/exceptions_2exceptions_8h.html#a10fda7c8e711fb643a62c9cfacdd344d", null ],
    [ "isc_throw_4", "d0/d24/exceptions_2exceptions_8h.html#a2bacc5dcb0e4b42fee14c913c259112d", null ]
];