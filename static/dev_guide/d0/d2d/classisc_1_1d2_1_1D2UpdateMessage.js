var classisc_1_1d2_1_1D2UpdateMessage =
[
    [ "Direction", "d0/d2d/classisc_1_1d2_1_1D2UpdateMessage.html#aac98a71b171074dd9799627651cb8022", [
      [ "INBOUND", "d0/d2d/classisc_1_1d2_1_1D2UpdateMessage.html#aac98a71b171074dd9799627651cb8022a51cd29bb779fb47337d2385ecc8a67d6", null ],
      [ "OUTBOUND", "d0/d2d/classisc_1_1d2_1_1D2UpdateMessage.html#aac98a71b171074dd9799627651cb8022a5f62ea69f88ed21e2f1881d528a038b0", null ]
    ] ],
    [ "QRFlag", "d0/d2d/classisc_1_1d2_1_1D2UpdateMessage.html#a22aaf5d09de0e8ab0aaa4f237e98af2d", [
      [ "REQUEST", "d0/d2d/classisc_1_1d2_1_1D2UpdateMessage.html#a22aaf5d09de0e8ab0aaa4f237e98af2da82dcc23cdc8e707a43137a47ca645713", null ],
      [ "RESPONSE", "d0/d2d/classisc_1_1d2_1_1D2UpdateMessage.html#a22aaf5d09de0e8ab0aaa4f237e98af2da4023cfe5ce8662c4ed982f5aed5da279", null ]
    ] ],
    [ "UpdateMsgSection", "d0/d2d/classisc_1_1d2_1_1D2UpdateMessage.html#a18c6b31baf4091b5792181d8caa1cf12", [
      [ "SECTION_ZONE", "d0/d2d/classisc_1_1d2_1_1D2UpdateMessage.html#a18c6b31baf4091b5792181d8caa1cf12a51de764c166230b2ce854760e195b87b", null ],
      [ "SECTION_PREREQUISITE", "d0/d2d/classisc_1_1d2_1_1D2UpdateMessage.html#a18c6b31baf4091b5792181d8caa1cf12a38be995df178af2ac52ba4a11bca3416", null ],
      [ "SECTION_UPDATE", "d0/d2d/classisc_1_1d2_1_1D2UpdateMessage.html#a18c6b31baf4091b5792181d8caa1cf12a142a572cfba9ba734503f7cfe9be04f0", null ],
      [ "SECTION_ADDITIONAL", "d0/d2d/classisc_1_1d2_1_1D2UpdateMessage.html#a18c6b31baf4091b5792181d8caa1cf12a91094b59224faddf371b5c789c442c8d", null ]
    ] ],
    [ "D2UpdateMessage", "d0/d2d/classisc_1_1d2_1_1D2UpdateMessage.html#acb62e5f221e28a4a18c592022999b1ba", null ],
    [ "addRRset", "d0/d2d/classisc_1_1d2_1_1D2UpdateMessage.html#ae61a080fbe36d25e1b4a3b107525b92e", null ],
    [ "beginSection", "d0/d2d/classisc_1_1d2_1_1D2UpdateMessage.html#a476123baac3fd1af1b465b40c0290787", null ],
    [ "endSection", "d0/d2d/classisc_1_1d2_1_1D2UpdateMessage.html#a93d7fb2f8d8702588469c64e77a1021b", null ],
    [ "fromWire", "d0/d2d/classisc_1_1d2_1_1D2UpdateMessage.html#a7fa5c0efc83e2fe8c65e4237ccb72eee", null ],
    [ "getId", "d0/d2d/classisc_1_1d2_1_1D2UpdateMessage.html#adf900fef4cf44708a86401cc2257e91c", null ],
    [ "getQRFlag", "d0/d2d/classisc_1_1d2_1_1D2UpdateMessage.html#abf3424ef48b3c4ff02ac93d8cea6a5c9", null ],
    [ "getRcode", "d0/d2d/classisc_1_1d2_1_1D2UpdateMessage.html#afafabd3147ab221718a6c7e1f8800b20", null ],
    [ "getRRCount", "d0/d2d/classisc_1_1d2_1_1D2UpdateMessage.html#a3262da48b503d591c2f45f2d7e4e4198", null ],
    [ "getZone", "d0/d2d/classisc_1_1d2_1_1D2UpdateMessage.html#a0906eb1a3b91737d846df1d4f6233846", null ],
    [ "setId", "d0/d2d/classisc_1_1d2_1_1D2UpdateMessage.html#a11ed51ea1e1ad7bec56b8e24a1823d25", null ],
    [ "setRcode", "d0/d2d/classisc_1_1d2_1_1D2UpdateMessage.html#ac1e75c1cb41e23c68eabd3ce16c45c31", null ],
    [ "setZone", "d0/d2d/classisc_1_1d2_1_1D2UpdateMessage.html#af7ad565c373ef1f6966a90a00894771b", null ],
    [ "toWire", "d0/d2d/classisc_1_1d2_1_1D2UpdateMessage.html#aa4525c47b1f247dd79d797c2b1029f61", null ]
];