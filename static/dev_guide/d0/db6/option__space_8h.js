var option__space_8h =
[
    [ "isc::dhcp::InvalidOptionSpace", "de/de9/classisc_1_1dhcp_1_1InvalidOptionSpace.html", "de/de9/classisc_1_1dhcp_1_1InvalidOptionSpace" ],
    [ "isc::dhcp::OptionSpace", "d5/da8/classisc_1_1dhcp_1_1OptionSpace.html", "d5/da8/classisc_1_1dhcp_1_1OptionSpace" ],
    [ "isc::dhcp::OptionSpace6", "db/d7e/classisc_1_1dhcp_1_1OptionSpace6.html", "db/d7e/classisc_1_1dhcp_1_1OptionSpace6" ],
    [ "OptionSpaceCollection", "d0/db6/option__space_8h.html#a3ff3730a8adf344f66a7da66aaf97d4c", null ],
    [ "OptionSpacePtr", "d0/db6/option__space_8h.html#a36bf8682e301c4edf771b7a08180404c", null ]
];