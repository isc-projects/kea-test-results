var classisc_1_1http_1_1Url =
[
    [ "Scheme", "d0/d70/classisc_1_1http_1_1Url.html#af73a3671383943fa9e080caeb96467e3", [
      [ "HTTP", "d0/d70/classisc_1_1http_1_1Url.html#af73a3671383943fa9e080caeb96467e3ab270519cda5a569e1af0c3fde9e9b27b", null ],
      [ "HTTPS", "d0/d70/classisc_1_1http_1_1Url.html#af73a3671383943fa9e080caeb96467e3a91980b9d8d005a81c1c60a981f163d33", null ]
    ] ],
    [ "Url", "d0/d70/classisc_1_1http_1_1Url.html#a7a0308050f6b9ce71c240ea3f446bb4d", null ],
    [ "getErrorMessage", "d0/d70/classisc_1_1http_1_1Url.html#a4ae8543307ee27a534377b6a0f5ade70", null ],
    [ "getPath", "d0/d70/classisc_1_1http_1_1Url.html#ab71bfef4a95c7ddf9083e96c4607663a", null ],
    [ "getPort", "d0/d70/classisc_1_1http_1_1Url.html#a5ffd1ad67b72eae6156199d0986dba17", null ],
    [ "getScheme", "d0/d70/classisc_1_1http_1_1Url.html#a6657e5f9c8d4f952cebc526a415b61be", null ],
    [ "getStrippedHostname", "d0/d70/classisc_1_1http_1_1Url.html#a2eda5e951b50761572efee800025b38e", null ],
    [ "isValid", "d0/d70/classisc_1_1http_1_1Url.html#aaf0b8ade09007e3a82e39d1679d55596", null ],
    [ "operator<", "d0/d70/classisc_1_1http_1_1Url.html#a72db665b1c5d545438a2d207fb308b49", null ],
    [ "rawUrl", "d0/d70/classisc_1_1http_1_1Url.html#aee0d6350de8bb6610390d86999304602", null ],
    [ "toText", "d0/d70/classisc_1_1http_1_1Url.html#a11d58f040d9e6bae6ac3eada0092960b", null ]
];