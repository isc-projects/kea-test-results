var iterative__allocation__state_8h =
[
    [ "isc::dhcp::PoolIterativeAllocationState", "da/de9/classisc_1_1dhcp_1_1PoolIterativeAllocationState.html", "da/de9/classisc_1_1dhcp_1_1PoolIterativeAllocationState" ],
    [ "isc::dhcp::SubnetIterativeAllocationState", "d0/d45/classisc_1_1dhcp_1_1SubnetIterativeAllocationState.html", "d0/d45/classisc_1_1dhcp_1_1SubnetIterativeAllocationState" ],
    [ "PoolIterativeAllocationStatePtr", "d0/de5/iterative__allocation__state_8h.html#a96d37bafd835d4af49490a56d38217e1", null ],
    [ "SubnetIterativeAllocationStatePtr", "d0/de5/iterative__allocation__state_8h.html#aa5b9d73b51ccebda456aea98518f003a", null ]
];