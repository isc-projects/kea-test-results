var callout__handle_8h =
[
    [ "isc::hooks::CalloutHandle", "d7/d9b/classisc_1_1hooks_1_1CalloutHandle.html", "d7/d9b/classisc_1_1hooks_1_1CalloutHandle" ],
    [ "isc::hooks::NoSuchArgument", "db/dee/classisc_1_1hooks_1_1NoSuchArgument.html", "db/dee/classisc_1_1hooks_1_1NoSuchArgument" ],
    [ "isc::hooks::NoSuchCalloutContext", "d3/dbd/classisc_1_1hooks_1_1NoSuchCalloutContext.html", "d3/dbd/classisc_1_1hooks_1_1NoSuchCalloutContext" ],
    [ "isc::hooks::ScopedCalloutHandleState", "da/d87/classisc_1_1hooks_1_1ScopedCalloutHandleState.html", "da/d87/classisc_1_1hooks_1_1ScopedCalloutHandleState" ],
    [ "CalloutHandlePtr", "d0/d60/callout__handle_8h.html#a8daec5c473b714625a7afe8caaa7b0cb", null ]
];