var classisc_1_1ha_1_1CommunicationState4 =
[
    [ "ConnectingClient4", "d3/d7f/structisc_1_1ha_1_1CommunicationState4_1_1ConnectingClient4.html", "d3/d7f/structisc_1_1ha_1_1CommunicationState4_1_1ConnectingClient4" ],
    [ "RejectedClient4", "db/dc8/structisc_1_1ha_1_1CommunicationState4_1_1RejectedClient4.html", "db/dc8/structisc_1_1ha_1_1CommunicationState4_1_1RejectedClient4" ],
    [ "ClientIdent4", "d0/d05/classisc_1_1ha_1_1CommunicationState4.html#a9d3e9e612119ca9658e34d1f902796e2", null ],
    [ "ConnectingClients4", "d0/d05/classisc_1_1ha_1_1CommunicationState4.html#a3aab6d65fe9652ba07b6678410b42a91", null ],
    [ "RejectedClients4", "d0/d05/classisc_1_1ha_1_1CommunicationState4.html#a086fc0d36b4a8bb862cd19328b72d09f", null ],
    [ "CommunicationState4", "d0/d05/classisc_1_1ha_1_1CommunicationState4.html#ae82ae7fde1e3af997359f32fbcd4894f", null ],
    [ "analyzeMessage", "d0/d05/classisc_1_1ha_1_1CommunicationState4.html#a9433d6393c7aa07d7e0053f761391886", null ],
    [ "analyzeMessageInternal", "d0/d05/classisc_1_1ha_1_1CommunicationState4.html#a6ab0fbf1b2d1364e1baeeffdebbfb54f", null ],
    [ "clearConnectingClients", "d0/d05/classisc_1_1ha_1_1CommunicationState4.html#abb50c53c0e9b7d8b3f79511516f65b25", null ],
    [ "clearRejectedLeaseUpdatesInternal", "d0/d05/classisc_1_1ha_1_1CommunicationState4.html#a69f0af97e13db79bec2e2068d79268b6", null ],
    [ "failureDetected", "d0/d05/classisc_1_1ha_1_1CommunicationState4.html#ab83df197a88c689014676718fe936eef", null ],
    [ "failureDetectedInternal", "d0/d05/classisc_1_1ha_1_1CommunicationState4.html#a9d249199270a2777b72a7255ac449257", null ],
    [ "getConnectingClientsCount", "d0/d05/classisc_1_1ha_1_1CommunicationState4.html#a8c5bf94b19cc038abc7a6792e50893f7", null ],
    [ "getRejectedLeaseUpdatesCountInternal", "d0/d05/classisc_1_1ha_1_1CommunicationState4.html#a38ec0f0bc8fea87b4d486b08b1d3bb1b", null ],
    [ "getUnackedClientsCount", "d0/d05/classisc_1_1ha_1_1CommunicationState4.html#a67b29c3f91b4c9024fef0206102a43e5", null ],
    [ "reportRejectedLeaseUpdateInternal", "d0/d05/classisc_1_1ha_1_1CommunicationState4.html#a1cab2866b6dc5a6b06edcda8dc3c692d", null ],
    [ "reportSuccessfulLeaseUpdateInternal", "d0/d05/classisc_1_1ha_1_1CommunicationState4.html#a4a1ea355963e93e7aeffbc7720398861", null ],
    [ "connecting_clients_", "d0/d05/classisc_1_1ha_1_1CommunicationState4.html#ab0d27d4cce75db9830a20c7836581bc8", null ],
    [ "rejected_clients_", "d0/d05/classisc_1_1ha_1_1CommunicationState4.html#a9ab49e2700a325546a2789a2b773c53d", null ]
];