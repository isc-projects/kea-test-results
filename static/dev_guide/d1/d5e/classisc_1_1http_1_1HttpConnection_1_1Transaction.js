var classisc_1_1http_1_1HttpConnection_1_1Transaction =
[
    [ "Transaction", "d1/d5e/classisc_1_1http_1_1HttpConnection_1_1Transaction.html#a2f5131b31278c0cdab5093b44e31abe5", null ],
    [ "consumeOutputBuf", "d1/d5e/classisc_1_1http_1_1HttpConnection_1_1Transaction.html#abbd705cfc0efdaa9500322515030fddc", null ],
    [ "getInputBufData", "d1/d5e/classisc_1_1http_1_1HttpConnection_1_1Transaction.html#ab249d95312732bca217c72708aca045b", null ],
    [ "getInputBufSize", "d1/d5e/classisc_1_1http_1_1HttpConnection_1_1Transaction.html#a892118f3285293beb585f18f2b7c95d6", null ],
    [ "getOutputBufData", "d1/d5e/classisc_1_1http_1_1HttpConnection_1_1Transaction.html#ab6d744cf24146f0c679963cf73c81c40", null ],
    [ "getOutputBufSize", "d1/d5e/classisc_1_1http_1_1HttpConnection_1_1Transaction.html#ac16c3ebfe23a396a5555cb33f9bde44a", null ],
    [ "getParser", "d1/d5e/classisc_1_1http_1_1HttpConnection_1_1Transaction.html#aef3ae5bf69ff999ae7957cd95fe30161", null ],
    [ "getRequest", "d1/d5e/classisc_1_1http_1_1HttpConnection_1_1Transaction.html#a293d3eae4ad6102fb8348ac510bc0b42", null ],
    [ "outputDataAvail", "d1/d5e/classisc_1_1http_1_1HttpConnection_1_1Transaction.html#a3d7c6341602b0c2d68c1bf03f0e27a83", null ],
    [ "setOutputBuf", "d1/d5e/classisc_1_1http_1_1HttpConnection_1_1Transaction.html#a0a6bafce1308e4aee38160424e000529", null ]
];