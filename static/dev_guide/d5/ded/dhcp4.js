var dhcp4 =
[
    [ "Configuration Parser in DHCPv4", "d5/ded/dhcp4.html#dhcpv4ConfigParser", null ],
    [ "Configuration Parser for DHCPv4 (bison)", "d5/ded/dhcp4.html#dhcpv4ConfigParserBison", null ],
    [ "Parsing Partial Configuration in DHCPv4", "d5/ded/dhcp4.html#dhcpv4ConfigSubParser", null ],
    [ "Config File Includes", "d5/ded/dhcp4.html#dhcp4ParserIncludes", null ],
    [ "Avoiding syntactical conflicts in parsers", "d5/ded/dhcp4.html#dhcp4ParserConflicts", null ],
    [ "DHCPv4 configuration inheritance", "d5/ded/dhcp4.html#dhcpv4ConfigInherit", null ],
    [ "Custom functions to parse message options", "d5/ded/dhcp4.html#dhcpv4OptionsParse", null ],
    [ "DHCPv4 Server Support for the Dynamic DNS Updates", "d5/ded/dhcp4.html#dhcpv4DDNSIntegration", null ],
    [ "DHCPv4 Client Classification", "d5/ded/dhcp4.html#dhcpv4Classifier", [
      [ "Simple Client Classification in DHCPv4", "d5/ded/dhcp4.html#dhcpv4ClassifierSimple", null ],
      [ "Full Client Classification in DHCPv4", "d5/ded/dhcp4.html#dhcpv4ClassifierFull", null ],
      [ "How client classification information is used in DHCPv4", "d5/ded/dhcp4.html#dhcpv4ClassifierUsage", null ]
    ] ],
    [ "Configuration backend for DHCPv4", "d5/ded/dhcp4.html#dhcpv4ConfigBackend", null ],
    [ "Reconfiguring DHCPv4 server with SIGHUP signal", "d5/ded/dhcp4.html#dhcpv4SignalBasedReconfiguration", null ],
    [ "Other DHCPv4 topics", "d5/ded/dhcp4.html#dhcpv4Other", null ],
    [ "DHCPv4-over-DHCPv6 DHCPv4 Server Side", "dc/db8/dhcpv4o6Dhcp4.html", [
      [ "DHCPv6-to-DHCPv4 Inter Process Communication", "dc/db8/dhcpv4o6Dhcp4.html#Dhcp4to6Ipc", null ],
      [ "DHCPv4-over-DHCPv6 Packet Processing", "dc/db8/dhcpv4o6Dhcp4.html#dhcp4to6Receive", null ],
      [ "Modified DHCPv4 Routines", "dc/db8/dhcpv4o6Dhcp4.html#dhcp4to6Specific", null ]
    ] ]
];