var db__log_8h =
[
    [ "isc::db::DB_LOG< log_type >", "dc/d32/structisc_1_1db_1_1DB__LOG.html", "dc/d32/structisc_1_1db_1_1DB__LOG" ],
    [ "isc::db::DB_LOG_DEBUG", "d0/d48/structisc_1_1db_1_1DB__LOG__DEBUG.html", "d0/d48/structisc_1_1db_1_1DB__LOG__DEBUG" ],
    [ "isc::db::DB_LOG_ERROR", "d6/dc5/structisc_1_1db_1_1DB__LOG__ERROR.html", "d6/dc5/structisc_1_1db_1_1DB__LOG__ERROR" ],
    [ "isc::db::DB_LOG_FATAL", "dc/dfd/structisc_1_1db_1_1DB__LOG__FATAL.html", "dc/dfd/structisc_1_1db_1_1DB__LOG__FATAL" ],
    [ "isc::db::DB_LOG_INFO", "da/d52/structisc_1_1db_1_1DB__LOG__INFO.html", "da/d52/structisc_1_1db_1_1DB__LOG__INFO" ],
    [ "isc::db::DB_LOG_WARN", "d4/de0/structisc_1_1db_1_1DB__LOG__WARN.html", "d4/de0/structisc_1_1db_1_1DB__LOG__WARN" ],
    [ "isc::db::DbLogger", "df/dc1/classisc_1_1db_1_1DbLogger.html", "df/dc1/classisc_1_1db_1_1DbLogger" ],
    [ "DbLoggerStack", "d5/d35/db__log_8h.html#a1c65cccec71e67743d0049e0e5de1569", null ],
    [ "DbMessageID", "d5/d35/db__log_8h.html#a2c3a15b100e123134e558b3f2c73dc8b", [
      [ "DB_INVALID_ACCESS", "d5/d35/db__log_8h.html#a2c3a15b100e123134e558b3f2c73dc8ba0013ed55293a029f07780b3e2e9e3eb3", null ],
      [ "PGSQL_INITIALIZE_SCHEMA", "d5/d35/db__log_8h.html#a2c3a15b100e123134e558b3f2c73dc8badb56ff0078f9a513d73b2316476d6a66", null ],
      [ "PGSQL_DEALLOC_ERROR", "d5/d35/db__log_8h.html#a2c3a15b100e123134e558b3f2c73dc8badf49dfc57059dfccb186146d4960c5ee", null ],
      [ "PGSQL_FATAL_ERROR", "d5/d35/db__log_8h.html#a2c3a15b100e123134e558b3f2c73dc8bab6aa830619ad88caef3e5cda792156d5", null ],
      [ "PGSQL_START_TRANSACTION", "d5/d35/db__log_8h.html#a2c3a15b100e123134e558b3f2c73dc8ba73dd675802f68bdd1971bd000878e003", null ],
      [ "PGSQL_COMMIT", "d5/d35/db__log_8h.html#a2c3a15b100e123134e558b3f2c73dc8ba69ddf265d443c86978422612125697fa", null ],
      [ "PGSQL_ROLLBACK", "d5/d35/db__log_8h.html#a2c3a15b100e123134e558b3f2c73dc8ba3812b237336d39ce2a8713ed2c72c173", null ],
      [ "PGSQL_CREATE_SAVEPOINT", "d5/d35/db__log_8h.html#a2c3a15b100e123134e558b3f2c73dc8ba34c6041c145f880bb91ff3d08ac2af1c", null ],
      [ "PGSQL_ROLLBACK_SAVEPOINT", "d5/d35/db__log_8h.html#a2c3a15b100e123134e558b3f2c73dc8ba2ee861e5625d6c103c1a32caf2e00e71", null ],
      [ "PGSQL_TCP_USER_TIMEOUT_UNSUPPORTED", "d5/d35/db__log_8h.html#a2c3a15b100e123134e558b3f2c73dc8ba50134ee616496d8c702c42af80034375", null ],
      [ "MYSQL_INITIALIZE_SCHEMA", "d5/d35/db__log_8h.html#a2c3a15b100e123134e558b3f2c73dc8ba41e7970074db1789ac235f3c1f3be543", null ],
      [ "MYSQL_FATAL_ERROR", "d5/d35/db__log_8h.html#a2c3a15b100e123134e558b3f2c73dc8baecf3fe21221b5c9d320799601a7edfc6", null ],
      [ "MYSQL_START_TRANSACTION", "d5/d35/db__log_8h.html#a2c3a15b100e123134e558b3f2c73dc8ba135cf7f4900c3454f2779a315b0235bb", null ],
      [ "MYSQL_COMMIT", "d5/d35/db__log_8h.html#a2c3a15b100e123134e558b3f2c73dc8baf5f0f7e4f825a61a3a61fbf2ead2a612", null ],
      [ "MYSQL_ROLLBACK", "d5/d35/db__log_8h.html#a2c3a15b100e123134e558b3f2c73dc8bab8356865ffa4bc8571000039f76aa8a2", null ]
    ] ],
    [ "log_type_t", "d5/d35/db__log_8h.html#a6fa63e437108eb1d729f1e4af07ede2a", [
      [ "fatal", "d5/d35/db__log_8h.html#a6fa63e437108eb1d729f1e4af07ede2aa1fa67c9b529dc3f8789f724f5c780c21", null ],
      [ "error", "d5/d35/db__log_8h.html#a6fa63e437108eb1d729f1e4af07ede2aaae9b4281db9c559a24ab4e1b3a7664c1", null ],
      [ "warn", "d5/d35/db__log_8h.html#a6fa63e437108eb1d729f1e4af07ede2aac4709fe41c91da5d9f6a5b102f19e7f6", null ],
      [ "info", "d5/d35/db__log_8h.html#a6fa63e437108eb1d729f1e4af07ede2aa38336d354bbd209a30db9619d56946d3", null ],
      [ "debug", "d5/d35/db__log_8h.html#a6fa63e437108eb1d729f1e4af07ede2aa5d215ed41626a31ca956bc1164f4a358", null ]
    ] ],
    [ "checkDbLoggerStack", "d5/d35/db__log_8h.html#ab973435c5ff3569c774d9bcd9752f01c", null ]
];