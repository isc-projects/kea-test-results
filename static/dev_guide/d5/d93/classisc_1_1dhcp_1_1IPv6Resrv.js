var classisc_1_1dhcp_1_1IPv6Resrv =
[
    [ "Type", "d5/d93/classisc_1_1dhcp_1_1IPv6Resrv.html#a8ba9025acbc2209c8deb030334530b46", [
      [ "TYPE_NA", "d5/d93/classisc_1_1dhcp_1_1IPv6Resrv.html#a8ba9025acbc2209c8deb030334530b46a87057a88247e32156ee5d77a09e8a03a", null ],
      [ "TYPE_PD", "d5/d93/classisc_1_1dhcp_1_1IPv6Resrv.html#a8ba9025acbc2209c8deb030334530b46a0ccc7309ce0f516a4d4c533ab59bcacc", null ]
    ] ],
    [ "IPv6Resrv", "d5/d93/classisc_1_1dhcp_1_1IPv6Resrv.html#aac68d1cfa172b55061ebcf69865ed6d2", null ],
    [ "getPDExclude", "d5/d93/classisc_1_1dhcp_1_1IPv6Resrv.html#ac6042609498f077e334d1e5be6e85bb9", null ],
    [ "getPrefix", "d5/d93/classisc_1_1dhcp_1_1IPv6Resrv.html#a19d098150fef82ad50922d96da820001", null ],
    [ "getPrefixLen", "d5/d93/classisc_1_1dhcp_1_1IPv6Resrv.html#a8e130d016821882e6180ec2875d165ca", null ],
    [ "getType", "d5/d93/classisc_1_1dhcp_1_1IPv6Resrv.html#a46539b8262172121e34cc4b8d202b90e", null ],
    [ "operator!=", "d5/d93/classisc_1_1dhcp_1_1IPv6Resrv.html#af35a477e034bc19b74efeb3b5edbaffc", null ],
    [ "operator==", "d5/d93/classisc_1_1dhcp_1_1IPv6Resrv.html#acdb4d2f88036b9386b4d0cf403d4d778", null ],
    [ "PDExcludetoText", "d5/d93/classisc_1_1dhcp_1_1IPv6Resrv.html#a458d9bb507383e4f01be57522b6ce40a", null ],
    [ "set", "d5/d93/classisc_1_1dhcp_1_1IPv6Resrv.html#a0d184c4a337d9082fa76acba31dae843", null ],
    [ "setPDExclude", "d5/d93/classisc_1_1dhcp_1_1IPv6Resrv.html#a8e0a3e21d51cc3a715c5371371bd37bb", null ],
    [ "toText", "d5/d93/classisc_1_1dhcp_1_1IPv6Resrv.html#ad469843dff84cd9474387242ae076a0f", null ]
];