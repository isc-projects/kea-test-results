var structisc_1_1config_1_1UnixSocketInfo =
[
    [ "UnixSocketInfo", "d9/d74/structisc_1_1config_1_1UnixSocketInfo.html#ac01aa02a8d259718258ddef4faea1b16", null ],
    [ "~UnixSocketInfo", "d9/d74/structisc_1_1config_1_1UnixSocketInfo.html#a4db4d70da2ea27795d37429f55eed063", null ],
    [ "acceptor_", "d9/d74/structisc_1_1config_1_1UnixSocketInfo.html#a39ef1bbcf3364b104d458df437be46b1", null ],
    [ "config_", "d9/d74/structisc_1_1config_1_1UnixSocketInfo.html#a3adf984901663b5a24d84094020ac2de", null ],
    [ "lock_fd_", "d9/d74/structisc_1_1config_1_1UnixSocketInfo.html#a8b87da80b824b9cbd490644d2bbd93b8", null ],
    [ "socket_", "d9/d74/structisc_1_1config_1_1UnixSocketInfo.html#a3e6b60f7c37e2e10e0b7eafc761318b8", null ],
    [ "usable_", "d9/d74/structisc_1_1config_1_1UnixSocketInfo.html#aca98873f7fdb8d7c57f1441c11a3231d", null ]
];