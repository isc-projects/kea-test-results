var duid_8h =
[
    [ "isc::dhcp::ClientId", "d2/d03/classisc_1_1dhcp_1_1ClientId.html", "d2/d03/classisc_1_1dhcp_1_1ClientId" ],
    [ "isc::dhcp::DUID", "d3/d92/classisc_1_1dhcp_1_1DUID.html", "d3/d92/classisc_1_1dhcp_1_1DUID" ],
    [ "isc::dhcp::IdentifierBaseType", "dc/ddd/classisc_1_1dhcp_1_1IdentifierBaseType.html", "dc/ddd/classisc_1_1dhcp_1_1IdentifierBaseType" ],
    [ "isc::dhcp::IdentifierType< min_size, max_size >", "d3/dea/classisc_1_1dhcp_1_1IdentifierType.html", "d3/dea/classisc_1_1dhcp_1_1IdentifierType" ],
    [ "ClientIdPtr", "d9/d19/duid_8h.html#a99101d8d42c5a3bbbaa95cd4f674c50f", null ],
    [ "DuidPtr", "d9/d19/duid_8h.html#a43cc89624319ffdb48351523e739001a", null ],
    [ "IdentifierBaseTypePtr", "d9/d19/duid_8h.html#a86b7e9476e8fc2c8db52f0b3bb106856", null ]
];