var classisc_1_1db_1_1ServerSelector =
[
    [ "Type", "d9/df6/classisc_1_1db_1_1ServerSelector.html#a5eab0bc6aa92d740d1b59b8307b9ad4c", [
      [ "UNASSIGNED", "d9/df6/classisc_1_1db_1_1ServerSelector.html#a5eab0bc6aa92d740d1b59b8307b9ad4ca5ef0153ec1486a4da1db6a130c2dcbaa", null ],
      [ "ALL", "d9/df6/classisc_1_1db_1_1ServerSelector.html#a5eab0bc6aa92d740d1b59b8307b9ad4ca5fb1f955b45e38e31789286a1790398d", null ],
      [ "SUBSET", "d9/df6/classisc_1_1db_1_1ServerSelector.html#a5eab0bc6aa92d740d1b59b8307b9ad4ca956763dd1f14e97548bb5fce4b233b4b", null ],
      [ "ANY", "d9/df6/classisc_1_1db_1_1ServerSelector.html#a5eab0bc6aa92d740d1b59b8307b9ad4ca8e1bde3c3d303163521522cf1d62f21f", null ]
    ] ],
    [ "amAll", "d9/df6/classisc_1_1db_1_1ServerSelector.html#a87e01cbbd9875733866e192811a1f467", null ],
    [ "amAny", "d9/df6/classisc_1_1db_1_1ServerSelector.html#a88155c7b8fca152c1dd6e14dc758cb8a", null ],
    [ "amUnassigned", "d9/df6/classisc_1_1db_1_1ServerSelector.html#a3fc9fd10eb5a12e2b684b018e2641705", null ],
    [ "getTags", "d9/df6/classisc_1_1db_1_1ServerSelector.html#a0814dfe6e1528a2281d4d6d7c55a86d5", null ],
    [ "getType", "d9/df6/classisc_1_1db_1_1ServerSelector.html#aabf45acd1c5d992c939729d4d048e443", null ],
    [ "hasMultipleTags", "d9/df6/classisc_1_1db_1_1ServerSelector.html#a56225c7e4c342b1e106a039f6c078e84", null ],
    [ "hasNoTags", "d9/df6/classisc_1_1db_1_1ServerSelector.html#a10eb6d09f210a90efd8699b25e5c3c3f", null ]
];