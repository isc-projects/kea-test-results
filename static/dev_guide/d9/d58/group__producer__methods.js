var group__producer__methods =
[
    [ "isc::stats::StatsMgr::addValue", "d9/d58/group__producer__methods.html#ga706588dfd8bd300b64ea9035e428fc30", null ],
    [ "isc::stats::StatsMgr::addValue", "d9/d58/group__producer__methods.html#gadb861d768c9f285fcfccca6f53dc764f", null ],
    [ "isc::stats::StatsMgr::addValue", "d9/d58/group__producer__methods.html#gad5e3071885372926a55d2c9ece4cd61b", null ],
    [ "isc::stats::StatsMgr::addValue", "d9/d58/group__producer__methods.html#ga8f11bc4714853382264f10788f3339c6", null ],
    [ "isc::stats::StatsMgr::addValue", "d9/d58/group__producer__methods.html#gafd62286bde36079036035010382f0167", null ],
    [ "isc::stats::StatsMgr::getMaxSampleAgeDefault", "d9/d58/group__producer__methods.html#gabeb1d384bb5e757382583fc84a9d0df9", null ],
    [ "isc::stats::StatsMgr::getMaxSampleCountDefault", "d9/d58/group__producer__methods.html#ga4cfe95c09a7546116061954dea836338", null ],
    [ "isc::stats::StatsMgr::setMaxSampleAge", "d9/d58/group__producer__methods.html#gaa2349c55d07d71d79544b433a9dc30c5", null ],
    [ "isc::stats::StatsMgr::setMaxSampleAgeAll", "d9/d58/group__producer__methods.html#gac434b3ec9eb5cd598264e4afd6df2d58", null ],
    [ "isc::stats::StatsMgr::setMaxSampleAgeDefault", "d9/d58/group__producer__methods.html#gaee755156847e183684e4e2c03bfc0325", null ],
    [ "isc::stats::StatsMgr::setMaxSampleCount", "d9/d58/group__producer__methods.html#ga2b26aee9bfb7021c700cc74e1fd158c5", null ],
    [ "isc::stats::StatsMgr::setMaxSampleCountAll", "d9/d58/group__producer__methods.html#gad796393099cf186c33defe05343a9d03", null ],
    [ "isc::stats::StatsMgr::setMaxSampleCountDefault", "d9/d58/group__producer__methods.html#ga23632a99cc4034a545e827616045a504", null ],
    [ "isc::stats::StatsMgr::setValue", "d9/d58/group__producer__methods.html#gac4155345ef322091bb432c0d3b260346", null ],
    [ "isc::stats::StatsMgr::setValue", "d9/d58/group__producer__methods.html#ga968990a230a88b2f2ffcd9df88b0a05e", null ],
    [ "isc::stats::StatsMgr::setValue", "d9/d58/group__producer__methods.html#gae02c447857bf653a8e9a107d2c966db5", null ],
    [ "isc::stats::StatsMgr::setValue", "d9/d58/group__producer__methods.html#ga359381216ebd9d900737e983233b8175", null ],
    [ "isc::stats::StatsMgr::setValue", "d9/d58/group__producer__methods.html#gab686f2bd0a4035d2c8b0f1c3af07c794", null ]
];