var cfg__iface_8h =
[
    [ "isc::dhcp::CfgIface", "d6/d8b/classisc_1_1dhcp_1_1CfgIface.html", "d6/d8b/classisc_1_1dhcp_1_1CfgIface" ],
    [ "isc::dhcp::DuplicateAddress", "d0/d38/classisc_1_1dhcp_1_1DuplicateAddress.html", "d0/d38/classisc_1_1dhcp_1_1DuplicateAddress" ],
    [ "isc::dhcp::DuplicateIfaceName", "d4/d1d/classisc_1_1dhcp_1_1DuplicateIfaceName.html", "d4/d1d/classisc_1_1dhcp_1_1DuplicateIfaceName" ],
    [ "isc::dhcp::InvalidIfaceName", "d9/d86/classisc_1_1dhcp_1_1InvalidIfaceName.html", "d9/d86/classisc_1_1dhcp_1_1InvalidIfaceName" ],
    [ "isc::dhcp::InvalidSocketType", "d2/d86/classisc_1_1dhcp_1_1InvalidSocketType.html", "d2/d86/classisc_1_1dhcp_1_1InvalidSocketType" ],
    [ "isc::dhcp::NoSuchAddress", "d2/de7/classisc_1_1dhcp_1_1NoSuchAddress.html", "d2/de7/classisc_1_1dhcp_1_1NoSuchAddress" ],
    [ "isc::dhcp::NoSuchIface", "d7/d20/classisc_1_1dhcp_1_1NoSuchIface.html", "d7/d20/classisc_1_1dhcp_1_1NoSuchIface" ],
    [ "CfgIfacePtr", "d9/d08/cfg__iface_8h.html#a5479b8616cf9c0ba1a2ed02ab19300dd", null ],
    [ "ConstCfgIfacePtr", "d9/d08/cfg__iface_8h.html#a4978172e122359fbf5ee76e33d80e2b6", null ]
];