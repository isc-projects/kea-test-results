var asiodns__messages_8cc =
[
    [ "ASIODNS_FETCH_COMPLETED", "d9/d6b/asiodns__messages_8cc.html#a83c0bbe734707167b56161f1451856c1", null ],
    [ "ASIODNS_FETCH_STOPPED", "d9/d6b/asiodns__messages_8cc.html#a714bdba9f4965a320fb0a7449b544b69", null ],
    [ "ASIODNS_OPEN_SOCKET", "d9/d6b/asiodns__messages_8cc.html#a75b064732b70d5f4e453f399d386dae0", null ],
    [ "ASIODNS_READ_DATA", "d9/d6b/asiodns__messages_8cc.html#a98bcfb561a1c9a47a270fb7fb3f97eb5", null ],
    [ "ASIODNS_READ_TIMEOUT", "d9/d6b/asiodns__messages_8cc.html#a4539eb12f2049b66ae862d36cb7f62d0", null ],
    [ "ASIODNS_SEND_DATA", "d9/d6b/asiodns__messages_8cc.html#acd52dc1fa0fb1787dc71a82506f82a38", null ],
    [ "ASIODNS_UNKNOWN_ORIGIN", "d9/d6b/asiodns__messages_8cc.html#a5c5d1906692386ccdb561f71a4816cc3", null ],
    [ "ASIODNS_UNKNOWN_RESULT", "d9/d6b/asiodns__messages_8cc.html#a7b5f3093d3eb69f9690bc6a6eadfc771", null ]
];