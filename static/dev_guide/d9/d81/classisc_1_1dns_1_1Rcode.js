var classisc_1_1dns_1_1Rcode =
[
    [ "CodeValue", "d9/d81/classisc_1_1dns_1_1Rcode.html#acb4d8f6295e2257e435788f0644b3dea", [
      [ "NOERROR_CODE", "d9/d81/classisc_1_1dns_1_1Rcode.html#acb4d8f6295e2257e435788f0644b3deaaf6d823d9ff8e6e2e280d6094a163fc67", null ],
      [ "FORMERR_CODE", "d9/d81/classisc_1_1dns_1_1Rcode.html#acb4d8f6295e2257e435788f0644b3deaa7052490aa79b40d1eeb00a2318d35b4a", null ],
      [ "SERVFAIL_CODE", "d9/d81/classisc_1_1dns_1_1Rcode.html#acb4d8f6295e2257e435788f0644b3deaa71066d3d3a8ee87ebad0bf183c63d44e", null ],
      [ "NXDOMAIN_CODE", "d9/d81/classisc_1_1dns_1_1Rcode.html#acb4d8f6295e2257e435788f0644b3deaaadb6e4ae04fd016001ed736614683416", null ],
      [ "NOTIMP_CODE", "d9/d81/classisc_1_1dns_1_1Rcode.html#acb4d8f6295e2257e435788f0644b3deaa31faa8169abc7070bc57b9c95b8e3777", null ],
      [ "REFUSED_CODE", "d9/d81/classisc_1_1dns_1_1Rcode.html#acb4d8f6295e2257e435788f0644b3deaab6564d299c4bc19b69984085a13b35dc", null ],
      [ "YXDOMAIN_CODE", "d9/d81/classisc_1_1dns_1_1Rcode.html#acb4d8f6295e2257e435788f0644b3deaabd38bef6b9e6e8b89a46f7c140180866", null ],
      [ "YXRRSET_CODE", "d9/d81/classisc_1_1dns_1_1Rcode.html#acb4d8f6295e2257e435788f0644b3deaa0767466e01d10562e79334c33ed5fd12", null ],
      [ "NXRRSET_CODE", "d9/d81/classisc_1_1dns_1_1Rcode.html#acb4d8f6295e2257e435788f0644b3deaa50c48fda9320fa2e896727c2de318797", null ],
      [ "NOTAUTH_CODE", "d9/d81/classisc_1_1dns_1_1Rcode.html#acb4d8f6295e2257e435788f0644b3deaae45f7f52cf3ea97c66bf3a0c4873ed7f", null ],
      [ "NOTZONE_CODE", "d9/d81/classisc_1_1dns_1_1Rcode.html#acb4d8f6295e2257e435788f0644b3deaadec70ef4247507de5042ddc5f7f596a1", null ],
      [ "RESERVED11_CODE", "d9/d81/classisc_1_1dns_1_1Rcode.html#acb4d8f6295e2257e435788f0644b3deaa7943cc5bd8fff25b4876a61651671f03", null ],
      [ "RESERVED12_CODE", "d9/d81/classisc_1_1dns_1_1Rcode.html#acb4d8f6295e2257e435788f0644b3deaa6fdf9f66de13aa452e2f1bc97ffa5df3", null ],
      [ "RESERVED13_CODE", "d9/d81/classisc_1_1dns_1_1Rcode.html#acb4d8f6295e2257e435788f0644b3deaa76e1e74872974b38ba2ca12db14ae5cd", null ],
      [ "RESERVED14_CODE", "d9/d81/classisc_1_1dns_1_1Rcode.html#acb4d8f6295e2257e435788f0644b3deaaa8838d133b0e17f3109ba15504b610e2", null ],
      [ "RESERVED15_CODE", "d9/d81/classisc_1_1dns_1_1Rcode.html#acb4d8f6295e2257e435788f0644b3deaa31a2ee8cd12cb372872ed57a0e7cef01", null ],
      [ "BADVERS_CODE", "d9/d81/classisc_1_1dns_1_1Rcode.html#acb4d8f6295e2257e435788f0644b3deaafcacbc9ef406e57b309e42033a7f7499", null ]
    ] ],
    [ "Rcode", "d9/d81/classisc_1_1dns_1_1Rcode.html#a0df6941cbb2a766f43c7867f2db3409a", null ],
    [ "Rcode", "d9/d81/classisc_1_1dns_1_1Rcode.html#a04e3128555ee9edf384b411d97dad6bc", null ],
    [ "equals", "d9/d81/classisc_1_1dns_1_1Rcode.html#a604c571968117e58947748921f772e30", null ],
    [ "getCode", "d9/d81/classisc_1_1dns_1_1Rcode.html#a54b012380f11375119d8a03dd97e3631", null ],
    [ "getExtendedCode", "d9/d81/classisc_1_1dns_1_1Rcode.html#ab0a8ed35cd8d0a96b0a2fda4f8833f38", null ],
    [ "nequals", "d9/d81/classisc_1_1dns_1_1Rcode.html#a8bff37fd3e7c2613082591fd6bc38bcb", null ],
    [ "operator!=", "d9/d81/classisc_1_1dns_1_1Rcode.html#a4f8495faed4f1f10a83996793306f97c", null ],
    [ "operator==", "d9/d81/classisc_1_1dns_1_1Rcode.html#a06b6c572ba71b1b4f62959629a75a227", null ],
    [ "toText", "d9/d81/classisc_1_1dns_1_1Rcode.html#a383ac116bf6d7db5c3418b8204aa9fa3", null ]
];