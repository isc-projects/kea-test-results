var addr__utilities_8h =
[
    [ "addrsInRange", "d9/dd7/addr__utilities_8h.html#a90299422c1548620a9b3f48fe133afb0", null ],
    [ "firstAddrInPrefix", "d9/dd7/addr__utilities_8h.html#a816eb22de14b0c0a3e3bb79ce8eef0ab", null ],
    [ "getNetmask4", "d9/dd7/addr__utilities_8h.html#aa315c62ddce36a8037ba85518d23fc55", null ],
    [ "lastAddrInPrefix", "d9/dd7/addr__utilities_8h.html#aa0fc41c02c4201101b6f72b1ae151cdb", null ],
    [ "offsetAddress", "d9/dd7/addr__utilities_8h.html#ad82e873f716288bdf69d9076ebb7a464", null ],
    [ "prefixesInRange", "d9/dd7/addr__utilities_8h.html#a979539406738fb07906763bac582e931", null ],
    [ "prefixLengthFromRange", "d9/dd7/addr__utilities_8h.html#ad98f4a68fb0c3242ae1dd68624169dee", null ]
];