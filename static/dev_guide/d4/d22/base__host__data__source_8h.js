var base__host__data__source_8h =
[
    [ "isc::dhcp::BadHostAddress", "dd/d6c/classisc_1_1dhcp_1_1BadHostAddress.html", "dd/d6c/classisc_1_1dhcp_1_1BadHostAddress" ],
    [ "isc::dhcp::BaseHostDataSource", "df/d59/classisc_1_1dhcp_1_1BaseHostDataSource.html", "df/d59/classisc_1_1dhcp_1_1BaseHostDataSource" ],
    [ "isc::dhcp::DuplicateHost", "df/df8/classisc_1_1dhcp_1_1DuplicateHost.html", "df/df8/classisc_1_1dhcp_1_1DuplicateHost" ],
    [ "isc::dhcp::HostNotFound", "d6/d7c/classisc_1_1dhcp_1_1HostNotFound.html", "d6/d7c/classisc_1_1dhcp_1_1HostNotFound" ],
    [ "isc::dhcp::HostPageSize", "d1/dae/classisc_1_1dhcp_1_1HostPageSize.html", "d1/dae/classisc_1_1dhcp_1_1HostPageSize" ],
    [ "isc::dhcp::ReservedAddress", "d9/d61/classisc_1_1dhcp_1_1ReservedAddress.html", "d9/d61/classisc_1_1dhcp_1_1ReservedAddress" ],
    [ "HostDataSourceList", "d4/d22/base__host__data__source_8h.html#a647f7eeac7f7c9535babd48cbbaaa902", null ],
    [ "HostDataSourcePtr", "d4/d22/base__host__data__source_8h.html#a0288dc6b0ed46c4c4272c9f36d3f4251", null ]
];