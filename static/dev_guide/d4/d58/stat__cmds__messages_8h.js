var stat__cmds__messages_8h =
[
    [ "STAT_CMDS_DEINIT_OK", "d4/d58/stat__cmds__messages_8h.html#a221a6e76b77fcad19b9efb89b40cadb7", null ],
    [ "STAT_CMDS_INIT_OK", "d4/d58/stat__cmds__messages_8h.html#a67794f992b816b8b6be0e62019489384", null ],
    [ "STAT_CMDS_LEASE4_FAILED", "d4/d58/stat__cmds__messages_8h.html#a280657c3eb7b2c881d742a27d0a84ab2", null ],
    [ "STAT_CMDS_LEASE4_GET", "d4/d58/stat__cmds__messages_8h.html#ad3d79494660f30c45d6581d5a57954a7", null ],
    [ "STAT_CMDS_LEASE4_GET_FAILED", "d4/d58/stat__cmds__messages_8h.html#aa5950b3ac7b3b62249a5c70785193bc6", null ],
    [ "STAT_CMDS_LEASE4_GET_INVALID", "d4/d58/stat__cmds__messages_8h.html#a090e414672d095824dfcf11cde8e2b41", null ],
    [ "STAT_CMDS_LEASE4_GET_NO_SUBNETS", "d4/d58/stat__cmds__messages_8h.html#a47dc36573c413f40ea521a6275bc6938", null ],
    [ "STAT_CMDS_LEASE4_ORPHANED_STATS", "d4/d58/stat__cmds__messages_8h.html#ac6025988e46b86db1295a1d5c781efe3", null ],
    [ "STAT_CMDS_LEASE6_FAILED", "d4/d58/stat__cmds__messages_8h.html#a256b49797c1a7ad814d7ecf2de8606bd", null ],
    [ "STAT_CMDS_LEASE6_GET", "d4/d58/stat__cmds__messages_8h.html#a27004c410eb45a186c2b6f9ec9b9c419", null ],
    [ "STAT_CMDS_LEASE6_GET_FAILED", "d4/d58/stat__cmds__messages_8h.html#a287ae71e348cbea209de59c2a1fc77bd", null ],
    [ "STAT_CMDS_LEASE6_GET_INVALID", "d4/d58/stat__cmds__messages_8h.html#a18698f838726c5c2aa8ca1643be81d99", null ],
    [ "STAT_CMDS_LEASE6_GET_NO_SUBNETS", "d4/d58/stat__cmds__messages_8h.html#a49688e1a78d3b5ce3b1cf7f43f825dde", null ],
    [ "STAT_CMDS_LEASE6_ORPHANED_STATS", "d4/d58/stat__cmds__messages_8h.html#a6e12fde0a0620c5c35278a9dcbf25be4", null ]
];