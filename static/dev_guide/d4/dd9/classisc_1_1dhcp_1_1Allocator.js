var classisc_1_1dhcp_1_1Allocator =
[
    [ "PrefixLenMatchType", "d4/dd9/classisc_1_1dhcp_1_1Allocator.html#a5e8e2d374514b168a5faa59d313d867b", [
      [ "PREFIX_LEN_EQUAL", "d4/dd9/classisc_1_1dhcp_1_1Allocator.html#a5e8e2d374514b168a5faa59d313d867ba67c11f2f53148d0e93edebc6664fd95d", null ],
      [ "PREFIX_LEN_LOWER", "d4/dd9/classisc_1_1dhcp_1_1Allocator.html#a5e8e2d374514b168a5faa59d313d867ba8322b58dbf94cdffb4e7eecbe7b00f10", null ],
      [ "PREFIX_LEN_HIGHER", "d4/dd9/classisc_1_1dhcp_1_1Allocator.html#a5e8e2d374514b168a5faa59d313d867bafc1a5ff1f5f306ad46e194bc18a025a7", null ]
    ] ],
    [ "Allocator", "d4/dd9/classisc_1_1dhcp_1_1Allocator.html#a949e2f8a27305dcba03c749d82ab552a", null ],
    [ "~Allocator", "d4/dd9/classisc_1_1dhcp_1_1Allocator.html#ab41f99a458ad66340864e50f9dc59b3a", null ],
    [ "getType", "d4/dd9/classisc_1_1dhcp_1_1Allocator.html#ae280982376dd4babaffe200d0c1d4125", null ],
    [ "initAfterConfigure", "d4/dd9/classisc_1_1dhcp_1_1Allocator.html#a14683eac5657fd038ec4251d97e016a6", null ],
    [ "initAfterConfigureInternal", "d4/dd9/classisc_1_1dhcp_1_1Allocator.html#a573e4bee31509133792e5523d5e30777", null ],
    [ "pickAddress", "d4/dd9/classisc_1_1dhcp_1_1Allocator.html#a4c8cb297011943beefd31720834668d8", null ],
    [ "pickPrefix", "d4/dd9/classisc_1_1dhcp_1_1Allocator.html#a923f6e6ee56c60f4636a1ca062641015", null ],
    [ "inited_", "d4/dd9/classisc_1_1dhcp_1_1Allocator.html#a86562f48a161cb7261eaef63bdca98eb", null ],
    [ "mutex_", "d4/dd9/classisc_1_1dhcp_1_1Allocator.html#a7bd4de1cfe600607939d7ecb45a58607", null ],
    [ "pool_type_", "d4/dd9/classisc_1_1dhcp_1_1Allocator.html#a5780fa080a01feafafba7cc34ea4925f", null ],
    [ "subnet_", "d4/dd9/classisc_1_1dhcp_1_1Allocator.html#ac0584a611b75102c6b12aba333a07ec8", null ],
    [ "subnet_id_", "d4/dd9/classisc_1_1dhcp_1_1Allocator.html#a5680e4db94bb9c9bd910ad6a1be43b7f", null ]
];