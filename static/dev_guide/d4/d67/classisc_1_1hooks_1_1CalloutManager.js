var classisc_1_1hooks_1_1CalloutManager =
[
    [ "CalloutManager", "d4/d67/classisc_1_1hooks_1_1CalloutManager.html#a53c228219444ff2046ec84408208883b", null ],
    [ "callCallouts", "d4/d67/classisc_1_1hooks_1_1CalloutManager.html#a2bfd51aa7d01ee1676ddadffa2b318f2", null ],
    [ "callCommandHandlers", "d4/d67/classisc_1_1hooks_1_1CalloutManager.html#a519e3009128960287f5cd3c520d3474d", null ],
    [ "calloutsPresent", "d4/d67/classisc_1_1hooks_1_1CalloutManager.html#ac2d01167d34d86f8f7445df0e2c123c1", null ],
    [ "commandHandlersPresent", "d4/d67/classisc_1_1hooks_1_1CalloutManager.html#a2e1f7f7a3d7ef739796a93cff6bb66dc", null ],
    [ "deregisterAllCallouts", "d4/d67/classisc_1_1hooks_1_1CalloutManager.html#a60cd5217cbb9eaf4eae42a53fc51b17f", null ],
    [ "deregisterCallout", "d4/d67/classisc_1_1hooks_1_1CalloutManager.html#aa8ad42a8f325cf673044d28d5775c42e", null ],
    [ "getHookLibsVectorSize", "d4/d67/classisc_1_1hooks_1_1CalloutManager.html#a39257bd6f7ee255e7be9494b27a2ac06", null ],
    [ "getLibraryHandle", "d4/d67/classisc_1_1hooks_1_1CalloutManager.html#a1c746862a75b61fd761a8efbf49386be", null ],
    [ "getLibraryIndex", "d4/d67/classisc_1_1hooks_1_1CalloutManager.html#a9046178665c6e7ab3cba9e0196202c2e", null ],
    [ "getNumLibraries", "d4/d67/classisc_1_1hooks_1_1CalloutManager.html#a9da8c779155d3286bd816be86c1e7267", null ],
    [ "getPostLibraryHandle", "d4/d67/classisc_1_1hooks_1_1CalloutManager.html#a64559a12cf07035ee059d0677c472a78", null ],
    [ "getPreLibraryHandle", "d4/d67/classisc_1_1hooks_1_1CalloutManager.html#afd7c45b2e093d4e52557a6b301bb37fc", null ],
    [ "registerCallout", "d4/d67/classisc_1_1hooks_1_1CalloutManager.html#accc5770df466c22641cefce5a4675244", null ],
    [ "registerCommandHook", "d4/d67/classisc_1_1hooks_1_1CalloutManager.html#acc9d00505941b50b6300c2cabce2cd10", null ],
    [ "setLibraryIndex", "d4/d67/classisc_1_1hooks_1_1CalloutManager.html#aa727dcbf62c054b215ab0990b0210d28", null ]
];