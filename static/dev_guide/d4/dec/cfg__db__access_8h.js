var cfg__db__access_8h =
[
    [ "isc::dhcp::CfgDbAccess", "de/d27/classisc_1_1dhcp_1_1CfgDbAccess.html", "de/d27/classisc_1_1dhcp_1_1CfgDbAccess" ],
    [ "isc::dhcp::CfgHostDbAccess", "dc/d53/structisc_1_1dhcp_1_1CfgHostDbAccess.html", "dc/d53/structisc_1_1dhcp_1_1CfgHostDbAccess" ],
    [ "isc::dhcp::CfgLeaseDbAccess", "d9/de5/structisc_1_1dhcp_1_1CfgLeaseDbAccess.html", "d9/de5/structisc_1_1dhcp_1_1CfgLeaseDbAccess" ],
    [ "CfgDbAccessPtr", "d4/dec/cfg__db__access_8h.html#a402be8d0afa66fcb6aee9b4206275ee6", null ],
    [ "ConstCfgDbAccessPtr", "d4/dec/cfg__db__access_8h.html#a52afe4e8a10f7bc25f4f45883d6d3453", null ]
];