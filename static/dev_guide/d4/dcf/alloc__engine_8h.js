var alloc__engine_8h =
[
    [ "isc::dhcp::AllocEngine", "d9/ddb/classisc_1_1dhcp_1_1AllocEngine.html", "d9/ddb/classisc_1_1dhcp_1_1AllocEngine" ],
    [ "isc::dhcp::AllocEngine::ClientContext4", "d1/d73/structisc_1_1dhcp_1_1AllocEngine_1_1ClientContext4.html", "d1/d73/structisc_1_1dhcp_1_1AllocEngine_1_1ClientContext4" ],
    [ "isc::dhcp::AllocEngine::ClientContext6", "d5/d93/structisc_1_1dhcp_1_1AllocEngine_1_1ClientContext6.html", "d5/d93/structisc_1_1dhcp_1_1AllocEngine_1_1ClientContext6" ],
    [ "isc::dhcp::AllocEngine::ClientContext6::IAContext", "da/d6f/structisc_1_1dhcp_1_1AllocEngine_1_1ClientContext6_1_1IAContext.html", "da/d6f/structisc_1_1dhcp_1_1AllocEngine_1_1ClientContext6_1_1IAContext" ],
    [ "isc::dhcp::AllocEngine::Resource", "dd/d5e/classisc_1_1dhcp_1_1AllocEngine_1_1Resource.html", "dd/d5e/classisc_1_1dhcp_1_1AllocEngine_1_1Resource" ],
    [ "isc::dhcp::AllocEngine::ResourceCompare", "d1/deb/structisc_1_1dhcp_1_1AllocEngine_1_1ResourceCompare.html", "d1/deb/structisc_1_1dhcp_1_1AllocEngine_1_1ResourceCompare" ],
    [ "AllocEnginePtr", "d4/dcf/alloc__engine_8h.html#a4b2c7a4804c619c8828b0de767ca96c5", null ]
];