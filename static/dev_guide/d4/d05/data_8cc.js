var data_8cc =
[
    [ "copy", "d4/d05/data_8cc.html#a16726178475de4c999d32567add4d8bd", null ],
    [ "extend", "d4/d05/data_8cc.html#ac28d36100922f9ef9877172a446898a7", null ],
    [ "isEquivalent", "d4/d05/data_8cc.html#a6c2d8d550860ab05fd07adda22a30398", null ],
    [ "isNull", "d4/d05/data_8cc.html#ac624ad274b9900071ada4e995d838e9e", null ],
    [ "merge", "d4/d05/data_8cc.html#a29232e24250a595c2de8abff43369858", null ],
    [ "mergeDiffAdd", "d4/d05/data_8cc.html#a1e07e7bf822c7292d324abe40e0292a6", null ],
    [ "mergeDiffDel", "d4/d05/data_8cc.html#a3b59560a23409c061f08284b35091e5b", null ],
    [ "operator!=", "d4/d05/data_8cc.html#adc89b58b04e4a2be6c4cca393e4d02df", null ],
    [ "operator<", "d4/d05/data_8cc.html#a5e67f5e8b3d9dba73eb5362b9cc9a163", null ],
    [ "operator<<", "d4/d05/data_8cc.html#a9f2e2a5805a5cffbaff271da3ce3f031", null ],
    [ "operator<<", "d4/d05/data_8cc.html#acbec0b598eef7c880c62f86a013fe46f", null ],
    [ "operator==", "d4/d05/data_8cc.html#a1c9c56e05292faaca1b7d46816fef84a", null ],
    [ "prettyPrint", "d4/d05/data_8cc.html#a8ab01676ce5abff2fe3507ffcfd21c18", null ],
    [ "prettyPrint", "d4/d05/data_8cc.html#a9410f0f91680b2e0260b6287b8f035cd", null ],
    [ "removeIdentical", "d4/d05/data_8cc.html#ac70e36997bdcd28977cbbd249b0ca510", null ],
    [ "removeIdentical", "d4/d05/data_8cc.html#a23f8af6e2ae4cea07143a7feab9fbfd1", null ]
];