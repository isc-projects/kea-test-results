var classisc_1_1dhcp_1_1CfgConsistency =
[
    [ "ExtendedInfoSanity", "d4/dbd/classisc_1_1dhcp_1_1CfgConsistency.html#ad79627de2595a78c5048d374b697fb9b", [
      [ "EXTENDED_INFO_CHECK_NONE", "d4/dbd/classisc_1_1dhcp_1_1CfgConsistency.html#ad79627de2595a78c5048d374b697fb9bae2a847a5c6656441cf8ef0837e932747", null ],
      [ "EXTENDED_INFO_CHECK_FIX", "d4/dbd/classisc_1_1dhcp_1_1CfgConsistency.html#ad79627de2595a78c5048d374b697fb9ba3aa7f6575f1e87322676489fc6007f94", null ],
      [ "EXTENDED_INFO_CHECK_STRICT", "d4/dbd/classisc_1_1dhcp_1_1CfgConsistency.html#ad79627de2595a78c5048d374b697fb9ba0b8c57ccf29ddeac6e4a109458665629", null ],
      [ "EXTENDED_INFO_CHECK_PEDANTIC", "d4/dbd/classisc_1_1dhcp_1_1CfgConsistency.html#ad79627de2595a78c5048d374b697fb9ba1acd9e436bf3a8374c7442547f49660c", null ]
    ] ],
    [ "LeaseSanity", "d4/dbd/classisc_1_1dhcp_1_1CfgConsistency.html#a40ef2d24a1b83ec712347deade5be91f", [
      [ "LEASE_CHECK_NONE", "d4/dbd/classisc_1_1dhcp_1_1CfgConsistency.html#a40ef2d24a1b83ec712347deade5be91faad881502bb2509040e2521f9fddd9e29", null ],
      [ "LEASE_CHECK_WARN", "d4/dbd/classisc_1_1dhcp_1_1CfgConsistency.html#a40ef2d24a1b83ec712347deade5be91fa97f0c85ea55237e78695e95f016ac4ce", null ],
      [ "LEASE_CHECK_FIX", "d4/dbd/classisc_1_1dhcp_1_1CfgConsistency.html#a40ef2d24a1b83ec712347deade5be91fa6e1b8ec30db197a92dfad4e7cd9248b7", null ],
      [ "LEASE_CHECK_FIX_DEL", "d4/dbd/classisc_1_1dhcp_1_1CfgConsistency.html#a40ef2d24a1b83ec712347deade5be91fa734af98a138fc2bb2cd6fadf1aebefd3", null ],
      [ "LEASE_CHECK_DEL", "d4/dbd/classisc_1_1dhcp_1_1CfgConsistency.html#a40ef2d24a1b83ec712347deade5be91fa7153d9e95352a4a22581e5e820834376", null ]
    ] ],
    [ "CfgConsistency", "d4/dbd/classisc_1_1dhcp_1_1CfgConsistency.html#adbab0c5bd9f7cf80ee2e71baaadae5f8", null ],
    [ "getExtendedInfoSanityCheck", "d4/dbd/classisc_1_1dhcp_1_1CfgConsistency.html#ac818e81557891591395a025280785369", null ],
    [ "getLeaseSanityCheck", "d4/dbd/classisc_1_1dhcp_1_1CfgConsistency.html#ae33580245db0faea620939d761dec83a", null ],
    [ "setExtendedInfoSanityCheck", "d4/dbd/classisc_1_1dhcp_1_1CfgConsistency.html#aebf6c29a2f44b1312201f51f6dae446f", null ],
    [ "setLeaseSanityCheck", "d4/dbd/classisc_1_1dhcp_1_1CfgConsistency.html#a231d9856f7a13162591730e133dd5bb9", null ],
    [ "toElement", "d4/dbd/classisc_1_1dhcp_1_1CfgConsistency.html#a1ac31c71081534cb380d02f41a916e1d", null ]
];