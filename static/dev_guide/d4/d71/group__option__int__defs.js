var group__option__int__defs =
[
    [ "isc::dhcp::OptionUint16", "d4/d71/group__option__int__defs.html#ga3dd98b59b9f1a7c7d65ec6fb3a397b36", null ],
    [ "isc::dhcp::OptionUint16Ptr", "d4/d71/group__option__int__defs.html#gabdbb17a533d23d0671bd1443d78064ea", null ],
    [ "isc::dhcp::OptionUint32", "d4/d71/group__option__int__defs.html#ga3c67cc47ae4611ada65173f09121efb7", null ],
    [ "isc::dhcp::OptionUint32Ptr", "d4/d71/group__option__int__defs.html#gaa6cec58768c00d4936fc596a3391edcc", null ],
    [ "isc::dhcp::OptionUint8", "d4/d71/group__option__int__defs.html#ga01cdc472cb4a556f0a30defab7979d5e", null ],
    [ "isc::dhcp::OptionUint8Ptr", "d4/d71/group__option__int__defs.html#ga54255c7809151cafe2d5fb632155231e", null ]
];