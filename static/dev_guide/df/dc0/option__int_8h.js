var option__int_8h =
[
    [ "isc::dhcp::OptionInt< T >", "d4/d82/classisc_1_1dhcp_1_1OptionInt.html", "d4/d82/classisc_1_1dhcp_1_1OptionInt" ],
    [ "OptionUint16", "df/dc0/option__int_8h.html#ga3dd98b59b9f1a7c7d65ec6fb3a397b36", null ],
    [ "OptionUint16Ptr", "df/dc0/option__int_8h.html#gabdbb17a533d23d0671bd1443d78064ea", null ],
    [ "OptionUint32", "df/dc0/option__int_8h.html#ga3c67cc47ae4611ada65173f09121efb7", null ],
    [ "OptionUint32Ptr", "df/dc0/option__int_8h.html#gaa6cec58768c00d4936fc596a3391edcc", null ],
    [ "OptionUint8", "df/dc0/option__int_8h.html#ga01cdc472cb4a556f0a30defab7979d5e", null ],
    [ "OptionUint8Ptr", "df/dc0/option__int_8h.html#ga54255c7809151cafe2d5fb632155231e", null ]
];