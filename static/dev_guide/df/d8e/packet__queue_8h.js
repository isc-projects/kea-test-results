var packet__queue_8h =
[
    [ "isc::dhcp::InvalidQueueParameter", "df/d3d/classisc_1_1dhcp_1_1InvalidQueueParameter.html", "df/d3d/classisc_1_1dhcp_1_1InvalidQueueParameter" ],
    [ "isc::dhcp::PacketQueue< PacketTypePtr >", "d1/d11/classisc_1_1dhcp_1_1PacketQueue.html", "d1/d11/classisc_1_1dhcp_1_1PacketQueue" ],
    [ "PacketQueue4Ptr", "df/d8e/packet__queue_8h.html#a2e8ec53efc6675daf2c4eae306dd7ffc", null ],
    [ "PacketQueue6Ptr", "df/d8e/packet__queue_8h.html#a7f79c3791c69a897d9d4dcf2de6d8fa9", null ],
    [ "QueueEnd", "df/d8e/packet__queue_8h.html#ac2346afa330a13da4f942f563bb2240c", [
      [ "FRONT", "df/d8e/packet__queue_8h.html#ac2346afa330a13da4f942f563bb2240cabb2fe5c916efb43aab8cbb68f997d2ee", null ],
      [ "BACK", "df/d8e/packet__queue_8h.html#ac2346afa330a13da4f942f563bb2240ca1dd26f1f1790f0b56d5752fb0fbecef0", null ]
    ] ]
];