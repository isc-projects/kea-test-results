var database__connection_8h =
[
    [ "isc::db::DatabaseConnection", "db/db4/classisc_1_1db_1_1DatabaseConnection.html", "db/db4/classisc_1_1db_1_1DatabaseConnection" ],
    [ "isc::db::DbConnectionInitWithRetry", "d1/d1a/classisc_1_1db_1_1DbConnectionInitWithRetry.html", "d1/d1a/classisc_1_1db_1_1DbConnectionInitWithRetry" ],
    [ "isc::db::DbConnectionUnusable", "d8/d3c/classisc_1_1db_1_1DbConnectionUnusable.html", "d8/d3c/classisc_1_1db_1_1DbConnectionUnusable" ],
    [ "isc::db::DbInvalidPort", "d5/d49/classisc_1_1db_1_1DbInvalidPort.html", "d5/d49/classisc_1_1db_1_1DbInvalidPort" ],
    [ "isc::db::DbInvalidReadOnly", "d5/d7a/classisc_1_1db_1_1DbInvalidReadOnly.html", "d5/d7a/classisc_1_1db_1_1DbInvalidReadOnly" ],
    [ "isc::db::DbInvalidTimeout", "d8/d78/classisc_1_1db_1_1DbInvalidTimeout.html", "d8/d78/classisc_1_1db_1_1DbInvalidTimeout" ],
    [ "isc::db::DbOpenError", "d4/d39/classisc_1_1db_1_1DbOpenError.html", "d4/d39/classisc_1_1db_1_1DbOpenError" ],
    [ "isc::db::DbOpenErrorWithRetry", "d5/ded/classisc_1_1db_1_1DbOpenErrorWithRetry.html", "d5/ded/classisc_1_1db_1_1DbOpenErrorWithRetry" ],
    [ "isc::db::DbOperationError", "dc/d5d/classisc_1_1db_1_1DbOperationError.html", "dc/d5d/classisc_1_1db_1_1DbOperationError" ],
    [ "isc::db::DatabaseConnection::EnterTest", "d9/dcf/classisc_1_1db_1_1DatabaseConnection_1_1EnterTest.html", "d9/dcf/classisc_1_1db_1_1DatabaseConnection_1_1EnterTest" ],
    [ "isc::db::InvalidType", "d5/d28/classisc_1_1db_1_1InvalidType.html", "d5/d28/classisc_1_1db_1_1InvalidType" ],
    [ "isc::db::NoDatabaseName", "d7/dad/classisc_1_1db_1_1NoDatabaseName.html", "d7/dad/classisc_1_1db_1_1NoDatabaseName" ],
    [ "isc::db::SchemaInitializationFailed", "df/de3/classisc_1_1db_1_1SchemaInitializationFailed.html", "df/de3/classisc_1_1db_1_1SchemaInitializationFailed" ],
    [ "DbCallback", "df/d67/database__connection_8h.html#aef13466fe62823c87fc1481f63846226", null ],
    [ "IOServiceAccessor", "df/d67/database__connection_8h.html#af0426766191408cf0e7677cf76ca3917", null ],
    [ "IOServiceAccessorPtr", "df/d67/database__connection_8h.html#a5ee2f39d90eaf33b188d34f92d0497e4", null ]
];