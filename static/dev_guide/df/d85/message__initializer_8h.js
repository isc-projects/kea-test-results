var message__initializer_8h =
[
    [ "isc::log::MessageInitializer", "d4/d72/classisc_1_1log_1_1MessageInitializer.html", "d4/d72/classisc_1_1log_1_1MessageInitializer" ],
    [ "LoggerDuplicatesList", "df/d85/message__initializer_8h.html#afddf80d6e6d91355b4c90c45d28ccfad", null ],
    [ "LoggerDuplicatesListPtr", "df/d85/message__initializer_8h.html#abc5fc05ba2a7c1de5d86d3374dfdfaa5", null ],
    [ "LoggerValuesList", "df/d85/message__initializer_8h.html#a177fa2047905e3746018e96565a3d789", null ],
    [ "LoggerValuesListPtr", "df/d85/message__initializer_8h.html#a98122fd635c4650c873092e3fa546e8c", null ]
];