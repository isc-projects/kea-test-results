var group__command__methods =
[
    [ "isc::stats::StatsMgr::statisticGetAllHandler", "df/d05/group__command__methods.html#ga3f9ee2831b9c57b417492a220c638d6c", null ],
    [ "isc::stats::StatsMgr::statisticGetHandler", "df/d05/group__command__methods.html#ga5e3a21b054ab29ae630ede41d4bb8a54", null ],
    [ "isc::stats::StatsMgr::statisticRemoveAllHandler", "df/d05/group__command__methods.html#gae6c5737cd4198af8fdccd57afe6b62ba", null ],
    [ "isc::stats::StatsMgr::statisticRemoveHandler", "df/d05/group__command__methods.html#ga5c0c973e1e2967c94c93350e49abfb20", null ],
    [ "isc::stats::StatsMgr::statisticResetAllHandler", "df/d05/group__command__methods.html#gad785edef7f7a09da99e0a9b7672e20f2", null ],
    [ "isc::stats::StatsMgr::statisticResetHandler", "df/d05/group__command__methods.html#ga3526de4e0b292b4cbae38719abbb7489", null ],
    [ "isc::stats::StatsMgr::statisticSetMaxSampleAgeAllHandler", "df/d05/group__command__methods.html#gaae364e0d7b005e5b16ae6aafe1944868", null ],
    [ "isc::stats::StatsMgr::statisticSetMaxSampleAgeHandler", "df/d05/group__command__methods.html#gae59e4a07a8e3acc3f4636507135b3b84", null ],
    [ "isc::stats::StatsMgr::statisticSetMaxSampleCountAllHandler", "df/d05/group__command__methods.html#ga22923cc12b4615b5a8bbe4209a440507", null ],
    [ "isc::stats::StatsMgr::statisticSetMaxSampleCountHandler", "df/d05/group__command__methods.html#gae97c65c05c2891bb20377447a975aed5", null ]
];