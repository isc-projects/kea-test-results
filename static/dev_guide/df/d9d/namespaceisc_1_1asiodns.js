var namespaceisc_1_1asiodns =
[
    [ "IOFetch", "d6/dcd/classisc_1_1asiodns_1_1IOFetch.html", "d6/dcd/classisc_1_1asiodns_1_1IOFetch" ],
    [ "IOFetchData", "d6/da3/structisc_1_1asiodns_1_1IOFetchData.html", "d6/da3/structisc_1_1asiodns_1_1IOFetchData" ],
    [ "IOFetchPtr", "df/d9d/namespaceisc_1_1asiodns.html#af86a5eb29a5f192185a4cf164f64adba", null ],
    [ "ASIODNS_FETCH_COMPLETED", "df/d9d/namespaceisc_1_1asiodns.html#a83c0bbe734707167b56161f1451856c1", null ],
    [ "ASIODNS_FETCH_STOPPED", "df/d9d/namespaceisc_1_1asiodns.html#a714bdba9f4965a320fb0a7449b544b69", null ],
    [ "ASIODNS_OPEN_SOCKET", "df/d9d/namespaceisc_1_1asiodns.html#a75b064732b70d5f4e453f399d386dae0", null ],
    [ "ASIODNS_READ_DATA", "df/d9d/namespaceisc_1_1asiodns.html#a98bcfb561a1c9a47a270fb7fb3f97eb5", null ],
    [ "ASIODNS_READ_TIMEOUT", "df/d9d/namespaceisc_1_1asiodns.html#a4539eb12f2049b66ae862d36cb7f62d0", null ],
    [ "ASIODNS_SEND_DATA", "df/d9d/namespaceisc_1_1asiodns.html#acd52dc1fa0fb1787dc71a82506f82a38", null ],
    [ "ASIODNS_UNKNOWN_ORIGIN", "df/d9d/namespaceisc_1_1asiodns.html#a5c5d1906692386ccdb561f71a4816cc3", null ],
    [ "ASIODNS_UNKNOWN_RESULT", "df/d9d/namespaceisc_1_1asiodns.html#a7b5f3093d3eb69f9690bc6a6eadfc771", null ],
    [ "DBG_COMMON", "df/d9d/namespaceisc_1_1asiodns.html#a8b2073aae8ab4ff554aec3c65ba3ff51", null ],
    [ "DBG_IMPORTANT", "df/d9d/namespaceisc_1_1asiodns.html#abde4c72636445831d702ac8460e745fe", null ],
    [ "DBG_VERBOSE", "df/d9d/namespaceisc_1_1asiodns.html#a07df9701d4f93e990c2dc734b98684ef", null ],
    [ "logger", "df/d9d/namespaceisc_1_1asiodns.html#a322c71d77902d2db025c902907beb62e", null ]
];