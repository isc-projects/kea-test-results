var index =
[
    [ "Contributor's Guide", "index.html#contrib", null ],
    [ "Quality Assurance", "index.html#qa", null ],
    [ "Hooks Framework", "index.html#hooksFramework", null ],
    [ "DHCP Maintenance Guide", "index.html#dhcpMaintenanceGuide", null ],
    [ "Kea libraries", "index.html#libraries", null ],
    [ "Miscellaneous Topics", "index.html#miscellaneousTopics", null ],
    [ "Kea Contributor's Guide", "d7/df4/contributorGuide.html", null ],
    [ "Quality Assurance in Kea", "d6/d4f/qaIntro.html", [
      [ "Running CI pipeline on Github", "d6/d4f/qaIntro.html#ciGithub", null ],
      [ "Running CI pipeline on Gitlab", "d6/d4f/qaIntro.html#ciGitlab", null ],
      [ "Running CI pipeline on Jenkins", "d6/d4f/qaIntro.html#ciJenkins", null ]
    ] ],
    [ "Building Kea with Unit Tests", "d1/d2e/unitTests.html", [
      [ "Environment Variables", "d1/d2e/unitTests.html#unitTestsEnvironmentVariables", null ],
      [ "Use Sanitizers", "d1/d2e/unitTests.html#unitTestsSanitizers", null ],
      [ "Databases Configuration for Unit Tests", "d1/d2e/unitTests.html#unitTestsDatabaseConfig", [
        [ "Database Users Required for Unit Tests", "d1/d2e/unitTests.html#unitTestsDatabaseUsers", null ],
        [ "MySQL Database", "d1/d2e/unitTests.html#mysqlUnitTestsPrerequisites", null ],
        [ "MySQL Database with SSL/TLS", "d1/d2e/unitTests.html#mysqlUnitTestsTLS", null ],
        [ "PostgreSQL Database", "d1/d2e/unitTests.html#pgsqlUnitTestsPrerequisites", null ]
      ] ],
      [ "Kerberos Configuration for Unit Tests", "d1/d2e/unitTests.html#unitTestsKerberos", null ],
      [ "Writing shell scripts and tests", "d1/d2e/unitTests.html#writingShellScriptsAndTests", null ]
    ] ],
    [ "Performance Testing", "dc/d3c/performance.html", "dc/d3c/performance" ],
    [ "Fuzzing Kea", "d9/d8b/fuzzer.html", [
      [ "Introduction", "d9/d8b/fuzzer.html#fuzzIntro", null ],
      [ "Using the LLVMFuzzerTestOneInput Harness Function", "d9/d8b/fuzzer.html#LLVMFuzzerTestOneInput", [
        [ "How to Build the LLVM Fuzzer", "d9/d8b/fuzzer.html#HowToBuild", null ],
        [ "How to Run the LLVM Fuzzer", "d9/d8b/fuzzer.html#HowToRun", null ],
        [ "The Code Structure of the LLVM Fuzzer", "d9/d8b/fuzzer.html#FuzzingStructure", null ],
        [ "Development Considerations About LLVM Fuzzing in Kea", "d9/d8b/fuzzer.html#FuzzingConsiderations", null ]
      ] ],
      [ "Using AFL", "d9/d8b/fuzzer.html#usingAFL", [
        [ "Fuzzing with Network Packets", "d9/d8b/fuzzer.html#fuzzTypeNetwork", null ],
        [ "Fuzzing with Configuration Files", "d9/d8b/fuzzer.html#fuzzTypeConfig", null ],
        [ "Building Kea for Fuzzing", "d9/d8b/fuzzer.html#fuzzBuild", null ],
        [ "Fuzzing with Network Packets", "d9/d8b/fuzzer.html#fuzzRunNetwork", null ],
        [ "Fuzzing with Configuration Files", "d9/d8b/fuzzer.html#fuzzRunConfig", null ],
        [ "Fuzzing with Network Packets", "d9/d8b/fuzzer.html#fuzzInternalNetwork", null ],
        [ "Fuzzing with Configuration Files", "d9/d8b/fuzzer.html#fuzzInternalConfig", null ],
        [ "Changes Required for Multi-Threaded Kea", "d9/d8b/fuzzer.html#fuzzThreads", null ],
        [ "Unit Test Failures", "d9/d8b/fuzzer.html#fuzzNotesUnitTests", null ]
      ] ]
    ] ],
    [ "Hooks Developer's Guide", "df/d46/hooksdgDevelopersGuide.html", [
      [ "Introduction", "df/d46/hooksdgDevelopersGuide.html#hooksdgIntroduction", [
        [ "Languages", "df/d46/hooksdgDevelopersGuide.html#hooksdgLanguages", null ],
        [ "Terminology", "df/d46/hooksdgDevelopersGuide.html#hooksdgTerminology", null ]
      ] ],
      [ "Tutorial", "df/d46/hooksdgDevelopersGuide.html#hooksdgTutorial", [
        [ "Framework Functions", "df/d46/hooksdgDevelopersGuide.html#hooksdgFrameworkFunctions", [
          [ "The \"version\" Function", "df/d46/hooksdgDevelopersGuide.html#hooksdgVersionFunction", null ],
          [ "The \"load\" and \"unload\" Functions", "df/d46/hooksdgDevelopersGuide.html#hooksdgLoadUnloadFunctions", null ],
          [ "The \"multi_threading_compatible\" function", "df/d46/hooksdgDevelopersGuide.html#hooksdgMultiThreadingCompatibleFunction", null ]
        ] ],
        [ "Callouts", "df/d46/hooksdgDevelopersGuide.html#hooksdgCallouts", [
          [ "The Callout Signature", "df/d46/hooksdgDevelopersGuide.html#hooksdgCalloutSignature", null ],
          [ "Callout Arguments", "df/d46/hooksdgDevelopersGuide.html#hooksdgArguments", null ],
          [ "The Next step status", "df/d46/hooksdgDevelopersGuide.html#hooksdgNextStep", null ],
          [ "The \"Skip\" Flag (deprecated)", "df/d46/hooksdgDevelopersGuide.html#hooksdgSkipFlag", null ],
          [ "Per-Request Context", "df/d46/hooksdgDevelopersGuide.html#hooksdgCalloutContext", null ]
        ] ],
        [ "Example Callouts", "df/d46/hooksdgDevelopersGuide.html#hooksdgExampleCallouts", null ],
        [ "Logging in the Hooks Library", "df/d46/hooksdgDevelopersGuide.html#hooksdgLogging", null ],
        [ "Building the Library", "df/d46/hooksdgDevelopersGuide.html#hooksdgBuild", null ],
        [ "Configuring the Hooks Library", "df/d46/hooksdgDevelopersGuide.html#hooksdgConfiguration", null ]
      ] ],
      [ "Advanced Topics", "df/d46/hooksdgDevelopersGuide.html#hooksdgAdvancedTopics", [
        [ "Context Creation and Destruction", "df/d46/hooksdgDevelopersGuide.html#hooksdgContextCreateDestroy", null ],
        [ "Registering Callouts", "df/d46/hooksdgDevelopersGuide.html#hooksdgCalloutRegistration", [
          [ "The LibraryHandle Object", "df/d46/hooksdgDevelopersGuide.html#hooksdgLibraryHandle", null ],
          [ "Non-Standard Callout Names", "df/d46/hooksdgDevelopersGuide.html#hooksdgNonstandardCalloutNames", null ],
          [ "Using Callouts as Command handlers", "df/d46/hooksdgDevelopersGuide.html#hooksdgCommandHandlers", null ],
          [ "Multiple Callouts on a Hook", "df/d46/hooksdgDevelopersGuide.html#hooksdgMultipleCallouts", null ]
        ] ],
        [ "Multiple User Libraries", "df/d46/hooksdgDevelopersGuide.html#hooksdgMultipleLibraries", null ],
        [ "Passing Data Between Libraries", "df/d46/hooksdgDevelopersGuide.html#hooksdgInterLibraryData", null ],
        [ "Running Against a Statically-Linked Kea", "df/d46/hooksdgDevelopersGuide.html#hooksdgStaticallyLinkedKea", null ],
        [ "Configuring Hooks Libraries", "df/d46/hooksdgDevelopersGuide.html#hooksdgHooksConfig", null ],
        [ "Memory Management Considerations for Hooks Writer", "df/d46/hooksdgDevelopersGuide.html#hooksMemoryManagement", null ],
        [ "Multi-Threading Considerations for Hooks Writers", "df/d46/hooksdgDevelopersGuide.html#hooksMultiThreading", null ]
      ] ]
    ] ],
    [ "The Hooks API for the DHCPv4 Server", "de/df3/dhcpv4Hooks.html", [
      [ "Introduction", "de/df3/dhcpv4Hooks.html#dhcpv4HooksIntroduction", null ],
      [ "Hooks in the DHCPv4 Server", "de/df3/dhcpv4Hooks.html#dhcpv4HooksHookPoints", [
        [ "dhcp4_srv_configured", "de/df3/dhcpv4Hooks.html#dhcp4HooksDhcpv4SrvConfigured", null ],
        [ "cb4_updated", "de/df3/dhcpv4Hooks.html#dhcpv4HooksCb4Update", null ],
        [ "buffer4_receive", "de/df3/dhcpv4Hooks.html#dhcpv4HooksBuffer4Receive", null ],
        [ "pkt4_receive", "de/df3/dhcpv4Hooks.html#dhcpv4HooksPkt4Receive", null ],
        [ "subnet4_select", "de/df3/dhcpv4Hooks.html#dhcpv4HooksSubnet4Select", null ],
        [ "host4_identifier", "de/df3/dhcpv4Hooks.html#dhcpv4HooksHost4Identifier", null ],
        [ "lease4_select", "de/df3/dhcpv4Hooks.html#dhcpv4HooksLeaseSelect", null ],
        [ "lease4_renew", "de/df3/dhcpv4Hooks.html#dhcpv4HooksLeaseRenew", null ],
        [ "lease4_release", "de/df3/dhcpv4Hooks.html#dhcpv4HooksLeaseRelease", null ],
        [ "lease4_decline", "de/df3/dhcpv4Hooks.html#dhcpv4HooksLeaseDecline", null ],
        [ "ddns4_update", "de/df3/dhcpv4Hooks.html#dhcpv4HooksDdns4Update", null ],
        [ "leases4_committed", "de/df3/dhcpv4Hooks.html#dhcpv4Leases4Committed", null ],
        [ "lease4_offer", "de/df3/dhcpv4Hooks.html#dhcpv4Lease4Offer", null ],
        [ "lease4_server_decline", "de/df3/dhcpv4Hooks.html#dhcpv4Lease4ServerDecline", null ],
        [ "pkt4_send", "de/df3/dhcpv4Hooks.html#dhcpv4HooksPkt4Send", null ],
        [ "buffer4_send", "de/df3/dhcpv4Hooks.html#dhcpv4HooksBuffer4Send", null ],
        [ "lease4_expire", "de/df3/dhcpv4Hooks.html#dhcpv4HooksLease4Expire", null ],
        [ "lease4_recover", "de/df3/dhcpv4Hooks.html#dhcpv4HooksLease4Recover", null ],
        [ "command_processed", "de/df3/dhcpv4Hooks.html#dhcpv4HooksCommandProcessed", null ]
      ] ],
      [ "Accessing DHCPv4 Options within a Packet", "de/df3/dhcpv4Hooks.html#dhcpv4HooksOptionsAccess", null ]
    ] ],
    [ "The Hooks API for the DHCPv6 Server", "d1/d02/dhcpv6Hooks.html", [
      [ "Introduction", "d1/d02/dhcpv6Hooks.html#dhcpv6HooksIntroduction", null ],
      [ "Hooks in the DHCPv6 Server", "d1/d02/dhcpv6Hooks.html#dhcpv6HooksHookPoints", [
        [ "dhcp6_srv_configured", "d1/d02/dhcpv6Hooks.html#dhcp6HooksDhcpv6SrvConfigured", null ],
        [ "cb6_updated", "d1/d02/dhcpv6Hooks.html#dhcpv6HooksCb6Update", null ],
        [ "buffer6_receive", "d1/d02/dhcpv6Hooks.html#dhcpv6HooksBuffer6Receive", null ],
        [ "pkt6_receive", "d1/d02/dhcpv6Hooks.html#dhcpv6HooksPkt6Receive", null ],
        [ "subnet6_select", "d1/d02/dhcpv6Hooks.html#dhcpv6HooksSubnet6Select", null ],
        [ "host6_identifier", "d1/d02/dhcpv6Hooks.html#dhcpv6HooksHost6Identifier", null ],
        [ "lease6_select", "d1/d02/dhcpv6Hooks.html#dhcpv6HooksLease6Select", null ],
        [ "lease6_renew", "d1/d02/dhcpv6Hooks.html#dhcpv6HooksLease6Renew", null ],
        [ "lease6_rebind", "d1/d02/dhcpv6Hooks.html#dhcpv6HooksLease6Rebind", null ],
        [ "lease6_decline", "d1/d02/dhcpv6Hooks.html#dhcpv6HooksLease6Decline", null ],
        [ "lease6_release", "d1/d02/dhcpv6Hooks.html#dhcpv6HooksLease6Release", null ],
        [ "ddns6_update", "d1/d02/dhcpv6Hooks.html#dhcpv6HooksDdns6Update", null ],
        [ "addr6_register", "d1/d02/dhcpv6Hooks.html#dhcpv6HookAddr6Register", null ],
        [ "leases6_committed", "d1/d02/dhcpv6Hooks.html#dhcpv6Leases6Committed", null ],
        [ "pkt6_send", "d1/d02/dhcpv6Hooks.html#dhcpv6HooksPkt6Send", null ],
        [ "buffer6_send", "d1/d02/dhcpv6Hooks.html#dhcpv6HooksBuffer6Send", null ],
        [ "lease6_expire", "d1/d02/dhcpv6Hooks.html#dhcpv6HooksLease6Expire", null ],
        [ "lease6_recover", "d1/d02/dhcpv6Hooks.html#dhcpv6HooksLease6Recover", null ],
        [ "command_processed", "d1/d02/dhcpv6Hooks.html#dhcpv6HooksCommandProcessed", null ]
      ] ],
      [ "Accessing DHCPv6 Options within a Packet", "d1/d02/dhcpv6Hooks.html#dhcpv6HooksOptionsAccess", null ]
    ] ],
    [ "The Hooks API for the Control Agent", "d4/d6b/agentHooks.html", [
      [ "Introduction", "d4/d6b/agentHooks.html#agentHooksIntroduction", null ],
      [ "Hooks in the Control Agent", "d4/d6b/agentHooks.html#agentHooksHookPoints", [
        [ "http_auth", "d4/d6b/agentHooks.html#agentHooksAuth", null ],
        [ "http_response", "d4/d6b/agentHooks.html#agentHooksResponse", null ]
      ] ],
      [ "Handle and hook unload", "d4/d6b/agentHooks.html#agentHooksHandle", null ]
    ] ],
    [ "The Hooks API for the Kea DHCP DDNS (D2)", "d4/d7d/d2Hooks.html", [
      [ "Introduction", "d4/d7d/d2Hooks.html#d2HooksIntroduction", null ],
      [ "Hooks in Kea DHCP DDNS", "d4/d7d/d2Hooks.html#d2HooksHookPoints", [
        [ "d2_srv_configured", "d4/d7d/d2Hooks.html#d2HooksD2SrvConfigured", null ],
        [ "select_key", "d4/d7d/d2Hooks.html#d2HooksSelectKey", null ]
      ] ]
    ] ],
    [ "Guide to Hooks for the Kea Component Developer", "dd/dfa/hooksComponentDeveloperGuide.html", [
      [ "Introduction", "dd/dfa/hooksComponentDeveloperGuide.html#hooksComponentIntroduction", [
        [ "Terminology", "dd/dfa/hooksComponentDeveloperGuide.html#hooksComponentTerminology", null ],
        [ "Languages", "dd/dfa/hooksComponentDeveloperGuide.html#hooksComponentLanguages", null ]
      ] ],
      [ "Basic Ideas", "dd/dfa/hooksComponentDeveloperGuide.html#hooksComponentBasicIdeas", null ],
      [ "Determining the Hook Points", "dd/dfa/hooksComponentDeveloperGuide.html#hooksComponentDefinition", null ],
      [ "Naming and Registering the Hooks Points", "dd/dfa/hooksComponentDeveloperGuide.html#hooksComponentRegistration", [
        [ "Automatic Registration of Hooks", "dd/dfa/hooksComponentDeveloperGuide.html#hooksComponentAutomaticRegistration", null ],
        [ "Hook Names", "dd/dfa/hooksComponentDeveloperGuide.html#hooksComponentHookNames", null ]
      ] ],
      [ "Calling Callouts on a Hook", "dd/dfa/hooksComponentDeveloperGuide.html#hooksComponentCallingCallouts", [
        [ "The Callout Handle", "dd/dfa/hooksComponentDeveloperGuide.html#hooksComponentArgument", null ],
        [ "The Skip Flag (obsolete)", "dd/dfa/hooksComponentDeveloperGuide.html#hooksComponentSkipFlag", null ],
        [ "The next step status", "dd/dfa/hooksComponentDeveloperGuide.html#hooksComponentNextStep", null ],
        [ "Getting the Callout Handle", "dd/dfa/hooksComponentDeveloperGuide.html#hooksComponentGettingHandle", null ],
        [ "Calling the Callout", "dd/dfa/hooksComponentDeveloperGuide.html#hooksComponentCallingCallout", [
          [ "Conditionally Calling Hook Callouts", "dd/dfa/hooksComponentDeveloperGuide.html#hooksComponentConditionalCallout", null ]
        ] ]
      ] ],
      [ "Loading the User Libraries", "dd/dfa/hooksComponentDeveloperGuide.html#hooksComponentLoadLibraries", [
        [ "Unload and Reload Issues", "dd/dfa/hooksComponentDeveloperGuide.html#hooksComponentUnloadIssues", null ]
      ] ],
      [ "Component-Defined Callouts", "dd/dfa/hooksComponentDeveloperGuide.html#hooksComponentCallouts", null ]
    ] ],
    [ "Hooks Maintenance Guide", "d6/d6e/hooksmgMaintenanceGuide.html", [
      [ "Introduction", "d6/d6e/hooksmgMaintenanceGuide.html#hooksmgIntroduction", null ],
      [ "Hooks Framework Objects", "d6/d6e/hooksmgMaintenanceGuide.html#hooksmgObjects", [
        [ "User-Side Objects", "d6/d6e/hooksmgMaintenanceGuide.html#hooksmgUserObjects", [
          [ "Callout Handle", "d6/d6e/hooksmgMaintenanceGuide.html#hooksmgCalloutHandle", null ],
          [ "Library Handle", "d6/d6e/hooksmgMaintenanceGuide.html#hooksmgLibraryHandle", null ]
        ] ],
        [ "Server-Side Objects", "d6/d6e/hooksmgMaintenanceGuide.html#hooksmgServerObjects", [
          [ "Server Hooks", "d6/d6e/hooksmgMaintenanceGuide.html#hooksmgServerHooks", null ],
          [ "Library Manager", "d6/d6e/hooksmgMaintenanceGuide.html#hooksmgLibraryManager", null ],
          [ "Library Manager Collection", "d6/d6e/hooksmgMaintenanceGuide.html#hooksmgLibraryManagerCollection", null ],
          [ "Callout Manager", "d6/d6e/hooksmgMaintenanceGuide.html#hooksmgCalloutManager", null ],
          [ "Hooks Manager", "d6/d6e/hooksmgMaintenanceGuide.html#hooksmgHooksManager", null ]
        ] ]
      ] ],
      [ "Other Issues", "d6/d6e/hooksmgMaintenanceGuide.html#hooksmgOtherIssues", [
        [ "Memory Allocation", "d6/d6e/hooksmgMaintenanceGuide.html#hooksmgMemoryAllocation", null ],
        [ "Hooks and Statically-Linked Kea", "d6/d6e/hooksmgMaintenanceGuide.html#hooksmgStaticLinking", null ]
      ] ]
    ] ],
    [ "Kea High Availability Hooks Library", "d3/d96/libdhcp_ha.html", [
      [ "Overview", "d3/d96/libdhcp_ha.html#haOverview", null ],
      [ "Why Hook Library?", "d3/d96/libdhcp_ha.html#haWhyHookLibrary", null ],
      [ "Notable Differences to ISC DHCP", "d3/d96/libdhcp_ha.html#haNotableDifferences", null ],
      [ "Asynchronous Communication with Boost Asio", "d3/d96/libdhcp_ha.html#haAyncCommunication", null ],
      [ "Client Classification in Load Balancing", "d3/d96/libdhcp_ha.html#haClientClassification", null ],
      [ "HA Hooks Library Code Structure", "d3/d96/libdhcp_ha.html#haCodeStructure", [
        [ "HA Service Class", "d3/d96/libdhcp_ha.html#haService", null ],
        [ "HA Implementation Class", "d3/d96/libdhcp_ha.html#haImplementation", null ],
        [ "Query Filter Class", "d3/d96/libdhcp_ha.html#haQueryFilter", null ],
        [ "Communication State Class", "d3/d96/libdhcp_ha.html#haCommunicationState", null ],
        [ "Command Creator Class", "d3/d96/libdhcp_ha.html#haCommandCreator", null ]
      ] ],
      [ "Future HA Hooks Library Improvement Ideas", "d3/d96/libdhcp_ha.html#haShortcomings", [
        [ "Controlling State Machine", "d3/d96/libdhcp_ha.html#haStateMachineControl", null ],
        [ "DNS Updates are not Coordinated", "d3/d96/libdhcp_ha.html#haNameUpdates", null ]
      ] ],
      [ "Multi-Threading Compatibility", "d3/d96/libdhcp_ha.html#haMTCompatibility", null ]
    ] ],
    [ "DHCP User Check Hooks Library", "d8/db2/libdhcp_user_chk.html", [
      [ "user_chk: An Example Hooks Library", "d8/db2/libdhcp_user_chk.html#libdhcp_user_chkIntro", [
        [ "Introduction", "d8/db2/libdhcp_user_chk.html#autotoc_md8", null ],
        [ "User Registry Classes", "d8/db2/libdhcp_user_chk.html#autotoc_md9", null ],
        [ "Callout Processing", "d8/db2/libdhcp_user_chk.html#autotoc_md10", null ],
        [ "Using the library", "d8/db2/libdhcp_user_chk.html#autotoc_md11", [
          [ "Creating the Registry File", "d8/db2/libdhcp_user_chk.html#autotoc_md12", null ],
          [ "Configuring the DHCP Modules", "d8/db2/libdhcp_user_chk.html#autotoc_md13", null ]
        ] ],
        [ "User Check Outcome", "d8/db2/libdhcp_user_chk.html#autotoc_md14", null ]
      ] ],
      [ "Multi-Threading Compatibility", "d8/db2/libdhcp_user_chk.html#libdhcp_user_chkMTCompatibility", null ]
    ] ],
    [ "Kea Lease Commands Hooks Library", "d9/dda/libdhcp_lease_cmds.html", [
      [ "Introduction", "d9/dda/libdhcp_lease_cmds.html#libdhcp_lease_cmdsIntro", null ],
      [ "Lease Commands Overview", "d9/dda/libdhcp_lease_cmds.html#lease_cmds", null ],
      [ "Lease Commands Code Overview", "d9/dda/libdhcp_lease_cmds.html#lease_cmdsCode", null ],
      [ "Lease Commands Design choices", "d9/dda/libdhcp_lease_cmds.html#lease_cmdsDesigns", null ],
      [ "Multi-Threading Compatibility", "d9/dda/libdhcp_lease_cmds.html#lease_cmdsMTCompatibility", null ]
    ] ],
    [ "Kea Stat Commands Hooks Library", "d1/d88/libdhcp_stat_cmds.html", [
      [ "Introduction", "d1/d88/libdhcp_stat_cmds.html#libdhcp_stat_cmdsIntro", null ],
      [ "Stat Commands Overview", "d1/d88/libdhcp_stat_cmds.html#stat_cmds", null ],
      [ "Stat Commands Code Overview", "d1/d88/libdhcp_stat_cmds.html#stat_cmdsCode", [
        [ "Lease Stat Commands Code Overview", "d1/d88/libdhcp_stat_cmds.html#stat_cmdsLeaseStatCode", null ]
      ] ],
      [ "Multi-Threading Compatibility", "d1/d88/libdhcp_stat_cmds.html#stat_cmdsMTCompatibility", null ]
    ] ],
    [ "DHCPv4 Server Component", "d5/ded/dhcp4.html", "d5/ded/dhcp4" ],
    [ "DHCPv4-over-DHCPv6 DHCPv4 Server Side", "dc/db8/dhcpv4o6Dhcp4.html", [
      [ "DHCPv6-to-DHCPv4 Inter Process Communication", "dc/db8/dhcpv4o6Dhcp4.html#Dhcp4to6Ipc", null ],
      [ "DHCPv4-over-DHCPv6 Packet Processing", "dc/db8/dhcpv4o6Dhcp4.html#dhcp4to6Receive", null ],
      [ "Modified DHCPv4 Routines", "dc/db8/dhcpv4o6Dhcp4.html#dhcp4to6Specific", null ]
    ] ],
    [ "DHCPv6 Server Component", "d3/db8/dhcp6.html", "d3/db8/dhcp6" ],
    [ "DHCPv4-over-DHCPv6 DHCPv6 Server Side", "dc/d30/dhcpv4o6Dhcp6.html", [
      [ "DHCPv6-to-DHCPv4 Inter Process Communication", "dc/d30/dhcpv4o6Dhcp6.html#dhcp6to4Ipc", null ],
      [ "DHCPv6-to-DHCPv4 Packet Processing", "dc/d30/dhcpv4o6Dhcp6.html#dhcp6to4Process", null ]
    ] ],
    [ "Congestion Handling in Kea DHCP Servers", "d8/dba/congestionHandling.html", [
      [ "What is Congestion?", "d8/dba/congestionHandling.html#background", null ],
      [ "Congestion Handling Overview", "d8/dba/congestionHandling.html#introduction", null ],
      [ "Custom Packet Queues", "d8/dba/congestionHandling.html#custom-implementations", [
        [ "Developing isc::dhcp::PacketQueue Derivations", "d8/dba/congestionHandling.html#packet-queue-derivation", null ],
        [ "The Basics", "d8/dba/congestionHandling.html#packet-queue-derivation-basics", null ],
        [ "Defining a Factory", "d8/dba/congestionHandling.html#packet-queue-derivation-factory", null ],
        [ "Registering Your Implementation", "d8/dba/congestionHandling.html#packet-queue-registration", null ],
        [ "Configuring Kea to use YourPacketQueue4", "d8/dba/congestionHandling.html#packet-queue-factory", null ],
        [ "DHCPv6 Example Snippets", "d8/dba/congestionHandling.html#packet-queue-example-dhcpv6", null ]
      ] ]
    ] ],
    [ "DHCP-DDNS Component", "d6/d31/d2.html", [
      [ "D2's CPL Derivations", "d6/d31/d2.html#d2ProcessDerivation", null ],
      [ "Configuration Management", "d6/d31/d2.html#d2ConfigMgt", null ],
      [ "Request Reception and Queuing", "d6/d31/d2.html#d2NCRReceipt", null ],
      [ "Update Execution", "d6/d31/d2.html#d2DDNSUpdateExecution", null ],
      [ "Main Event Loop", "d6/d31/d2.html#d2EventLoop", null ],
      [ "Transactions", "d6/d31/d2.html#d2TransDetail", [
        [ "State Model Implementation", "d6/d31/d2.html#d2StateModel", null ],
        [ "Transaction Execution Example", "d6/d31/d2.html#d2TransExecExample", null ]
      ] ]
    ] ],
    [ "Control Agent Component", "d7/dc0/controlAgent.html", [
      [ "Receiving commands over HTTP", "d7/dc0/controlAgent.html#ctrlAgentHttp", null ],
      [ "Creating HTTP responses", "d7/dc0/controlAgent.html#ctrlAgentCreatingResponse", null ],
      [ "Handling commands with Command Manager", "d7/dc0/controlAgent.html#ctrlAgentCommandMgr", null ],
      [ "Security considerations", "d7/dc0/controlAgent.html#CtrlAgentSecurity", null ]
    ] ],
    [ "Lease File Cleanup Component", "d1/d47/lfc.html", [
      [ "Processing", "d1/d47/lfc.html#lfcProcessing", null ],
      [ "File Manipulation", "d1/d47/lfc.html#lfcFiles", null ]
    ] ],
    [ "Control Channel", "d2/d96/ctrlSocket.html", [
      [ "Control Channel Overview", "d2/d96/ctrlSocket.html#ctrlSocketOverview", null ],
      [ "Using UNIX Control Channel", "d2/d96/ctrlSocket.html#ctrlSocketClient", null ],
      [ "Control Channel Implementation", "d2/d96/ctrlSocket.html#ctrlSocketImpl", null ],
      [ "UNIX Control Channel Implementation", "d2/d96/ctrlSocket.html#unixCtrlSocketImpl", null ],
      [ "Accepting connections", "d2/d96/ctrlSocket.html#ctrlSocketConnections", null ],
      [ "Multi-Threading Consideration for Control Channel", "d2/d96/ctrlSocket.html#ctrlSocketMTConsiderations", null ]
    ] ],
    [ "DHCP Database Back-Ends", "d6/dbd/dhcpDatabaseBackends.html", [
      [ "Instantiation of Lease Managers", "d6/dbd/dhcpDatabaseBackends.html#dhcpdb-instantiation", [
        [ "MySQL connection string keywords", "d6/dbd/dhcpDatabaseBackends.html#dhcpdb-keywords-mysql", null ],
        [ "PostgreSQL connection string keywords", "d6/dbd/dhcpDatabaseBackends.html#dhcpdb-keywords-pgsql", [
          [ "Infinite Valid Lifetime", "d6/dbd/dhcpDatabaseBackends.html#infinite-valid-lifetime", null ]
        ] ],
        [ "Memfile Lease Back-End", "d6/dbd/dhcpDatabaseBackends.html#memfile-description", [
          [ "DHCPv4 lease entry format in CSV files", "d6/dbd/dhcpDatabaseBackends.html#lease4-csv", null ]
        ] ],
        [ "DHCPv6 lease entry format in CSV files", "d6/dbd/dhcpDatabaseBackends.html#lease6-csv", null ]
      ] ],
      [ "Host Backends", "d6/dbd/dhcpDatabaseBackends.html#dhcpdb-host", [
        [ "Caching", "d6/dbd/dhcpDatabaseBackends.html#dhcpdb-caching", null ]
      ] ],
      [ "Multi-Threading Consideration for DHCP Database Backends", "d6/dbd/dhcpDatabaseBackends.html#dhcpDatabaseBackendsMTConsiderations", null ]
    ] ],
    [ "Kea Configuration Backends", "d7/dc5/configBackend.html", [
      [ "Introduction", "d7/dc5/configBackend.html#configBackendIntro", null ],
      [ "Alternate Configuration Backends", "d7/dc5/configBackend.html#configBackendHistoric", null ],
      [ "The JSON Configuration Backend", "d7/dc5/configBackend.html#configBackendJSONDesign", null ]
    ] ],
    [ "libkea-util - Kea Utilities Library", "dd/dd6/libutil.html", [
      [ "Utilities", "dd/dd6/libutil.html#utilUtilities", null ],
      [ "Multi-Threading Consideration for Utilities", "dd/dd6/libutil.html#utilMTConsiderations", null ]
    ] ],
    [ "libkea-asiolink - Kea Boost ASIO Library", "d8/d79/libasiolink.html", [
      [ "Boost ASIO Utilities", "d8/d79/libasiolink.html#asiolinkUtilities", null ],
      [ "TLS ASIO support", "d8/d79/libasiolink.html#asiolinkTLSSupport", null ],
      [ "Multi-Threading Consideration for Boost ASIO Utilities", "d8/d79/libasiolink.html#asiolinkMTConsiderations", null ]
    ] ],
    [ "libkea-cc - Kea Configuration Utilities Library", "db/d30/libcc.html", [
      [ "Simple JSON Parser", "db/d30/libcc.html#ccSimpleParser", [
        [ "Default values in Simple Parser", "db/d30/libcc.html#ccSimpleParserDefaults", null ],
        [ "Inheriting parameters between scopes", "db/d30/libcc.html#ccSimpleParserInherits", null ],
        [ "Multi-Threading Consideration for Configuration Utilities", "db/d30/libcc.html#ccMTConsiderations", null ]
      ] ]
    ] ],
    [ "libkea-database - Kea Database Library", "d0/d41/libdatabase.html", [
      [ "Multi-Threading Consideration for Database", "d0/d41/libdatabase.html#databaseMTConsiderations", null ]
    ] ],
    [ "libkea-dhcp++ - Low Level DHCP Library", "d9/d6f/libdhcp.html", [
      [ "Libdhcp++ Library Introduction", "d9/d6f/libdhcp.html#libdhcpIntro", null ],
      [ "DHCPv4-over-DHCPv6 support", "d9/d6f/libdhcp.html#libdhcpDhcp4o6", null ],
      [ "Relay v6 support in Pkt6", "d9/d6f/libdhcp.html#libdhcpRelay", null ],
      [ "Interface Manager", "d9/d6f/libdhcp.html#libdhcpIfaceMgr", null ],
      [ "Switchable Packet Filter objects used by Interface Manager", "d9/d6f/libdhcp.html#libdhcpPktFilter", null ],
      [ "Switchable Packet Filters for DHCPv6", "d9/d6f/libdhcp.html#libdhcpPktFilter6", null ],
      [ "Logging non-fatal errors in IfaceMgr", "d9/d6f/libdhcp.html#libdhcpErrorLogging", null ],
      [ "Multi-Threading Consideration for DHCP library", "d9/d6f/libdhcp.html#libdhcpMTConsiderations", null ]
    ] ],
    [ "libkea-stats - Kea Statistics Library", "dd/d22/libstats.html", [
      [ "Multi-Threading Consideration for Statistics", "dd/d22/libstats.html#statsMTConsiderations", null ]
    ] ],
    [ "libkea-asiodns - Kea I/O Library", "de/dd2/libasiodns.html", [
      [ "Upstream Fetches", "de/dd2/libasiodns.html#autotoc_md15", [
        [ "Sockets", "de/dd2/libasiodns.html#autotoc_md16", null ],
        [ "Fetch Sequence", "de/dd2/libasiodns.html#autotoc_md17", null ]
      ] ]
    ] ],
    [ "libkea-dhcp_ddns - DHCP_DDNS Request I/O Library", "d2/d9a/libdhcp_ddns.html", [
      [ "Introduction", "d2/d9a/libdhcp_ddns.html#libdhcp_ddnsIntro", null ],
      [ "Multi-Threading Consideration for DHCP_DDNS Request I/O Library", "d2/d9a/libdhcp_ddns.html#libdhcp_ddnsMTConsiderations", null ]
    ] ],
    [ "libkea-dhcpsrv - Server DHCP Library", "d8/d5f/libdhcpsrv.html", [
      [ "Global Parameters", "d8/d5f/libdhcpsrv.html#cfgglobals", null ],
      [ "Lease Manager", "d8/d5f/libdhcpsrv.html#leasemgr", null ],
      [ "Configuration Manager", "d8/d5f/libdhcpsrv.html#cfgmgr", null ],
      [ "Host Manager", "d8/d5f/libdhcpsrv.html#hostmgr", [
        [ "PostgreSQL Host Reservation Management", "d8/d5f/libdhcpsrv.html#postgreSQLHostMgr", null ]
      ] ],
      [ "Options Configuration Information", "d8/d5f/libdhcpsrv.html#optionsConfig", null ],
      [ "Allocation Engine", "d8/d5f/libdhcpsrv.html#allocengine", [
        [ "Different lease types support", "d8/d5f/libdhcpsrv.html#allocEngineTypes", null ],
        [ "Prefix Delegation support in AllocEngine", "d8/d5f/libdhcpsrv.html#allocEnginePD", null ],
        [ "Host Reservation support", "d8/d5f/libdhcpsrv.html#allocEngineDHCPv4HostReservation", null ],
        [ "Allocation Engine Cache", "d8/d5f/libdhcpsrv.html#allocEngineReuse", null ]
      ] ],
      [ "Timer Manager", "d8/d5f/libdhcpsrv.html#timerManager", null ],
      [ "Leases Reclamation Routine", "d8/d5f/libdhcpsrv.html#leaseReclamationRoutine", null ],
      [ "Subnet Selection", "d8/d5f/libdhcpsrv.html#subnetSelect", [
        [ "DHCPv4 Subnet Selection", "d8/d5f/libdhcpsrv.html#dhcp4SubnetSelect", null ],
        [ "DHCPv4-over-DHCPv6 Subnet Selection", "d8/d5f/libdhcpsrv.html#dhcp4o6SubnetSelect", null ],
        [ "DHCPv6 Subnet Selection", "d8/d5f/libdhcpsrv.html#dhcp6SubnetSelection", null ]
      ] ],
      [ "DHCPv4-over-DHCPv6 Inter Process Communication", "d8/d5f/libdhcpsrv.html#dhcp4o6Ipc", null ],
      [ "Multi-Threading Consideration for Server DHCP Library", "d8/d5f/libdhcpsrv.html#libdhcpsrvMTConsiderations", null ]
    ] ],
    [ "libkea-eval - Expression Evaluation and Client Classification Library", "dd/d60/libeval.html", [
      [ "Introduction", "dd/d60/libeval.html#dhcpEvalIntroduction", null ],
      [ "Lexer generation using flex", "dd/d60/libeval.html#dhcpEvalLexer", null ],
      [ "Parser generation using bison", "dd/d60/libeval.html#dhcpEvalParser", null ],
      [ "Generating parser files", "dd/d60/libeval.html#dhcpEvalMakefile", null ],
      [ "Configure options", "dd/d60/libeval.html#dhcpEvalConfigure", null ],
      [ "Supported tokens", "dd/d60/libeval.html#dhcpEvalToken", null ],
      [ "Multi-Threading Consideration for Expression Evaluation Library", "dd/d60/libeval.html#dhcpEvalMTConsiderations", null ]
    ] ],
    [ "libkea-process - Controllable Process Layer (CPL)", "de/d7b/libprocess.html", [
      [ "Controllable Process Layer (CPL)", "de/d7b/libprocess.html#cpl", null ],
      [ "CPL Signal Handling", "de/d7b/libprocess.html#cplSignals", null ],
      [ "Redact Passwords", "de/d7b/libprocess.html#redact", null ],
      [ "Multi-Threading Consideration for Controllable Process Layer", "de/d7b/libprocess.html#cplMTConsiderations", null ]
    ] ],
    [ "libkea-yang - Kea YANG Utilities Library", "d7/dc8/libyang.html", [
      [ "Translator between YANG and JSON", "d7/dc8/libyang.html#yangTranslator", null ],
      [ "Pool translator", "d7/dc8/libyang.html#yangTranslatorPool", null ],
      [ "Subnet translator", "d7/dc8/libyang.html#yangTranslatorSubnet", null ],
      [ "Adapting JSON configuration", "d7/dc8/libyang.html#yangAdaptor", null ],
      [ "Running unit tests with Sysrepo", "d7/dc8/libyang.html#unitTestsSysrepo", null ],
      [ "Multi-Threading Consideration for YANG Utilities", "d7/dc8/libyang.html#yangMTConsiderations", null ]
    ] ],
    [ "libkea-http - Kea HTTP Library", "d1/d2a/libhttp.html", [
      [ "Multi-Threading Consideration for HTTP Library", "d1/d2a/libhttp.html#httpMTConsiderations", null ]
    ] ],
    [ "Terminology", "d8/d09/terminology.html", null ],
    [ "ChangeLog", "d5/d6f/changelog.html", null ],
    [ "Flex/Bison Parsers", "d0/d9b/parser.html", [
      [ "Parser background", "d0/d9b/parser.html#parserIntro", null ],
      [ "Flex/Bison Based Parser", "d0/d9b/parser.html#parserBisonIntro", null ],
      [ "Building Flex/Bison Code", "d0/d9b/parser.html#parserBuild", null ],
      [ "Flex Detailed", "d0/d9b/parser.html#parserFlex", null ],
      [ "Bison Grammar", "d0/d9b/parser.html#parserGrammar", null ],
      [ "Generating the Element Tree in Bison", "d0/d9b/parser.html#parserBisonStack", null ],
      [ "Parsing a Partial Configuration", "d0/d9b/parser.html#parserSubgrammar", null ],
      [ "Extending the Grammar", "d0/d9b/parser.html#parserBisonExtend", null ]
    ] ],
    [ "Kea Logging", "d8/d33/logKeaLogging.html", [
      [ "Basic Ideas", "d8/d33/logKeaLogging.html#logBasicIdeas", [
        [ "Message Severity", "d8/d33/logKeaLogging.html#logSeverity", null ],
        [ "Hierarchical Logging System", "d8/d33/logKeaLogging.html#logHierarchical", null ],
        [ "Separation of Messages Use from Message Text", "d8/d33/logKeaLogging.html#logSeparationUseText", null ]
      ] ],
      [ "Using Logging in a Kea Component", "d8/d33/logKeaLogging.html#logDeveloperUse", [
        [ "Create a Message File", "d8/d33/logKeaLogging.html#logMessageFiles", null ],
        [ "Produce Source Files", "d8/d33/logKeaLogging.html#logSourceFiles", [
          [ "Message Compiler", "d8/d33/logKeaLogging.html#logMessageCompiler", null ],
          [ "Include Message Compilation in Makefile", "d8/d33/logKeaLogging.html#logMakefile", null ]
        ] ],
        [ "Using Logging Files in Program Development", "d8/d33/logKeaLogging.html#logUsage", [
          [ "Use in a C++ Program or Module", "d8/d33/logKeaLogging.html#logCppUsage", null ]
        ] ],
        [ "Logging Initialization", "d8/d33/logKeaLogging.html#logInitialization", null ],
        [ "C++ Initialization", "d8/d33/logKeaLogging.html#logInitializationCpp", [
          [ "Variant #1, Used by Production Programs", "d8/d33/logKeaLogging.html#logInitializationCppVariant1", null ],
          [ "Variant #2, Used by Unit Tests", "d8/d33/logKeaLogging.html#logInitializationCppVariant2", null ]
        ] ],
        [ "Hooks Specific Notes", "d8/d33/logKeaLogging.html#logInitializationHooks", null ]
      ] ],
      [ "Notes on the Use of Logging", "d8/d33/logKeaLogging.html#logNotes", null ],
      [ "Multi-Threading Consideration for Logging", "d8/d33/logKeaLogging.html#logMTConsiderations", null ]
    ] ],
    [ "Logging API", "dd/d70/LoggingApi.html", [
      [ "Overview", "dd/d70/LoggingApi.html#LoggingApiOverview", null ],
      [ "Kea Logger Names", "dd/d70/LoggingApi.html#LoggingApiLoggerNames", null ],
      [ "Logging Messages", "dd/d70/LoggingApi.html#LoggingApiLoggingMessages", null ],
      [ "Implementation Issues", "dd/d70/LoggingApi.html#LoggingApiImplementationIssues", null ]
    ] ],
    [ "Kea Cross-Compiling Example", "de/d9a/crossCompile.html", [
      [ "Cross-Compile Tool Chain", "de/d9a/crossCompile.html#toolChain", null ],
      [ "How to get system and packages without a running Raspberry Pi", "de/d9a/crossCompile.html#noRaspberry", null ],
      [ "Usual problems", "de/d9a/crossCompile.html#crossCompilePitfalls", null ],
      [ "Usual Kea Dependencies", "de/d9a/crossCompile.html#raspbianDependencies", null ],
      [ "Prepare Cross Compiling", "de/d9a/crossCompile.html#prepareCrossCompile", null ]
    ] ],
    [ "Debugging Kea", "db/d4d/debug.html", [
      [ "Enabling debug symbols", "db/d4d/debug.html#debugSymbols", null ]
    ] ],
    [ "Building Kea Documentation", "d2/d86/docs.html", [
      [ "Documenting new command", "d2/d86/docs.html#docsNewCommand", null ],
      [ "Generating Developer's Guide", "d2/d86/docs.html#docsDevelGuide", null ]
    ] ]
];