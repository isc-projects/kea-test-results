var option__vendor_8h =
[
    [ "isc::dhcp::OptionVendor", "dc/d08/classisc_1_1dhcp_1_1OptionVendor.html", "dc/d08/classisc_1_1dhcp_1_1OptionVendor" ],
    [ "OptionVendorPtr", "de/dcd/option__vendor_8h.html#a01e455d8ca2c9ff38e5e7f30955533fd", null ],
    [ "VENDOR_CLASS_DATA_LEN_INDEX", "de/dcd/option__vendor_8h.html#a0a39296997dc7a014fb83ad428a8f06a", null ],
    [ "VENDOR_CLASS_ENTERPRISE_ID_INDEX", "de/dcd/option__vendor_8h.html#acf9d4a440ff65449aed5b6bd2141a567", null ],
    [ "VENDOR_CLASS_STRING_INDEX", "de/dcd/option__vendor_8h.html#a606913eece48ef8ca5588f57fa9f7fe3", null ]
];