var classisc_1_1d2_1_1DnsServerInfo =
[
    [ "DnsServerInfo", "de/d30/classisc_1_1d2_1_1DnsServerInfo.html#a73219bd56a8d98d1c57f10bd399a1d4e", null ],
    [ "~DnsServerInfo", "de/d30/classisc_1_1d2_1_1DnsServerInfo.html#af8ab401f55a4c10b33f7ddfcc5ddc3c3", null ],
    [ "disable", "de/d30/classisc_1_1d2_1_1DnsServerInfo.html#ad9d91b1bb3823b531c7059e5448bf72f", null ],
    [ "enable", "de/d30/classisc_1_1d2_1_1DnsServerInfo.html#a8ff05974224d508b0211157f3f72a4f7", null ],
    [ "getHostname", "de/d30/classisc_1_1d2_1_1DnsServerInfo.html#a37ef1a7165a61f2fe951b2f2da146f78", null ],
    [ "getIpAddress", "de/d30/classisc_1_1d2_1_1DnsServerInfo.html#adb4ddc4eabfb1923b4a9deef069cc85c", null ],
    [ "getKeyName", "de/d30/classisc_1_1d2_1_1DnsServerInfo.html#a1aeced80339d9f4b13ddf012b686e397", null ],
    [ "getPort", "de/d30/classisc_1_1d2_1_1DnsServerInfo.html#a8ce592e6f84b2a6ff6a37ad39b6e3a64", null ],
    [ "getTSIGKeyInfo", "de/d30/classisc_1_1d2_1_1DnsServerInfo.html#a2160a31c8c39828ac02505998e2845db", null ],
    [ "isEnabled", "de/d30/classisc_1_1d2_1_1DnsServerInfo.html#a0d3fb2f7ab2c51b82c3589a8acfa5ed7", null ],
    [ "toElement", "de/d30/classisc_1_1d2_1_1DnsServerInfo.html#a89587b81b95047a940c5cd14b8dab756", null ],
    [ "toText", "de/d30/classisc_1_1d2_1_1DnsServerInfo.html#a174fdd8933c99faeadb4743cd2f7fc89", null ]
];