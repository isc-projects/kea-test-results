var host__mgr_8h =
[
    [ "isc::dhcp::HostMgr", "d9/d9a/classisc_1_1dhcp_1_1HostMgr.html", "d9/d9a/classisc_1_1dhcp_1_1HostMgr" ],
    [ "HostMgrOperationTarget", "d8/d2c/host__mgr_8h.html#ae32cfc16322d3b1baf8b8644e4038f08", [
      [ "UNSPECIFIED_SOURCE", "d8/d2c/host__mgr_8h.html#ae32cfc16322d3b1baf8b8644e4038f08a28d9cdd5e91f81ffabee9838ba1322f3", null ],
      [ "PRIMARY_SOURCE", "d8/d2c/host__mgr_8h.html#ae32cfc16322d3b1baf8b8644e4038f08a14da36f1b61bd6e7ade1c2bb0bb08021", null ],
      [ "ALTERNATE_SOURCES", "d8/d2c/host__mgr_8h.html#ae32cfc16322d3b1baf8b8644e4038f08a1708a5bcaadd231c330186f8db12957d", null ],
      [ "ALL_SOURCES", "d8/d2c/host__mgr_8h.html#ae32cfc16322d3b1baf8b8644e4038f08a2e06301285b58a953d950c735c286572", null ]
    ] ]
];