var classisc_1_1db_1_1AuditEntry =
[
    [ "ModificationType", "d8/d73/classisc_1_1db_1_1AuditEntry.html#a1346bd4efd8e7614128ef5ac20c847cc", [
      [ "CREATE", "d8/d73/classisc_1_1db_1_1AuditEntry.html#a1346bd4efd8e7614128ef5ac20c847cca294ce20cdefa29be3be0735cb62e715d", null ],
      [ "UPDATE", "d8/d73/classisc_1_1db_1_1AuditEntry.html#a1346bd4efd8e7614128ef5ac20c847cca15a8022d0ed9cd9c2a2e756822703eb4", null ],
      [ "DELETE", "d8/d73/classisc_1_1db_1_1AuditEntry.html#a1346bd4efd8e7614128ef5ac20c847cca32f68a60cef40faedbc6af20298c1a1e", null ]
    ] ],
    [ "AuditEntry", "d8/d73/classisc_1_1db_1_1AuditEntry.html#ad9d28d296097461b93f7507477ce97b5", null ],
    [ "AuditEntry", "d8/d73/classisc_1_1db_1_1AuditEntry.html#abc2720ebe3ae72b1f1cf33752420b41d", null ],
    [ "getLogMessage", "d8/d73/classisc_1_1db_1_1AuditEntry.html#a60cfa40991e3b85536ff32023b4e2aa2", null ],
    [ "getModificationTime", "d8/d73/classisc_1_1db_1_1AuditEntry.html#a1665c6655db1e42ae2668e9408be9d19", null ],
    [ "getModificationType", "d8/d73/classisc_1_1db_1_1AuditEntry.html#a13ce7ca9fa1b170575c17325d40a9ca9", null ],
    [ "getObjectId", "d8/d73/classisc_1_1db_1_1AuditEntry.html#aa3681ff245743fb1bae3939c8c60a83a", null ],
    [ "getObjectType", "d8/d73/classisc_1_1db_1_1AuditEntry.html#a37a4d4c456f7f6756e63cd268fe524de", null ],
    [ "getRevisionId", "d8/d73/classisc_1_1db_1_1AuditEntry.html#a0ce028b8e98646c7f1e5942503b657ca", null ]
];