var classisc_1_1dns_1_1EDNS =
[
    [ "EDNS", "d8/dd5/classisc_1_1dns_1_1EDNS.html#a0b7d2b5cbaeee2bad0d81b3d5df1c552", null ],
    [ "EDNS", "d8/dd5/classisc_1_1dns_1_1EDNS.html#ae7380da0fa90939ec05be830cd9df51b", null ],
    [ "getDNSSECAwareness", "d8/dd5/classisc_1_1dns_1_1EDNS.html#a35597a388b30c476e2cb0899910bcb75", null ],
    [ "getUDPSize", "d8/dd5/classisc_1_1dns_1_1EDNS.html#ac2b4f6d55dada6f4600c8c6f2fb99a3b", null ],
    [ "getVersion", "d8/dd5/classisc_1_1dns_1_1EDNS.html#a697d9b5bcf078e519638e84a22738505", null ],
    [ "setDNSSECAwareness", "d8/dd5/classisc_1_1dns_1_1EDNS.html#a45ea6858fe0854feba397a7348189e0e", null ],
    [ "setUDPSize", "d8/dd5/classisc_1_1dns_1_1EDNS.html#a0a771d3a66e6ec0be45f81cc36af9971", null ],
    [ "toText", "d8/dd5/classisc_1_1dns_1_1EDNS.html#a30be8c133d5d7acea3361f4366d4560b", null ],
    [ "toWire", "d8/dd5/classisc_1_1dns_1_1EDNS.html#a8f8ab1f1ee25fd762777c70685d3e077", null ],
    [ "toWire", "d8/dd5/classisc_1_1dns_1_1EDNS.html#a4d75e34bee822bc3e8942d56e9554671", null ]
];