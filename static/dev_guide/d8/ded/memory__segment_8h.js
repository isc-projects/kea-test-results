var memory__segment_8h =
[
    [ "isc::util::MemorySegment", "d2/dcc/classisc_1_1util_1_1MemorySegment.html", "d2/dcc/classisc_1_1util_1_1MemorySegment" ],
    [ "isc::util::MemorySegmentError", "d7/d78/classisc_1_1util_1_1MemorySegmentError.html", "d7/d78/classisc_1_1util_1_1MemorySegmentError" ],
    [ "isc::util::MemorySegmentGrown", "d8/dfb/classisc_1_1util_1_1MemorySegmentGrown.html", "d8/dfb/classisc_1_1util_1_1MemorySegmentGrown" ],
    [ "isc::util::MemorySegmentOpenError", "d3/ddf/classisc_1_1util_1_1MemorySegmentOpenError.html", "d3/ddf/classisc_1_1util_1_1MemorySegmentOpenError" ]
];