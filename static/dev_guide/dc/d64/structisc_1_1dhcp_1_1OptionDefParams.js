var structisc_1_1dhcp_1_1OptionDefParams =
[
    [ "array", "dc/d64/structisc_1_1dhcp_1_1OptionDefParams.html#a5b6f436f9defff40afe16142587dca94", null ],
    [ "code", "dc/d64/structisc_1_1dhcp_1_1OptionDefParams.html#a587e4821d03854cfd93aa7e98e175fd7", null ],
    [ "encapsulates", "dc/d64/structisc_1_1dhcp_1_1OptionDefParams.html#a397bfa6b24505e6bd727f444cea650a5", null ],
    [ "name", "dc/d64/structisc_1_1dhcp_1_1OptionDefParams.html#aadf24af942ab50981a62f3d770ceed5e", null ],
    [ "records", "dc/d64/structisc_1_1dhcp_1_1OptionDefParams.html#abfd023bf03a793a5a3a3910f13362c98", null ],
    [ "records_size", "dc/d64/structisc_1_1dhcp_1_1OptionDefParams.html#af1c77c447d61a0f6d78d91f089507522", null ],
    [ "space", "dc/d64/structisc_1_1dhcp_1_1OptionDefParams.html#a7616d879600035ee7c99750eaa0c4a87", null ],
    [ "type", "dc/d64/structisc_1_1dhcp_1_1OptionDefParams.html#a3d53eac0352993a76f189d2a837147d8", null ]
];