var std__option__defs_8h =
[
    [ "CABLELABS_CLIENT_CONF_SPACE", "dc/d5d/std__option__defs_8h.html#af116881aad743289f8eb04ba1412c885", null ],
    [ "DHCP4_OPTION_SPACE", "dc/d5d/std__option__defs_8h.html#a1a456c40e861c6d71823e796488d97dd", null ],
    [ "DHCP6_OPTION_SPACE", "dc/d5d/std__option__defs_8h.html#ab8a326c817ac64bf910ab8fd4587a30d", null ],
    [ "DHCP_AGENT_OPTION_SPACE", "dc/d5d/std__option__defs_8h.html#a91734d411b5319146e3dba2dcdb4e152", null ],
    [ "ISC_V6_OPTION_SPACE", "dc/d5d/std__option__defs_8h.html#a3db70a4679ed4df4e92ccd85e32209a3", null ],
    [ "LAST_RESORT_V4_OPTION_SPACE", "dc/d5d/std__option__defs_8h.html#a1073fa41c41469a0045be0f262236643", null ],
    [ "LW_V6_OPTION_SPACE", "dc/d5d/std__option__defs_8h.html#aa0cf31eb8e71428fcdf67aad9c32f98c", null ],
    [ "MAPE_V6_OPTION_SPACE", "dc/d5d/std__option__defs_8h.html#a8b27b748fa919201ac7d6344815ea8e4", null ],
    [ "MAPT_V6_OPTION_SPACE", "dc/d5d/std__option__defs_8h.html#acb005333483863fe1209eca45ba78cb3", null ],
    [ "NO_RECORD_DEF", "dc/d5d/std__option__defs_8h.html#a6af9effc3fb03e70684b4664055f61d6", null ],
    [ "RECORD_DECL", "dc/d5d/std__option__defs_8h.html#a3ac5145ea42e5df91b5ad435c66bff2d", null ],
    [ "RECORD_DEF", "dc/d5d/std__option__defs_8h.html#a96a25311cf417e2b3f556a48e50c93c1", null ],
    [ "V4V6_BIND_OPTION_SPACE", "dc/d5d/std__option__defs_8h.html#a0d3eb0fc7e4dd4e4b61adcf685c05f32", null ],
    [ "V4V6_RULE_OPTION_SPACE", "dc/d5d/std__option__defs_8h.html#a87c0e4a6fa2a4c693300c092762c63fb", null ],
    [ "V6_NTP_SERVER_SPACE", "dc/d5d/std__option__defs_8h.html#a5132d375957c1715ba8567236f837f82", null ],
    [ "VENDOR_ENCAPSULATED_OPTION_SPACE", "dc/d5d/std__option__defs_8h.html#a83f23dbbb131da38ea546f76f4ed6f44", null ]
];