var lease__mgr_8h =
[
    [ "isc::dhcp::LeaseMgr", "d1/d11/classisc_1_1dhcp_1_1LeaseMgr.html", "d1/d11/classisc_1_1dhcp_1_1LeaseMgr" ],
    [ "isc::dhcp::LeasePageSize", "d9/d51/classisc_1_1dhcp_1_1LeasePageSize.html", "d9/d51/classisc_1_1dhcp_1_1LeasePageSize" ],
    [ "isc::dhcp::LeaseStatsQuery", "d8/de2/classisc_1_1dhcp_1_1LeaseStatsQuery.html", "d8/de2/classisc_1_1dhcp_1_1LeaseStatsQuery" ],
    [ "isc::dhcp::LeaseStatsRow", "db/da1/structisc_1_1dhcp_1_1LeaseStatsRow.html", "db/da1/structisc_1_1dhcp_1_1LeaseStatsRow" ],
    [ "LeaseStatsQueryPtr", "dc/dc3/lease__mgr_8h.html#a4af6dbf3ac93587012b3f2a69a79cbc3", null ],
    [ "LeaseStatsRowPtr", "dc/dc3/lease__mgr_8h.html#a590d6dff1b3424f906bb47397a81d131", null ],
    [ "VersionPair", "dc/dc3/lease__mgr_8h.html#a1dbce930b19c2a5aa9bff33b3781775e", null ]
];