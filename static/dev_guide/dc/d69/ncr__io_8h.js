var ncr__io_8h =
[
    [ "isc::dhcp_ddns::NameChangeListener", "d2/dfe/classisc_1_1dhcp__ddns_1_1NameChangeListener.html", "d2/dfe/classisc_1_1dhcp__ddns_1_1NameChangeListener" ],
    [ "isc::dhcp_ddns::NameChangeSender", "df/d1c/classisc_1_1dhcp__ddns_1_1NameChangeSender.html", "df/d1c/classisc_1_1dhcp__ddns_1_1NameChangeSender" ],
    [ "isc::dhcp_ddns::NcrListenerError", "d5/d25/classisc_1_1dhcp__ddns_1_1NcrListenerError.html", "d5/d25/classisc_1_1dhcp__ddns_1_1NcrListenerError" ],
    [ "isc::dhcp_ddns::NcrListenerOpenError", "de/d8f/classisc_1_1dhcp__ddns_1_1NcrListenerOpenError.html", "de/d8f/classisc_1_1dhcp__ddns_1_1NcrListenerOpenError" ],
    [ "isc::dhcp_ddns::NcrListenerReceiveError", "d4/de0/classisc_1_1dhcp__ddns_1_1NcrListenerReceiveError.html", "d4/de0/classisc_1_1dhcp__ddns_1_1NcrListenerReceiveError" ],
    [ "isc::dhcp_ddns::NcrSenderError", "df/d84/classisc_1_1dhcp__ddns_1_1NcrSenderError.html", "df/d84/classisc_1_1dhcp__ddns_1_1NcrSenderError" ],
    [ "isc::dhcp_ddns::NcrSenderOpenError", "d9/dcc/classisc_1_1dhcp__ddns_1_1NcrSenderOpenError.html", "d9/dcc/classisc_1_1dhcp__ddns_1_1NcrSenderOpenError" ],
    [ "isc::dhcp_ddns::NcrSenderQueueFull", "d7/d95/classisc_1_1dhcp__ddns_1_1NcrSenderQueueFull.html", "d7/d95/classisc_1_1dhcp__ddns_1_1NcrSenderQueueFull" ],
    [ "isc::dhcp_ddns::NcrSenderSendError", "df/d75/classisc_1_1dhcp__ddns_1_1NcrSenderSendError.html", "df/d75/classisc_1_1dhcp__ddns_1_1NcrSenderSendError" ],
    [ "isc::dhcp_ddns::NameChangeListener::RequestReceiveHandler", "de/d25/classisc_1_1dhcp__ddns_1_1NameChangeListener_1_1RequestReceiveHandler.html", "de/d25/classisc_1_1dhcp__ddns_1_1NameChangeListener_1_1RequestReceiveHandler" ],
    [ "isc::dhcp_ddns::NameChangeSender::RequestSendHandler", "d7/d00/classisc_1_1dhcp__ddns_1_1NameChangeSender_1_1RequestSendHandler.html", "d7/d00/classisc_1_1dhcp__ddns_1_1NameChangeSender_1_1RequestSendHandler" ],
    [ "NameChangeListenerPtr", "dc/d69/ncr__io_8h.html#a70560723da6ab6879d96e837ce5188f9", null ],
    [ "NameChangeSenderPtr", "dc/d69/ncr__io_8h.html#ab389567d5f3bfb53e94ccb7c55572217", null ],
    [ "NameChangeProtocol", "dc/d69/ncr__io_8h.html#a7eaf9fc79304f2c7c303f08da6ec5dce", [
      [ "NCR_UDP", "dc/d69/ncr__io_8h.html#a7eaf9fc79304f2c7c303f08da6ec5dcea402a6288a659edcfc8873f808b5c00f6", null ],
      [ "NCR_TCP", "dc/d69/ncr__io_8h.html#a7eaf9fc79304f2c7c303f08da6ec5dcea3a4ca1baef96479f9cf90ce0bd30bccb", null ]
    ] ],
    [ "ncrProtocolToString", "dc/d69/ncr__io_8h.html#a940a71481b7df31ef40d17b4599da7bf", null ],
    [ "stringToNcrProtocol", "dc/d69/ncr__io_8h.html#a29863aac5a4e0154c9c687f86bd9190c", null ]
];