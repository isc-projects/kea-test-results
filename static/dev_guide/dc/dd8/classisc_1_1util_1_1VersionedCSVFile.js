var classisc_1_1util_1_1VersionedCSVFile =
[
    [ "InputSchemaState", "dc/dd8/classisc_1_1util_1_1VersionedCSVFile.html#a2b46f4d4d4fe9e71ebe607560ac3cac8", [
      [ "CURRENT", "dc/dd8/classisc_1_1util_1_1VersionedCSVFile.html#a2b46f4d4d4fe9e71ebe607560ac3cac8ac9bbe4a15e30b304161fd1bc8862d196", null ],
      [ "NEEDS_UPGRADE", "dc/dd8/classisc_1_1util_1_1VersionedCSVFile.html#a2b46f4d4d4fe9e71ebe607560ac3cac8a26379054dae99f766101dbb17e54a8e3", null ],
      [ "NEEDS_DOWNGRADE", "dc/dd8/classisc_1_1util_1_1VersionedCSVFile.html#a2b46f4d4d4fe9e71ebe607560ac3cac8a2eb0e0aa5251db22c1cab8a9bb55b1fc", null ]
    ] ],
    [ "VersionedCSVFile", "dc/dd8/classisc_1_1util_1_1VersionedCSVFile.html#a504b1ca38c6ca5e1b93f2ec2da3ce7a0", null ],
    [ "~VersionedCSVFile", "dc/dd8/classisc_1_1util_1_1VersionedCSVFile.html#a19ef2e72a4b137bd70b9d586045d9029", null ],
    [ "addColumn", "dc/dd8/classisc_1_1util_1_1VersionedCSVFile.html#abbe5134d7ce04100f24ad578eb63bc53", null ],
    [ "columnCountError", "dc/dd8/classisc_1_1util_1_1VersionedCSVFile.html#ad9441a273d52b99eb3aa4497afa3d52b", null ],
    [ "getInputHeaderCount", "dc/dd8/classisc_1_1util_1_1VersionedCSVFile.html#a320df715b2ded299e2a589b98fb98384", null ],
    [ "getInputSchemaState", "dc/dd8/classisc_1_1util_1_1VersionedCSVFile.html#ae6b50ca3bb5e420bfa366e8f88873208", null ],
    [ "getInputSchemaVersion", "dc/dd8/classisc_1_1util_1_1VersionedCSVFile.html#a8a7c5f83f2f5dbc97596621f84f232ef", null ],
    [ "getMinimumValidColumns", "dc/dd8/classisc_1_1util_1_1VersionedCSVFile.html#ac1bb3a25f5ab928857d6c9d7541023e0", null ],
    [ "getSchemaVersion", "dc/dd8/classisc_1_1util_1_1VersionedCSVFile.html#a2b3cdae6cff00c10d506851d32733133", null ],
    [ "getValidColumnCount", "dc/dd8/classisc_1_1util_1_1VersionedCSVFile.html#ae1005279876442cc932ad3a6a2294e86", null ],
    [ "getVersionedColumn", "dc/dd8/classisc_1_1util_1_1VersionedCSVFile.html#a1aae6d68fd2c704b3eaaf6488ed26156", null ],
    [ "needsConversion", "dc/dd8/classisc_1_1util_1_1VersionedCSVFile.html#a0f2c361fa6612d9b969f6ade372c063d", null ],
    [ "next", "dc/dd8/classisc_1_1util_1_1VersionedCSVFile.html#aad1c5c9235dec54c8940c4e01c2cc42a", null ],
    [ "open", "dc/dd8/classisc_1_1util_1_1VersionedCSVFile.html#a110b09dafdbb91ce40e6587d117deee8", null ],
    [ "recreate", "dc/dd8/classisc_1_1util_1_1VersionedCSVFile.html#a9b31e8e67e749a65d6c268d768f6d52d", null ],
    [ "setMinimumValidColumns", "dc/dd8/classisc_1_1util_1_1VersionedCSVFile.html#a9fda06681db5ba0991850e260ce180f1", null ],
    [ "validateHeader", "dc/dd8/classisc_1_1util_1_1VersionedCSVFile.html#a3dd2ecf8dff67f2b12689d8bdc9cbf38", null ]
];