var command__options_8h =
[
    [ "isc::perfdhcp::CommandOptions", "d1/d86/classisc_1_1perfdhcp_1_1CommandOptions.html", "d1/d86/classisc_1_1perfdhcp_1_1CommandOptions" ],
    [ "isc::perfdhcp::CommandOptions::LeaseType", "d7/dbf/classisc_1_1perfdhcp_1_1CommandOptions_1_1LeaseType.html", "d7/dbf/classisc_1_1perfdhcp_1_1CommandOptions_1_1LeaseType" ],
    [ "Scenario", "dc/d2c/command__options_8h.html#afd33fc47ae04232efa0cbc86448efe21", [
      [ "BASIC", "dc/d2c/command__options_8h.html#afd33fc47ae04232efa0cbc86448efe21ae4ac03f6c9f00665644e868dd1fb9f1e", null ],
      [ "AVALANCHE", "dc/d2c/command__options_8h.html#afd33fc47ae04232efa0cbc86448efe21ab91c774ee71554933cd7955d1b423d7b", null ]
    ] ]
];