var classisc_1_1hooks_1_1ServerHooks =
[
    [ "findIndex", "dc/d4b/classisc_1_1hooks_1_1ServerHooks.html#a5dc264085028bb663dc9fd08cac3934b", null ],
    [ "getCount", "dc/d4b/classisc_1_1hooks_1_1ServerHooks.html#a6dc45b2ff9a91eacb1e4c9ab36e9fce0", null ],
    [ "getHookNames", "dc/d4b/classisc_1_1hooks_1_1ServerHooks.html#aeafe4dbf0a13b52dc4817e8e8f9b84ce", null ],
    [ "getIndex", "dc/d4b/classisc_1_1hooks_1_1ServerHooks.html#a14f7217a0d916a49994dc7b6aadf9628", null ],
    [ "getName", "dc/d4b/classisc_1_1hooks_1_1ServerHooks.html#aeec7d1019d6b20171a161cee63ebc1c5", null ],
    [ "getParkingLotPtr", "dc/d4b/classisc_1_1hooks_1_1ServerHooks.html#a0a887afaeaea4d85b4ca18bd1e3d0eb8", null ],
    [ "getParkingLotPtr", "dc/d4b/classisc_1_1hooks_1_1ServerHooks.html#af6c18dd4df0b6640d7faccfa1675df0c", null ],
    [ "getParkingLotsPtr", "dc/d4b/classisc_1_1hooks_1_1ServerHooks.html#a5c504008b6829e1a0798a51bf698dcad", null ],
    [ "registerHook", "dc/d4b/classisc_1_1hooks_1_1ServerHooks.html#ad58fc76a6f5f912082a465e5a3a413d2", null ],
    [ "reset", "dc/d4b/classisc_1_1hooks_1_1ServerHooks.html#aa5da543f2719beea9274b14027c00a39", null ]
];