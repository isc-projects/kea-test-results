var structisc_1_1log_1_1OutputOption =
[
    [ "Destination", "db/d33/structisc_1_1log_1_1OutputOption.html#a3a29edc675a945c4adcf77c4e94026a1", [
      [ "DEST_CONSOLE", "db/d33/structisc_1_1log_1_1OutputOption.html#a3a29edc675a945c4adcf77c4e94026a1a16b4515db6b5f5672e133091529066ce", null ],
      [ "DEST_FILE", "db/d33/structisc_1_1log_1_1OutputOption.html#a3a29edc675a945c4adcf77c4e94026a1a38d6b87e0ef8cbe6dc8f0cdcf2317755", null ],
      [ "DEST_SYSLOG", "db/d33/structisc_1_1log_1_1OutputOption.html#a3a29edc675a945c4adcf77c4e94026a1a38bd3462cc7e65aeea138b73eed57436", null ]
    ] ],
    [ "Stream", "db/d33/structisc_1_1log_1_1OutputOption.html#a064622093854f27830758d4ffa0d5ee7", [
      [ "STR_STDOUT", "db/d33/structisc_1_1log_1_1OutputOption.html#a064622093854f27830758d4ffa0d5ee7ab78ea2af996ba60db09e699897f09e5d", null ],
      [ "STR_STDERR", "db/d33/structisc_1_1log_1_1OutputOption.html#a064622093854f27830758d4ffa0d5ee7abc3347ad3d1a97c616e3bfe833d74da3", null ]
    ] ],
    [ "OutputOption", "db/d33/structisc_1_1log_1_1OutputOption.html#a743833c8b5f61d17a0db7620da114d6b", null ],
    [ "destination", "db/d33/structisc_1_1log_1_1OutputOption.html#a0acbff4c5b762ca5fcec7f3d3a444a36", null ],
    [ "facility", "db/d33/structisc_1_1log_1_1OutputOption.html#aa289dfe3e4e0a51081121f9bc214aa7f", null ],
    [ "filename", "db/d33/structisc_1_1log_1_1OutputOption.html#ae5231cd0a72b9488bad7f923dd61defa", null ],
    [ "flush", "db/d33/structisc_1_1log_1_1OutputOption.html#a491cbfa2c2583472334968776146b86d", null ],
    [ "maxsize", "db/d33/structisc_1_1log_1_1OutputOption.html#aa368e78bacbf2a381bc9f696df089c66", null ],
    [ "maxver", "db/d33/structisc_1_1log_1_1OutputOption.html#a77659d24387b067618a70f938bd5d18f", null ],
    [ "pattern", "db/d33/structisc_1_1log_1_1OutputOption.html#a1b7a7da1c0de5bcbec22850c50a4c1be", null ],
    [ "stream", "db/d33/structisc_1_1log_1_1OutputOption.html#ab8c43d4ade95e95420951bed5bbaeead", null ]
];