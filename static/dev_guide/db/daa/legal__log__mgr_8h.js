var legal__log__mgr_8h =
[
    [ "isc::dhcp::LegalLogMgr", "d0/d3f/classisc_1_1dhcp_1_1LegalLogMgr.html", "d0/d3f/classisc_1_1dhcp_1_1LegalLogMgr" ],
    [ "isc::dhcp::LegalLogMgrError", "da/d4f/classisc_1_1dhcp_1_1LegalLogMgrError.html", "da/d4f/classisc_1_1dhcp_1_1LegalLogMgrError" ],
    [ "LegalLogMgrPool", "db/daa/legal__log__mgr_8h.html#aa95cb2300a04491e2412c35c17976852", null ],
    [ "LegalLogMgrPtr", "db/daa/legal__log__mgr_8h.html#a40c0ffe3e432dce6a1b0e585879b5189", null ],
    [ "ManagerID", "db/daa/legal__log__mgr_8h.html#adfdb787d4d92dcc233530a37a6c8122e", null ],
    [ "Action", "db/daa/legal__log__mgr_8h.html#af6e12b3898457145c2671886bf4258c7", [
      [ "ASSIGN", "db/daa/legal__log__mgr_8h.html#af6e12b3898457145c2671886bf4258c7affd6976a2b4f6934eb075d0013316ff1", null ],
      [ "RELEASE", "db/daa/legal__log__mgr_8h.html#af6e12b3898457145c2671886bf4258c7a7d649ef069df9885e382417c79f3d5cd", null ]
    ] ],
    [ "actionToVerb", "db/daa/legal__log__mgr_8h.html#a6181aa3e4e062c7168ca107a0062ab8c", null ]
];