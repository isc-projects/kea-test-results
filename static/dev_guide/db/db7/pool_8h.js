var pool_8h =
[
    [ "isc::dhcp::Pool", "d2/d7f/classisc_1_1dhcp_1_1Pool.html", "d2/d7f/classisc_1_1dhcp_1_1Pool" ],
    [ "isc::dhcp::Pool4", "d5/d03/classisc_1_1dhcp_1_1Pool4.html", "d5/d03/classisc_1_1dhcp_1_1Pool4" ],
    [ "isc::dhcp::Pool6", "d8/d3e/classisc_1_1dhcp_1_1Pool6.html", "d8/d3e/classisc_1_1dhcp_1_1Pool6" ],
    [ "Pool4Ptr", "db/db7/pool_8h.html#a06d2ef5352f46fe8f5fafe7b7d234691", null ],
    [ "Pool6Ptr", "db/db7/pool_8h.html#afe7b42d3952ae9daa4e15d1120e9f3aa", null ],
    [ "PoolCollection", "db/db7/pool_8h.html#a5f444632007fc7fd79a2eedd2eff5985", null ],
    [ "PoolPtr", "db/db7/pool_8h.html#a8ab796bfe57f4989fd76c308d5573d25", null ]
];