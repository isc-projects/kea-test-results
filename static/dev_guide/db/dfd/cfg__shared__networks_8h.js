var cfg__shared__networks_8h =
[
    [ "isc::dhcp::CfgSharedNetworks< SharedNetworkPtrType, SharedNetworkCollection >", "d4/d81/classisc_1_1dhcp_1_1CfgSharedNetworks.html", "d4/d81/classisc_1_1dhcp_1_1CfgSharedNetworks" ],
    [ "isc::dhcp::CfgSharedNetworks4", "da/dd7/classisc_1_1dhcp_1_1CfgSharedNetworks4.html", "da/dd7/classisc_1_1dhcp_1_1CfgSharedNetworks4" ],
    [ "isc::dhcp::CfgSharedNetworks6", "d4/d05/classisc_1_1dhcp_1_1CfgSharedNetworks6.html", null ],
    [ "CfgSharedNetworks4Ptr", "db/dfd/cfg__shared__networks_8h.html#accdecf08666f0d6a1705e80c89dea679", null ],
    [ "CfgSharedNetworks6Ptr", "db/dfd/cfg__shared__networks_8h.html#a9611f3f224e5b496747c52731c09ddb3", null ]
];