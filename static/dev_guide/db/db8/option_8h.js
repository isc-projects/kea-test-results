var option_8h =
[
    [ "isc::dhcp::Option", "d9/dd5/classisc_1_1dhcp_1_1Option.html", "d9/dd5/classisc_1_1dhcp_1_1Option" ],
    [ "isc::dhcp::OptionParseError", "db/d4d/classisc_1_1dhcp_1_1OptionParseError.html", "db/d4d/classisc_1_1dhcp_1_1OptionParseError" ],
    [ "isc::dhcp::SkipRemainingOptionsError", "dc/df1/classisc_1_1dhcp_1_1SkipRemainingOptionsError.html", "dc/df1/classisc_1_1dhcp_1_1SkipRemainingOptionsError" ],
    [ "isc::dhcp::SkipThisOptionError", "da/db1/classisc_1_1dhcp_1_1SkipThisOptionError.html", "da/db1/classisc_1_1dhcp_1_1SkipThisOptionError" ],
    [ "OptionBuffer", "db/db8/option_8h.html#aca7e9ef2dc2ba7871a7042ca8438b0f3", null ],
    [ "OptionBufferConstIter", "db/db8/option_8h.html#a47fd5c3a503d201cb53760db4e115669", null ],
    [ "OptionBufferIter", "db/db8/option_8h.html#aa77e8b70869ef95c4e2bc745b230d8e6", null ],
    [ "OptionBufferPtr", "db/db8/option_8h.html#a57d1e7c48a2df7ddd7bf34a08dbe2f75", null ],
    [ "OptionCollection", "db/db8/option_8h.html#a544b867e744feb23e56dcd3f4b5b609b", null ],
    [ "OptionCollectionPtr", "db/db8/option_8h.html#a0d186ecd24a6cedc5e67ca134d63f79f", null ],
    [ "OptionPtr", "db/db8/option_8h.html#af27d4b9ed0518a61b48f401c298108b7", null ]
];