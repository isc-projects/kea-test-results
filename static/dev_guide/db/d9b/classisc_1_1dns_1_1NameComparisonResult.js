var classisc_1_1dns_1_1NameComparisonResult =
[
    [ "NameRelation", "db/d9b/classisc_1_1dns_1_1NameComparisonResult.html#abe90d2c11d20eaa5dfd713f845808fc6", [
      [ "SUPERDOMAIN", "db/d9b/classisc_1_1dns_1_1NameComparisonResult.html#abe90d2c11d20eaa5dfd713f845808fc6a3b8998ca33a4bc0c3d8dde1977d54c3e", null ],
      [ "SUBDOMAIN", "db/d9b/classisc_1_1dns_1_1NameComparisonResult.html#abe90d2c11d20eaa5dfd713f845808fc6a29065ce7416dc1949a41466ad0295c7e", null ],
      [ "EQUAL", "db/d9b/classisc_1_1dns_1_1NameComparisonResult.html#abe90d2c11d20eaa5dfd713f845808fc6ab3fe776969dacc24857ec41bbccd38c4", null ],
      [ "COMMONANCESTOR", "db/d9b/classisc_1_1dns_1_1NameComparisonResult.html#abe90d2c11d20eaa5dfd713f845808fc6a2a986aa6eff4cf4f0d8cc2b90de3a304", null ],
      [ "NONE", "db/d9b/classisc_1_1dns_1_1NameComparisonResult.html#abe90d2c11d20eaa5dfd713f845808fc6ae068ff4d9244b6a1f5ec6e397be02fcb", null ]
    ] ],
    [ "NameComparisonResult", "db/d9b/classisc_1_1dns_1_1NameComparisonResult.html#a70422536ffdf508162bd98072aed4521", null ],
    [ "getCommonLabels", "db/d9b/classisc_1_1dns_1_1NameComparisonResult.html#a04fec3d05b5be5797139a0645c1d1a5c", null ],
    [ "getOrder", "db/d9b/classisc_1_1dns_1_1NameComparisonResult.html#a4fe280a2923a411f85fd4311b9b6c57d", null ],
    [ "getRelation", "db/d9b/classisc_1_1dns_1_1NameComparisonResult.html#a8772555797ab33bf8c9e5fee1956722f", null ]
];