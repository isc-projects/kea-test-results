var alloc__engine__messages_8cc =
[
    [ "ALLOC_ENGINE_IGNORING_UNSUITABLE_GLOBAL_ADDRESS", "dd/d7d/alloc__engine__messages_8cc.html#a38bd7c2d2644c1bcb1e198718696fb97", null ],
    [ "ALLOC_ENGINE_IGNORING_UNSUITABLE_GLOBAL_ADDRESS6", "dd/d7d/alloc__engine__messages_8cc.html#a03f6cdd03d51c1dac50ff03cd7ade100", null ],
    [ "ALLOC_ENGINE_LEASE_RECLAIMED", "dd/d7d/alloc__engine__messages_8cc.html#a2bfc2faf27e03b09f0139a2cca4aec23", null ],
    [ "ALLOC_ENGINE_V4_ALLOC_ERROR", "dd/d7d/alloc__engine__messages_8cc.html#a1e639782c3d97d9a21d7320c9e0ad0ae", null ],
    [ "ALLOC_ENGINE_V4_ALLOC_FAIL", "dd/d7d/alloc__engine__messages_8cc.html#a3fb96fe0c592d4c314d2eab63d912d93", null ],
    [ "ALLOC_ENGINE_V4_ALLOC_FAIL_CLASSES", "dd/d7d/alloc__engine__messages_8cc.html#afc934a3af5f3f4fb7c91c3c005a96c54", null ],
    [ "ALLOC_ENGINE_V4_ALLOC_FAIL_NO_POOLS", "dd/d7d/alloc__engine__messages_8cc.html#a02ca7d6fc44e0e272971ddc0e3d7415d", null ],
    [ "ALLOC_ENGINE_V4_ALLOC_FAIL_SHARED_NETWORK", "dd/d7d/alloc__engine__messages_8cc.html#afc5d0e018ea400dd21b3927634dded36", null ],
    [ "ALLOC_ENGINE_V4_ALLOC_FAIL_SUBNET", "dd/d7d/alloc__engine__messages_8cc.html#ae0e8ff86c19ad65b194d0ce3d0296cc6", null ],
    [ "ALLOC_ENGINE_V4_DECLINED_RECOVERED", "dd/d7d/alloc__engine__messages_8cc.html#a49c7510b21d5b8b28f728f57b7d2cff8", null ],
    [ "ALLOC_ENGINE_V4_DISCOVER_ADDRESS_CONFLICT", "dd/d7d/alloc__engine__messages_8cc.html#ae1084db9e2239658a2ac058c6f0b78cc", null ],
    [ "ALLOC_ENGINE_V4_DISCOVER_HR", "dd/d7d/alloc__engine__messages_8cc.html#a6d2515ef27c603954834e8170bba5ef0", null ],
    [ "ALLOC_ENGINE_V4_LEASE_RECLAIM", "dd/d7d/alloc__engine__messages_8cc.html#a6c7e0ab512f6eb897785c90e9cf6b2e2", null ],
    [ "ALLOC_ENGINE_V4_LEASE_RECLAMATION_FAILED", "dd/d7d/alloc__engine__messages_8cc.html#a016b5e9f1e16f92a5dcf7789ed8eb0e5", null ],
    [ "ALLOC_ENGINE_V4_LEASES_RECLAMATION_COMPLETE", "dd/d7d/alloc__engine__messages_8cc.html#a34edf2e5149d488e8ab1504183fb8af1", null ],
    [ "ALLOC_ENGINE_V4_LEASES_RECLAMATION_FAILED", "dd/d7d/alloc__engine__messages_8cc.html#ab6fb5ed001c3783f919f1b6464dfe27b", null ],
    [ "ALLOC_ENGINE_V4_LEASES_RECLAMATION_SLOW", "dd/d7d/alloc__engine__messages_8cc.html#a4e3ebcd57bca25fd1f080cf13a6a2c65", null ],
    [ "ALLOC_ENGINE_V4_LEASES_RECLAMATION_START", "dd/d7d/alloc__engine__messages_8cc.html#a5939912f857057c4c2013768c93a79f4", null ],
    [ "ALLOC_ENGINE_V4_LEASES_RECLAMATION_TIMEOUT", "dd/d7d/alloc__engine__messages_8cc.html#a5024eb90babea3f5f5b9f7d0a7b34784", null ],
    [ "ALLOC_ENGINE_V4_NO_MORE_EXPIRED_LEASES", "dd/d7d/alloc__engine__messages_8cc.html#a9befba63653644718c89d4547edfdcca", null ],
    [ "ALLOC_ENGINE_V4_OFFER_EXISTING_LEASE", "dd/d7d/alloc__engine__messages_8cc.html#a8917a4216d1d0e2c0e8f5ec5744ee052", null ],
    [ "ALLOC_ENGINE_V4_OFFER_NEW_LEASE", "dd/d7d/alloc__engine__messages_8cc.html#aa334c4797bff4f2c52abcf55114faa1c", null ],
    [ "ALLOC_ENGINE_V4_OFFER_REQUESTED_LEASE", "dd/d7d/alloc__engine__messages_8cc.html#a29a8992801a0b60dcc28490988b6e4a7", null ],
    [ "ALLOC_ENGINE_V4_RECLAIMED_LEASES_DELETE", "dd/d7d/alloc__engine__messages_8cc.html#a7f27942f9f713883f5e1c1ea91cae525", null ],
    [ "ALLOC_ENGINE_V4_RECLAIMED_LEASES_DELETE_COMPLETE", "dd/d7d/alloc__engine__messages_8cc.html#a38d7c7725f65e7596f8678eca6dd4ba9", null ],
    [ "ALLOC_ENGINE_V4_RECLAIMED_LEASES_DELETE_FAILED", "dd/d7d/alloc__engine__messages_8cc.html#ab24696e49729ade4465f5b0fb0e3cbfd", null ],
    [ "ALLOC_ENGINE_V4_REQUEST_ADDRESS_RESERVED", "dd/d7d/alloc__engine__messages_8cc.html#a206e2f73629ce98fd3d9da6c5b0ba032", null ],
    [ "ALLOC_ENGINE_V4_REQUEST_ALLOC_REQUESTED", "dd/d7d/alloc__engine__messages_8cc.html#adc9532c02bdb726b4b747f945cced914", null ],
    [ "ALLOC_ENGINE_V4_REQUEST_EXTEND_LEASE", "dd/d7d/alloc__engine__messages_8cc.html#a09b1403d47b14f3ab7a8c78d4b5fa6e9", null ],
    [ "ALLOC_ENGINE_V4_REQUEST_IN_USE", "dd/d7d/alloc__engine__messages_8cc.html#a0af62503cead14f72edec4856692538d", null ],
    [ "ALLOC_ENGINE_V4_REQUEST_INVALID", "dd/d7d/alloc__engine__messages_8cc.html#a008a76a82e7c4c025acd9d3dc2b65fff", null ],
    [ "ALLOC_ENGINE_V4_REQUEST_OUT_OF_POOL", "dd/d7d/alloc__engine__messages_8cc.html#a1fa3f8dc34b0b941b5b8b9aa86348864", null ],
    [ "ALLOC_ENGINE_V4_REQUEST_PICK_ADDRESS", "dd/d7d/alloc__engine__messages_8cc.html#aa1aa5d7c025d491cd3cd8cdf7cf672ed", null ],
    [ "ALLOC_ENGINE_V4_REQUEST_REMOVE_LEASE", "dd/d7d/alloc__engine__messages_8cc.html#aeda6c66f89c9498566e09e7888cea3ab", null ],
    [ "ALLOC_ENGINE_V4_REQUEST_USE_HR", "dd/d7d/alloc__engine__messages_8cc.html#a4ff2c6c979fefe1d5f8a554f77b73943", null ],
    [ "ALLOC_ENGINE_V4_REUSE_EXPIRED_LEASE_DATA", "dd/d7d/alloc__engine__messages_8cc.html#a146ed5e375b74957fe5825fbac1345ea", null ],
    [ "ALLOC_ENGINE_V6_ALLOC_ERROR", "dd/d7d/alloc__engine__messages_8cc.html#a814d9c4fd24cd133015bcab03e638e68", null ],
    [ "ALLOC_ENGINE_V6_ALLOC_FAIL", "dd/d7d/alloc__engine__messages_8cc.html#ab07068dbace13fecf19e0f595364ac34", null ],
    [ "ALLOC_ENGINE_V6_ALLOC_FAIL_CLASSES", "dd/d7d/alloc__engine__messages_8cc.html#a7bbb0eb48fa501aab4ee8fac72a87ad8", null ],
    [ "ALLOC_ENGINE_V6_ALLOC_FAIL_NO_POOLS", "dd/d7d/alloc__engine__messages_8cc.html#a8c928b1b3881e8e07ee9b8852bce6710", null ],
    [ "ALLOC_ENGINE_V6_ALLOC_FAIL_SHARED_NETWORK", "dd/d7d/alloc__engine__messages_8cc.html#a8cdc26efdf6de3a264bf6028da4de3ea", null ],
    [ "ALLOC_ENGINE_V6_ALLOC_FAIL_SUBNET", "dd/d7d/alloc__engine__messages_8cc.html#af64e2bd2d3cec72f766a78498188fe71", null ],
    [ "ALLOC_ENGINE_V6_ALLOC_HR_LEASE_EXISTS", "dd/d7d/alloc__engine__messages_8cc.html#a0cb33c7903cf8661bdd057b1d6ede66a", null ],
    [ "ALLOC_ENGINE_V6_ALLOC_LEASES_HR", "dd/d7d/alloc__engine__messages_8cc.html#a416549411cdda74754a3e2860b79b226", null ],
    [ "ALLOC_ENGINE_V6_ALLOC_LEASES_NO_HR", "dd/d7d/alloc__engine__messages_8cc.html#addeb34d7ebc4732442b572184a35395d", null ],
    [ "ALLOC_ENGINE_V6_ALLOC_NO_LEASES_HR", "dd/d7d/alloc__engine__messages_8cc.html#a004318098ec1dd570dc3e432e86a5b9b", null ],
    [ "ALLOC_ENGINE_V6_ALLOC_NO_V6_HR", "dd/d7d/alloc__engine__messages_8cc.html#a1900c791689df1e816a18cee7ecc7cfa", null ],
    [ "ALLOC_ENGINE_V6_ALLOC_UNRESERVED", "dd/d7d/alloc__engine__messages_8cc.html#abfbc977eb3f173aadfe6b9147124ed6d", null ],
    [ "ALLOC_ENGINE_V6_CALCULATED_PREFERRED_LIFETIME", "dd/d7d/alloc__engine__messages_8cc.html#abb2e91895884674a429266d08bea3f95", null ],
    [ "ALLOC_ENGINE_V6_DECLINED_RECOVERED", "dd/d7d/alloc__engine__messages_8cc.html#ac03ea9b08c9293a0b8c18b7d3604f07c", null ],
    [ "ALLOC_ENGINE_V6_EXPIRED_HINT_RESERVED", "dd/d7d/alloc__engine__messages_8cc.html#a8f06183ff0a2ab9a66c4c4de1db02227", null ],
    [ "ALLOC_ENGINE_V6_EXTEND_ALLOC_UNRESERVED", "dd/d7d/alloc__engine__messages_8cc.html#a4732e94ad060b92769bef6b220e2610e", null ],
    [ "ALLOC_ENGINE_V6_EXTEND_ERROR", "dd/d7d/alloc__engine__messages_8cc.html#a3e18b44d819e24fbfbf3126bf697d9dc", null ],
    [ "ALLOC_ENGINE_V6_EXTEND_LEASE", "dd/d7d/alloc__engine__messages_8cc.html#a150ec5d71948eb84700600b009483398", null ],
    [ "ALLOC_ENGINE_V6_EXTEND_LEASE_DATA", "dd/d7d/alloc__engine__messages_8cc.html#a13b24d4b69e90b9018d01e3023824ad5", null ],
    [ "ALLOC_ENGINE_V6_EXTEND_NEW_LEASE_DATA", "dd/d7d/alloc__engine__messages_8cc.html#a80210429e95721ba60780a46bc388f13", null ],
    [ "ALLOC_ENGINE_V6_HINT_RESERVED", "dd/d7d/alloc__engine__messages_8cc.html#a1e77bb827e8db4b951ddcbea05bb5376", null ],
    [ "ALLOC_ENGINE_V6_HR_ADDR_GRANTED", "dd/d7d/alloc__engine__messages_8cc.html#a79af21f001679f2fc645fa1ee3b52bb0", null ],
    [ "ALLOC_ENGINE_V6_HR_PREFIX_GRANTED", "dd/d7d/alloc__engine__messages_8cc.html#afc8c90ec5ebdaa0b8e3582defbda4562", null ],
    [ "ALLOC_ENGINE_V6_LEASE_RECLAIM", "dd/d7d/alloc__engine__messages_8cc.html#a69c2f3436fc9bbf68f75c464feb28490", null ],
    [ "ALLOC_ENGINE_V6_LEASE_RECLAMATION_FAILED", "dd/d7d/alloc__engine__messages_8cc.html#ace16249fcd7f3227ce5d89754dcd371d", null ],
    [ "ALLOC_ENGINE_V6_LEASES_RECLAMATION_COMPLETE", "dd/d7d/alloc__engine__messages_8cc.html#ab395ecff40e588d3145da4da86c81404", null ],
    [ "ALLOC_ENGINE_V6_LEASES_RECLAMATION_FAILED", "dd/d7d/alloc__engine__messages_8cc.html#a95c7a7a8c42f70f652b82ec09798f8c3", null ],
    [ "ALLOC_ENGINE_V6_LEASES_RECLAMATION_SLOW", "dd/d7d/alloc__engine__messages_8cc.html#a2062dd939e5a53bab132c762036044b3", null ],
    [ "ALLOC_ENGINE_V6_LEASES_RECLAMATION_START", "dd/d7d/alloc__engine__messages_8cc.html#ac6d0149fcb8857615c20399cced80ca5", null ],
    [ "ALLOC_ENGINE_V6_LEASES_RECLAMATION_TIMEOUT", "dd/d7d/alloc__engine__messages_8cc.html#a74795b2fbbaab6f8389119ed09f4797d", null ],
    [ "ALLOC_ENGINE_V6_NO_MORE_EXPIRED_LEASES", "dd/d7d/alloc__engine__messages_8cc.html#a7cd9fc0150894518f8d133c95e452fdb", null ],
    [ "ALLOC_ENGINE_V6_RECLAIMED_LEASES_DELETE", "dd/d7d/alloc__engine__messages_8cc.html#aef4d5adb2ae8bd8c0f884cb84b0bc006", null ],
    [ "ALLOC_ENGINE_V6_RECLAIMED_LEASES_DELETE_COMPLETE", "dd/d7d/alloc__engine__messages_8cc.html#a974ddcb01d6dfd503b993b1a208e085d", null ],
    [ "ALLOC_ENGINE_V6_RECLAIMED_LEASES_DELETE_FAILED", "dd/d7d/alloc__engine__messages_8cc.html#a0dde10ee96e81ebb64f4e46c13a6f887", null ],
    [ "ALLOC_ENGINE_V6_RENEW_HR", "dd/d7d/alloc__engine__messages_8cc.html#a10e5023f555ba70dee811bd92c7f0b71", null ],
    [ "ALLOC_ENGINE_V6_RENEW_REMOVE_RESERVED", "dd/d7d/alloc__engine__messages_8cc.html#a603b40e74d2177a3d62982d0af86e249", null ],
    [ "ALLOC_ENGINE_V6_REUSE_EXPIRED_LEASE_DATA", "dd/d7d/alloc__engine__messages_8cc.html#a18843b60de268f0df047c2fa1f1deb52", null ],
    [ "ALLOC_ENGINE_V6_REVOKED_ADDR_LEASE", "dd/d7d/alloc__engine__messages_8cc.html#a82146a2aabc1109ac11e07b473eea701", null ],
    [ "ALLOC_ENGINE_V6_REVOKED_PREFIX_LEASE", "dd/d7d/alloc__engine__messages_8cc.html#a988ef32b9fa30dd51695e4c7b45445ed", null ],
    [ "ALLOC_ENGINE_V6_REVOKED_SHARED_ADDR_LEASE", "dd/d7d/alloc__engine__messages_8cc.html#a1e297f88521dd94664812f2fcd5bd864", null ],
    [ "ALLOC_ENGINE_V6_REVOKED_SHARED_PREFIX_LEASE", "dd/d7d/alloc__engine__messages_8cc.html#a17342273df3eb77bad1782387291fda5", null ]
];