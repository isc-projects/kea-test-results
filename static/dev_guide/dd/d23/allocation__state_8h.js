var allocation__state_8h =
[
    [ "isc::dhcp::AllocationState", "d6/d7f/classisc_1_1dhcp_1_1AllocationState.html", "d6/d7f/classisc_1_1dhcp_1_1AllocationState" ],
    [ "isc::dhcp::SubnetAllocationState", "d2/d9b/classisc_1_1dhcp_1_1SubnetAllocationState.html", "d2/d9b/classisc_1_1dhcp_1_1SubnetAllocationState" ],
    [ "AllocationStatePtr", "dd/d23/allocation__state_8h.html#a30b7dc7ba848aca35763e0ec7eb21186", null ],
    [ "SubnetAllocationStatePtr", "dd/d23/allocation__state_8h.html#ad2125cf672b54b045dab69c2ecacb6f1", null ]
];