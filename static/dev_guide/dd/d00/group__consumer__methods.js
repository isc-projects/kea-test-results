var group__consumer__methods =
[
    [ "isc::stats::StatsMgr::count", "dd/d00/group__consumer__methods.html#ga99e34eb771ae0fe2059ea99729f60b27", null ],
    [ "isc::stats::StatsMgr::del", "dd/d00/group__consumer__methods.html#ga94a4125896b0f4cdf5c88f63789356fd", null ],
    [ "isc::stats::StatsMgr::get", "dd/d00/group__consumer__methods.html#gae317e44a00df4d16e3063bc49b64001a", null ],
    [ "isc::stats::StatsMgr::getAll", "dd/d00/group__consumer__methods.html#ga9a2e4bd9d04780cf5912ac49b287f7cb", null ],
    [ "isc::stats::StatsMgr::getSize", "dd/d00/group__consumer__methods.html#gacf7a7f38c5fa4d2cc973add9e6cfdffe", null ],
    [ "isc::stats::StatsMgr::removeAll", "dd/d00/group__consumer__methods.html#ga5eedc14c4c269e51d33f404c8ddce77b", null ],
    [ "isc::stats::StatsMgr::reset", "dd/d00/group__consumer__methods.html#ga1b4d1743ec103bd522ea6a208910ec6a", null ],
    [ "isc::stats::StatsMgr::resetAll", "dd/d00/group__consumer__methods.html#ga6f4cac5f4548dc5ced63bba2555198b8", null ]
];