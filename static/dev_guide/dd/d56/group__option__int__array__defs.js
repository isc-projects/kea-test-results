var group__option__int__array__defs =
[
    [ "isc::dhcp::OptionUint16Array", "dd/d56/group__option__int__array__defs.html#gae33ec1ea2212aa3f01c441a1bc61ccb9", null ],
    [ "isc::dhcp::OptionUint16ArrayPtr", "dd/d56/group__option__int__array__defs.html#gad262f3f0f6f04f78d9efa0f95bc28167", null ],
    [ "isc::dhcp::OptionUint32Array", "dd/d56/group__option__int__array__defs.html#ga3a1d258ca85eb779f297dad888b4323d", null ],
    [ "isc::dhcp::OptionUint32ArrayPtr", "dd/d56/group__option__int__array__defs.html#ga91a72674ac4508b22514a951e6f5d4db", null ],
    [ "isc::dhcp::OptionUint8Array", "dd/d56/group__option__int__array__defs.html#ga82c1bdbd66e3d358534891d9c163e832", null ],
    [ "isc::dhcp::OptionUint8ArrayPtr", "dd/d56/group__option__int__array__defs.html#ga297a557d1ab8403117f56b13a9a563fd", null ]
];