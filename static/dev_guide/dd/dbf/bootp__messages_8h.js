var bootp__messages_8h =
[
    [ "BOOTP_BOOTP_QUERY", "dd/dbf/bootp__messages_8h.html#ab5d0634c8da7dc98d44641c4facf9d97", null ],
    [ "BOOTP_LOAD", "dd/dbf/bootp__messages_8h.html#a5a3eca84ec1be826b67583d89c3e4420", null ],
    [ "BOOTP_PACKET_OPTIONS_SKIPPED", "dd/dbf/bootp__messages_8h.html#a982cb2961277b715b4556dbb1ff9c334", null ],
    [ "BOOTP_PACKET_PACK", "dd/dbf/bootp__messages_8h.html#a859120178906d6c6de487a73c655db79", null ],
    [ "BOOTP_PACKET_PACK_FAIL", "dd/dbf/bootp__messages_8h.html#ac7c3a6254fa133216254a1a863db7ee6", null ],
    [ "BOOTP_PACKET_UNPACK_FAILED", "dd/dbf/bootp__messages_8h.html#a616c56b2280210e3787b55417678ccaa", null ],
    [ "BOOTP_UNLOAD", "dd/dbf/bootp__messages_8h.html#a8c9238273caaf6a388b75214d5799afe", null ]
];