var classisc_1_1data_1_1BaseStampedElement =
[
    [ "BaseStampedElement", "d2/dfd/classisc_1_1data_1_1BaseStampedElement.html#a486fbe01469d1210e5f1222074d6346a", null ],
    [ "getId", "d2/dfd/classisc_1_1data_1_1BaseStampedElement.html#a57418173385acb30121be23b58512c76", null ],
    [ "getModificationTime", "d2/dfd/classisc_1_1data_1_1BaseStampedElement.html#a149387c2cfbc72b7201b14509ab42541", null ],
    [ "setId", "d2/dfd/classisc_1_1data_1_1BaseStampedElement.html#aadb978a65583c64cc3ace6e12eee3762", null ],
    [ "setModificationTime", "d2/dfd/classisc_1_1data_1_1BaseStampedElement.html#a7161639ab2d568d2cf661475d2f32b9e", null ],
    [ "updateModificationTime", "d2/dfd/classisc_1_1data_1_1BaseStampedElement.html#a2917e3cd207e5b9ba29fe24400b31aaa", null ],
    [ "id_", "d2/dfd/classisc_1_1data_1_1BaseStampedElement.html#a294e91e7308947a3855062ae9f188b54", null ],
    [ "timestamp_", "d2/dfd/classisc_1_1data_1_1BaseStampedElement.html#ad2316c74e768746d2bd31e175918563e", null ]
];