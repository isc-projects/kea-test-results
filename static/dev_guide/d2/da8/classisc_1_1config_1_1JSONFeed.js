var classisc_1_1config_1_1JSONFeed =
[
    [ "JSONFeed", "d2/da8/classisc_1_1config_1_1JSONFeed.html#ad17ffdd0b602ec049cf0ac2201f2dc1d", null ],
    [ "feedOk", "d2/da8/classisc_1_1config_1_1JSONFeed.html#a57f556be727aba08cda78a51ebd410fa", null ],
    [ "getErrorMessage", "d2/da8/classisc_1_1config_1_1JSONFeed.html#a99a10621853df8f2d35888c6f3c0f1ee", null ],
    [ "getProcessedText", "d2/da8/classisc_1_1config_1_1JSONFeed.html#abdc78056f1713819df2f247779d5d942", null ],
    [ "initModel", "d2/da8/classisc_1_1config_1_1JSONFeed.html#aae4087b98884ffcdfc8b3fd8d970a250", null ],
    [ "needData", "d2/da8/classisc_1_1config_1_1JSONFeed.html#a5fc45d3217f41423f283049cc7d40462", null ],
    [ "poll", "d2/da8/classisc_1_1config_1_1JSONFeed.html#a886ad717dd13c5383700fdc7f3b50f3c", null ],
    [ "postBuffer", "d2/da8/classisc_1_1config_1_1JSONFeed.html#a3f31e5aa9cb1d6eb06edbda62830d49e", null ],
    [ "toElement", "d2/da8/classisc_1_1config_1_1JSONFeed.html#abbb8fda35841934c46c073fa0fae7a5a", null ]
];