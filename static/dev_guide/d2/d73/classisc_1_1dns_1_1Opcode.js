var classisc_1_1dns_1_1Opcode =
[
    [ "CodeValue", "d2/d73/classisc_1_1dns_1_1Opcode.html#adca7b23f00f256c890405d464d5013cb", [
      [ "QUERY_CODE", "d2/d73/classisc_1_1dns_1_1Opcode.html#adca7b23f00f256c890405d464d5013cbab7994f7e9aadd09c2858bac8a84722ad", null ],
      [ "IQUERY_CODE", "d2/d73/classisc_1_1dns_1_1Opcode.html#adca7b23f00f256c890405d464d5013cba9e22689b608cae7714049bb41f350102", null ],
      [ "STATUS_CODE", "d2/d73/classisc_1_1dns_1_1Opcode.html#adca7b23f00f256c890405d464d5013cba9bc0dab93f5b4e7c33952a32087776be", null ],
      [ "RESERVED3_CODE", "d2/d73/classisc_1_1dns_1_1Opcode.html#adca7b23f00f256c890405d464d5013cbadc82e33058f242ee726991b4ad4fc2ac", null ],
      [ "NOTIFY_CODE", "d2/d73/classisc_1_1dns_1_1Opcode.html#adca7b23f00f256c890405d464d5013cba66dce746e43556f7676e531dec8f21c7", null ],
      [ "UPDATE_CODE", "d2/d73/classisc_1_1dns_1_1Opcode.html#adca7b23f00f256c890405d464d5013cba55a53608cad1640803f0b638975809dd", null ],
      [ "RESERVED6_CODE", "d2/d73/classisc_1_1dns_1_1Opcode.html#adca7b23f00f256c890405d464d5013cbaec0ba73ce33691242221a1461f91bb8c", null ],
      [ "RESERVED7_CODE", "d2/d73/classisc_1_1dns_1_1Opcode.html#adca7b23f00f256c890405d464d5013cba95480525f84f17061022ab7a02521d91", null ],
      [ "RESERVED8_CODE", "d2/d73/classisc_1_1dns_1_1Opcode.html#adca7b23f00f256c890405d464d5013cbaf37cb0aa4e84eed246790337134525a8", null ],
      [ "RESERVED9_CODE", "d2/d73/classisc_1_1dns_1_1Opcode.html#adca7b23f00f256c890405d464d5013cbaf3b45eb844b257975e6afeafe123d3c6", null ],
      [ "RESERVED10_CODE", "d2/d73/classisc_1_1dns_1_1Opcode.html#adca7b23f00f256c890405d464d5013cba53cd67e506092fefb1706967ef3897c5", null ],
      [ "RESERVED11_CODE", "d2/d73/classisc_1_1dns_1_1Opcode.html#adca7b23f00f256c890405d464d5013cba53ce7fbb6f784630c0a0debebe8131e4", null ],
      [ "RESERVED12_CODE", "d2/d73/classisc_1_1dns_1_1Opcode.html#adca7b23f00f256c890405d464d5013cba40d7285f85dc1cee24bfbeba93678f08", null ],
      [ "RESERVED13_CODE", "d2/d73/classisc_1_1dns_1_1Opcode.html#adca7b23f00f256c890405d464d5013cba07aea4e75e0c6f8a49f3e0e3b32f6a4c", null ],
      [ "RESERVED14_CODE", "d2/d73/classisc_1_1dns_1_1Opcode.html#adca7b23f00f256c890405d464d5013cbabccc72ac401ffeb215eadb230a8b5f72", null ],
      [ "RESERVED15_CODE", "d2/d73/classisc_1_1dns_1_1Opcode.html#adca7b23f00f256c890405d464d5013cba3fb66d8f171ca4996cbf97a9ce393365", null ]
    ] ],
    [ "Opcode", "d2/d73/classisc_1_1dns_1_1Opcode.html#aa070d09bd151e8bd5c0c987dfa36fcee", null ],
    [ "equals", "d2/d73/classisc_1_1dns_1_1Opcode.html#ab6d30b20a4f0213625c951ac6b381931", null ],
    [ "getCode", "d2/d73/classisc_1_1dns_1_1Opcode.html#aa493e893b309cdd46344c4866d2c2de4", null ],
    [ "nequals", "d2/d73/classisc_1_1dns_1_1Opcode.html#a9d2637ea380cc40892dd8e8c17fb2dff", null ],
    [ "operator!=", "d2/d73/classisc_1_1dns_1_1Opcode.html#aca610227668535df7640dc46fa2caeff", null ],
    [ "operator==", "d2/d73/classisc_1_1dns_1_1Opcode.html#aa47a3b5c2b19b4bfc94b8ff175e9c76e", null ],
    [ "toText", "d2/d73/classisc_1_1dns_1_1Opcode.html#a528d2167ee76e5138928c972bfc6c166", null ]
];