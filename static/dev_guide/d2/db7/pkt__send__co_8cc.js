var pkt__send__co_8cc =
[
    [ "add4Option", "d2/db7/pkt__send__co_8cc.html#ad94c101c7d1a11b3aba1e90c4afc938f", null ],
    [ "add4Options", "d2/db7/pkt__send__co_8cc.html#af0e340cd6531ca97fe0ca8b5287652e7", null ],
    [ "add6Option", "d2/db7/pkt__send__co_8cc.html#a73956f35ce0bdb5de638dc0d50fb2e80", null ],
    [ "add6Options", "d2/db7/pkt__send__co_8cc.html#a00f5a5941ae35c3d5b8a21bd1f0dca69", null ],
    [ "checkIAStatus", "d2/db7/pkt__send__co_8cc.html#a01edfe1cda5cf616dffa937d3ca1f5c2", null ],
    [ "generate_output_record", "d2/db7/pkt__send__co_8cc.html#a03f51d6b0f5ef6b51b1e27b50a343a38", null ],
    [ "getAddrStrIA_NA", "d2/db7/pkt__send__co_8cc.html#a2e8a5a2ec894bd11b6405fdc620db3c0", null ],
    [ "getAddrStrIA_PD", "d2/db7/pkt__send__co_8cc.html#a4e7c8157293485e9eb58358f38458cb0", null ],
    [ "getDefaultUser4", "d2/db7/pkt__send__co_8cc.html#a7ff003a562b13cb4b402dafec5a07114", null ],
    [ "getDefaultUser6", "d2/db7/pkt__send__co_8cc.html#a863eb4c3fda260ff44805c37299bf017", null ],
    [ "getV6AddrStr", "d2/db7/pkt__send__co_8cc.html#ab44fb82e7bcde8bfef6d576718c9f90d", null ],
    [ "pkt4_send", "d2/db7/pkt__send__co_8cc.html#aa93fc0b15047f5d8370265d45912358c", null ],
    [ "pkt6_send", "d2/db7/pkt__send__co_8cc.html#aa576e2e4893f875ff4247cd28b700a99", null ]
];