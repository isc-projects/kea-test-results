var classisc_1_1db_1_1BackendSelector =
[
    [ "Type", "d2/d26/classisc_1_1db_1_1BackendSelector.html#ab16463b839647b04f651cdca753150f4", [
      [ "MYSQL", "d2/d26/classisc_1_1db_1_1BackendSelector.html#ab16463b839647b04f651cdca753150f4a14498b83dd1667a0c78f4fdaf5afbf4b", null ],
      [ "POSTGRESQL", "d2/d26/classisc_1_1db_1_1BackendSelector.html#ab16463b839647b04f651cdca753150f4a3efccad6e4b6b2044be88d6b86505a5b", null ],
      [ "UNSPEC", "d2/d26/classisc_1_1db_1_1BackendSelector.html#ab16463b839647b04f651cdca753150f4a2f03cf10116568e1dbf5263143cffaec", null ]
    ] ],
    [ "BackendSelector", "d2/d26/classisc_1_1db_1_1BackendSelector.html#ac23fd52a619f6b05aa0f9ca0eed1c6b9", null ],
    [ "BackendSelector", "d2/d26/classisc_1_1db_1_1BackendSelector.html#adaabbe452f2cb5dc7ed94f515961cd1c", null ],
    [ "BackendSelector", "d2/d26/classisc_1_1db_1_1BackendSelector.html#ada7fdb3f04fe74132195c301236fd648", null ],
    [ "BackendSelector", "d2/d26/classisc_1_1db_1_1BackendSelector.html#a4b5cda793c91b01e0ec4c38697327b88", null ],
    [ "amUnspecified", "d2/d26/classisc_1_1db_1_1BackendSelector.html#abbae16a80c5c7ba264ae4b788fcf0fdb", null ],
    [ "getBackendHost", "d2/d26/classisc_1_1db_1_1BackendSelector.html#a9c480e77c9bb8c1e55ec47b7391334ba", null ],
    [ "getBackendPort", "d2/d26/classisc_1_1db_1_1BackendSelector.html#aa24d83878e28cd17f59ac54ba248d0d6", null ],
    [ "getBackendType", "d2/d26/classisc_1_1db_1_1BackendSelector.html#a4ce62b2c7ca9321eed7e8ddc440bff3e", null ],
    [ "toElement", "d2/d26/classisc_1_1db_1_1BackendSelector.html#abd77828da41e419279634efaa5c405ca", null ],
    [ "toText", "d2/d26/classisc_1_1db_1_1BackendSelector.html#a7cb233a594e30a848ed84a1f96860b0f", null ]
];