var option__definition_8h =
[
    [ "isc::dhcp::DuplicateOptionDefinition", "df/d70/classisc_1_1dhcp_1_1DuplicateOptionDefinition.html", "df/d70/classisc_1_1dhcp_1_1DuplicateOptionDefinition" ],
    [ "isc::dhcp::InvalidOptionValue", "d8/df1/classisc_1_1dhcp_1_1InvalidOptionValue.html", "d8/df1/classisc_1_1dhcp_1_1InvalidOptionValue" ],
    [ "isc::dhcp::MalformedOptionDefinition", "d9/da7/classisc_1_1dhcp_1_1MalformedOptionDefinition.html", "d9/da7/classisc_1_1dhcp_1_1MalformedOptionDefinition" ],
    [ "isc::dhcp::OptionDefinition", "df/d6a/classisc_1_1dhcp_1_1OptionDefinition.html", "df/d6a/classisc_1_1dhcp_1_1OptionDefinition" ],
    [ "isc::dhcp::OptionDefSpaceContainer", "da/dc2/classisc_1_1dhcp_1_1OptionDefSpaceContainer.html", "da/dc2/classisc_1_1dhcp_1_1OptionDefSpaceContainer" ],
    [ "BaseOptionDefSpaceContainer", "d2/d26/option__definition_8h.html#a783c4169408c984dcd16eae987a03749", null ],
    [ "OptionDefContainer", "d2/d26/option__definition_8h.html#ad732f00b6ffb3d63ed9bd6d6dc48be9e", null ],
    [ "OptionDefContainerNameIndex", "d2/d26/option__definition_8h.html#aca1efa0eefdf342ea08a73b2c1b5de66", null ],
    [ "OptionDefContainerNameRange", "d2/d26/option__definition_8h.html#a91ec7ac83e1dbaf6e2d09ff2216346aa", null ],
    [ "OptionDefContainerPtr", "d2/d26/option__definition_8h.html#afd6d2c70235c1e6611c1a013988b17dc", null ],
    [ "OptionDefContainers", "d2/d26/option__definition_8h.html#abee7857f2614979ff5b84b5726c702c5", null ],
    [ "OptionDefContainerTypeIndex", "d2/d26/option__definition_8h.html#af0adca3d7f67b8b0bff9e5f626e1f285", null ],
    [ "OptionDefContainerTypeRange", "d2/d26/option__definition_8h.html#acccccd92e3990ff27603ad698b74ecf2", null ],
    [ "OptionDefinitionPtr", "d2/d26/option__definition_8h.html#a7f7298f8ea5b1e9876baf0fbeabe5b21", null ],
    [ "VendorOptionDefContainers", "d2/d26/option__definition_8h.html#a7e0c735717d3e5b355a66dc2d929eeda", null ]
];