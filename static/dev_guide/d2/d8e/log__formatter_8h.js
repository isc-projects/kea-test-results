var log__formatter_8h =
[
    [ "isc::log::FormatFailure", "d0/d1e/classisc_1_1log_1_1FormatFailure.html", "d0/d1e/classisc_1_1log_1_1FormatFailure" ],
    [ "isc::log::Formatter< Logger >", "d6/db1/classisc_1_1log_1_1Formatter.html", "d6/db1/classisc_1_1log_1_1Formatter" ],
    [ "isc::log::MismatchedPlaceholders", "d6/d3b/classisc_1_1log_1_1MismatchedPlaceholders.html", "d6/d3b/classisc_1_1log_1_1MismatchedPlaceholders" ],
    [ "checkExcessPlaceholders", "d2/d8e/log__formatter_8h.html#acdc106b2cc6ac615db061e6fd0b618dc", null ],
    [ "replacePlaceholder", "d2/d8e/log__formatter_8h.html#ad50f68d13bce8781d12aa5cf54510647", null ]
];