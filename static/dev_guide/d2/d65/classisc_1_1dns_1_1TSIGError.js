var classisc_1_1dns_1_1TSIGError =
[
    [ "CodeValue", "d2/d65/classisc_1_1dns_1_1TSIGError.html#a6ececece306c88e79aacbf147a8d4fb9", [
      [ "BAD_SIG_CODE", "d2/d65/classisc_1_1dns_1_1TSIGError.html#a6ececece306c88e79aacbf147a8d4fb9a71c451025f1a94e6ffb18f8af22c080d", null ],
      [ "BAD_KEY_CODE", "d2/d65/classisc_1_1dns_1_1TSIGError.html#a6ececece306c88e79aacbf147a8d4fb9a3f562219909984cb4717ea51d29de1f9", null ],
      [ "BAD_TIME_CODE", "d2/d65/classisc_1_1dns_1_1TSIGError.html#a6ececece306c88e79aacbf147a8d4fb9a4f8f18f4f948e40073d27f238afd04e6", null ],
      [ "BAD_MODE_CODE", "d2/d65/classisc_1_1dns_1_1TSIGError.html#a6ececece306c88e79aacbf147a8d4fb9ac869aae7d7716deab583c349ea4bab09", null ],
      [ "BAD_NAME_CODE", "d2/d65/classisc_1_1dns_1_1TSIGError.html#a6ececece306c88e79aacbf147a8d4fb9a348b4c9d0732cf997ca14d0f63321dab", null ],
      [ "BAD_ALG_CODE", "d2/d65/classisc_1_1dns_1_1TSIGError.html#a6ececece306c88e79aacbf147a8d4fb9a2c9e19ed9872e6755354149fa34c12be", null ],
      [ "BAD_TRUNC_CODE", "d2/d65/classisc_1_1dns_1_1TSIGError.html#a6ececece306c88e79aacbf147a8d4fb9a48a1ef304b3fc94d6699d3195805dd0d", null ]
    ] ],
    [ "TSIGError", "d2/d65/classisc_1_1dns_1_1TSIGError.html#a9e3b944096ea1eb54bc524b5ceb00a0d", null ],
    [ "TSIGError", "d2/d65/classisc_1_1dns_1_1TSIGError.html#a9048b8322ba040e9ad736f01488ad395", null ],
    [ "equals", "d2/d65/classisc_1_1dns_1_1TSIGError.html#aec2b7c8698bc01feee9bf53bf18750bb", null ],
    [ "getCode", "d2/d65/classisc_1_1dns_1_1TSIGError.html#abd618e8fa67de1c64d21903c48a1e545", null ],
    [ "nequals", "d2/d65/classisc_1_1dns_1_1TSIGError.html#a390508b6e9d179db741bb78c11667901", null ],
    [ "operator!=", "d2/d65/classisc_1_1dns_1_1TSIGError.html#a80bf93c7a79ad5059c6a5fab55a4c5d7", null ],
    [ "operator==", "d2/d65/classisc_1_1dns_1_1TSIGError.html#a1f823f44c15dad5c44d256e8ce324914", null ],
    [ "toRcode", "d2/d65/classisc_1_1dns_1_1TSIGError.html#aec4e7105b53cc02bb054c577528bb76c", null ],
    [ "toText", "d2/d65/classisc_1_1dns_1_1TSIGError.html#a8cda1fe9303bcbd4092a8ed70afe1845", null ]
];