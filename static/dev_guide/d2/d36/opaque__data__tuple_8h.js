var opaque__data__tuple_8h =
[
    [ "isc::dhcp::OpaqueDataTuple", "d6/d73/classisc_1_1dhcp_1_1OpaqueDataTuple.html", "d6/d73/classisc_1_1dhcp_1_1OpaqueDataTuple" ],
    [ "isc::dhcp::OpaqueDataTupleError", "dc/d15/classisc_1_1dhcp_1_1OpaqueDataTupleError.html", "dc/d15/classisc_1_1dhcp_1_1OpaqueDataTupleError" ],
    [ "OpaqueDataTuplePtr", "d2/d36/opaque__data__tuple_8h.html#ab758f437be06e9ad04c6665d20e9a9ee", null ],
    [ "operator<<", "d2/d36/opaque__data__tuple_8h.html#a61f1cf9a7f4d087b5b1a0cd711f5f9e3", null ],
    [ "operator>>", "d2/d36/opaque__data__tuple_8h.html#a2b88e04ed18aba855d8fdf9e7ea99f9a", null ]
];