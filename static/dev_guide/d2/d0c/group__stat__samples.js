var group__stat__samples =
[
    [ "isc::stats::BigIntegerSample", "d2/d0c/group__stat__samples.html#gae465a8d4b552995fbacb37909a1848f7", null ],
    [ "isc::stats::DurationSample", "d2/d0c/group__stat__samples.html#gaf1876ff2062b28e2fac21cfa9d135665", null ],
    [ "isc::stats::FloatSample", "d2/d0c/group__stat__samples.html#ga1ef2ecdaebdf44672f83adfd5abd3a42", null ],
    [ "isc::stats::IntegerSample", "d2/d0c/group__stat__samples.html#ga7074a266212c6c20ac7f6f98443e5c5f", null ],
    [ "isc::stats::StringSample", "d2/d0c/group__stat__samples.html#gaa6607b8f8dc7b81a61bb5decf7a1eddf", null ]
];