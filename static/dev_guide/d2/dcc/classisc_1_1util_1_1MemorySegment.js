var classisc_1_1util_1_1MemorySegment =
[
    [ "NamedAddressResult", "d2/dcc/classisc_1_1util_1_1MemorySegment.html#acef7c1f581bd98952ff61f4d4566067a", null ],
    [ "~MemorySegment", "d2/dcc/classisc_1_1util_1_1MemorySegment.html#a001d654ebead88e7c0552db24c8d9936", null ],
    [ "allMemoryDeallocated", "d2/dcc/classisc_1_1util_1_1MemorySegment.html#a889f7c46733dc0859d19f6a1804dd7ea", null ],
    [ "allocate", "d2/dcc/classisc_1_1util_1_1MemorySegment.html#ad8c37b3e8a58c5d5cdca63acc8c1bc37", null ],
    [ "clearNamedAddress", "d2/dcc/classisc_1_1util_1_1MemorySegment.html#a2872eae3ceff1726d5c8d9adf58b5b0d", null ],
    [ "clearNamedAddressImpl", "d2/dcc/classisc_1_1util_1_1MemorySegment.html#a5e34fbc1067999280182820067c3cf2b", null ],
    [ "deallocate", "d2/dcc/classisc_1_1util_1_1MemorySegment.html#a41a2cbce7c3ea355f6053aeecbf8636d", null ],
    [ "getNamedAddress", "d2/dcc/classisc_1_1util_1_1MemorySegment.html#a941713a4cfd46bcd7f4a3904d35e59df", null ],
    [ "getNamedAddressImpl", "d2/dcc/classisc_1_1util_1_1MemorySegment.html#af034848a9daefda9a06f842e4280d4e4", null ],
    [ "setNamedAddress", "d2/dcc/classisc_1_1util_1_1MemorySegment.html#ac53fab9f5c757a6c905b201b5f52b67e", null ],
    [ "setNamedAddressImpl", "d2/dcc/classisc_1_1util_1_1MemorySegment.html#a489235a4a9bb7c2ed5ffb81c19abbd36", null ]
];