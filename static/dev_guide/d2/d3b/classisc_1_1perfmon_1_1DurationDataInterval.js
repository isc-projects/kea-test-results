var classisc_1_1perfmon_1_1DurationDataInterval =
[
    [ "DurationDataInterval", "d2/d3b/classisc_1_1perfmon_1_1DurationDataInterval.html#a494d91e72bc54690d7ffbc1786233cc6", null ],
    [ "~DurationDataInterval", "d2/d3b/classisc_1_1perfmon_1_1DurationDataInterval.html#ad06d07d59e8d970777d9422631b6403e", null ],
    [ "addDuration", "d2/d3b/classisc_1_1perfmon_1_1DurationDataInterval.html#af4247d78fe85a1c638d8169812cb34d1", null ],
    [ "getMaxDuration", "d2/d3b/classisc_1_1perfmon_1_1DurationDataInterval.html#a26e4f8980fbdaff0c893cfbf91b52cce", null ],
    [ "getMeanDuration", "d2/d3b/classisc_1_1perfmon_1_1DurationDataInterval.html#a21482b56e0fb9f5a093ef407a7f7b4f8", null ],
    [ "getMinDuration", "d2/d3b/classisc_1_1perfmon_1_1DurationDataInterval.html#af1fdb9861cf20ea2ef5f11bc34666398", null ],
    [ "getOccurrences", "d2/d3b/classisc_1_1perfmon_1_1DurationDataInterval.html#aee0748a1ceae8d56520d12a83ff838fa", null ],
    [ "getStartTime", "d2/d3b/classisc_1_1perfmon_1_1DurationDataInterval.html#abc68e177250eccad6be464a846f9147a", null ],
    [ "getTotalDuration", "d2/d3b/classisc_1_1perfmon_1_1DurationDataInterval.html#a6c944e47ea53ac242909023d2795e57c", null ],
    [ "operator==", "d2/d3b/classisc_1_1perfmon_1_1DurationDataInterval.html#a8c3ee9ece5606eddc4de1065334413cb", null ],
    [ "setStartTime", "d2/d3b/classisc_1_1perfmon_1_1DurationDataInterval.html#a1569a0f9656a0f0773f235fa09fb6364", null ]
];