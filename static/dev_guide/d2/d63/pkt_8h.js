var pkt_8h =
[
    [ "isc::dhcp::Pkt", "de/d71/classisc_1_1dhcp_1_1Pkt.html", "de/d71/classisc_1_1dhcp_1_1Pkt" ],
    [ "isc::dhcp::PktEvent", "d0/d46/classisc_1_1dhcp_1_1PktEvent.html", "d0/d46/classisc_1_1dhcp_1_1PktEvent" ],
    [ "isc::dhcp::ScopedEnableOptionsCopy< PktType >", "d2/d0f/classisc_1_1dhcp_1_1ScopedEnableOptionsCopy.html", "d2/d0f/classisc_1_1dhcp_1_1ScopedEnableOptionsCopy" ],
    [ "isc::dhcp::ScopedPktOptionsCopy< PktType >", "d7/dca/classisc_1_1dhcp_1_1ScopedPktOptionsCopy.html", "d7/dca/classisc_1_1dhcp_1_1ScopedPktOptionsCopy" ],
    [ "isc::dhcp::ScopedSubOptionsCopy", "df/deb/classisc_1_1dhcp_1_1ScopedSubOptionsCopy.html", "df/deb/classisc_1_1dhcp_1_1ScopedSubOptionsCopy" ],
    [ "PktPtr", "d2/d63/pkt_8h.html#a04924197db6ff7558e566c6a836cbbb5", null ],
    [ "UNSET_IFINDEX", "d2/d63/pkt_8h.html#ab91178d82a65488209367e89c712185a", null ]
];