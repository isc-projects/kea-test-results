var libdhcp_09_09_8h =
[
    [ "isc::dhcp::LibDHCP", "d8/dd6/classisc_1_1dhcp_1_1LibDHCP.html", "d8/dd6/classisc_1_1dhcp_1_1LibDHCP" ],
    [ "isc::dhcp::ManagedScopedOptionsCopyContainer", "d7/dca/structisc_1_1dhcp_1_1ManagedScopedOptionsCopyContainer.html", "d7/dca/structisc_1_1dhcp_1_1ManagedScopedOptionsCopyContainer" ],
    [ "ScopedOptionsCopyContainer", "d2/df8/libdhcp_09_09_8h.html#af799ccd06656ff0ab79206a7195d1672", null ],
    [ "ScopedOptionsCopyPtr", "d2/df8/libdhcp_09_09_8h.html#abf6d6729f4b92ebeb1461b08e284543d", null ],
    [ "ScopedPkt4OptionsCopy", "d2/df8/libdhcp_09_09_8h.html#a2364813fdacf486aa32740b96c69e0b6", null ],
    [ "ScopedPkt6OptionsCopy", "d2/df8/libdhcp_09_09_8h.html#ac418452c7e1821f34007787a956dd6a6", null ]
];