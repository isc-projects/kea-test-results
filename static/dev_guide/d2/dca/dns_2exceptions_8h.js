var dns_2exceptions_8h =
[
    [ "isc::dns::DNSMessageBADVERS", "d9/df0/classisc_1_1dns_1_1DNSMessageBADVERS.html", "d9/df0/classisc_1_1dns_1_1DNSMessageBADVERS" ],
    [ "isc::dns::DNSMessageFORMERR", "df/dc9/classisc_1_1dns_1_1DNSMessageFORMERR.html", "df/dc9/classisc_1_1dns_1_1DNSMessageFORMERR" ],
    [ "isc::dns::DNSProtocolError", "db/dd9/classisc_1_1dns_1_1DNSProtocolError.html", "db/dd9/classisc_1_1dns_1_1DNSProtocolError" ],
    [ "isc::dns::DNSTextError", "da/dbd/classisc_1_1dns_1_1DNSTextError.html", "da/dbd/classisc_1_1dns_1_1DNSTextError" ],
    [ "isc::dns::Exception", "d8/d8c/classisc_1_1dns_1_1Exception.html", "d8/d8c/classisc_1_1dns_1_1Exception" ],
    [ "isc::dns::NameParserException", "dc/d54/classisc_1_1dns_1_1NameParserException.html", "dc/d54/classisc_1_1dns_1_1NameParserException" ]
];