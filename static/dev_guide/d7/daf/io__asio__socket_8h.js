var io__asio__socket_8h =
[
    [ "isc::asiolink::BufferOverflow", "da/df9/classisc_1_1asiolink_1_1BufferOverflow.html", "da/df9/classisc_1_1asiolink_1_1BufferOverflow" ],
    [ "isc::asiolink::DummyAsioSocket< C >", "d6/d07/classisc_1_1asiolink_1_1DummyAsioSocket.html", "d6/d07/classisc_1_1asiolink_1_1DummyAsioSocket" ],
    [ "isc::asiolink::IOAsioSocket< C >", "d1/d96/classisc_1_1asiolink_1_1IOAsioSocket.html", "d1/d96/classisc_1_1asiolink_1_1IOAsioSocket" ],
    [ "isc::asiolink::SocketNotOpen", "d4/dcf/classisc_1_1asiolink_1_1SocketNotOpen.html", "d4/dcf/classisc_1_1asiolink_1_1SocketNotOpen" ],
    [ "isc::asiolink::SocketSetError", "dc/d86/classisc_1_1asiolink_1_1SocketSetError.html", "dc/d86/classisc_1_1asiolink_1_1SocketSetError" ]
];