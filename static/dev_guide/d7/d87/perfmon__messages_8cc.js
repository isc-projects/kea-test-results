var perfmon__messages_8cc =
[
    [ "PERFMON_ALARM_CLEARED", "d7/d87/perfmon__messages_8cc.html#a04b8a75e074f707f357c5f024c325904", null ],
    [ "PERFMON_ALARM_TRIGGERED", "d7/d87/perfmon__messages_8cc.html#a2d41a6873619913a97ccd060127ec65a", null ],
    [ "PERFMON_CMDS_CONTROL_ERROR", "d7/d87/perfmon__messages_8cc.html#afcfb905824efc48da0b94e80d112ef27", null ],
    [ "PERFMON_CMDS_CONTROL_OK", "d7/d87/perfmon__messages_8cc.html#a21101584311d1f50158cc4e76e12b25d", null ],
    [ "PERFMON_CMDS_GET_ALL_DURATIONS_ERROR", "d7/d87/perfmon__messages_8cc.html#a95b95d0cfe6fe2364c963a5b59732e6c", null ],
    [ "PERFMON_CMDS_GET_ALL_DURATIONS_OK", "d7/d87/perfmon__messages_8cc.html#a19b66515013e6d9000477b7ddf02c28f", null ],
    [ "PERFMON_DEINIT_OK", "d7/d87/perfmon__messages_8cc.html#acd506ada684a4309c07c45d1bd2fbac4", null ],
    [ "PERFMON_DHCP4_PKT_EVENTS", "d7/d87/perfmon__messages_8cc.html#a4f23f74c18e18e2ad1b4292fe30fc84d", null ],
    [ "PERFMON_DHCP4_PKT_PROCESS_ERROR", "d7/d87/perfmon__messages_8cc.html#a62f63df51076c750c396fee13bab41a7", null ],
    [ "PERFMON_DHCP4_SOCKET_RECEIVED_TIME_SUPPORT", "d7/d87/perfmon__messages_8cc.html#aff7576db91f6a922e61d3863d0f1b2e5", null ],
    [ "PERFMON_DHCP6_PKT_EVENTS", "d7/d87/perfmon__messages_8cc.html#abcd83fa441a9cba70940b7a449667d71", null ],
    [ "PERFMON_DHCP6_PKT_PROCESS_ERROR", "d7/d87/perfmon__messages_8cc.html#a2b289afde295000e759c1e40618a0fbd", null ],
    [ "PERFMON_DHCP6_SOCKET_RECEIVED_TIME_SUPPORT", "d7/d87/perfmon__messages_8cc.html#ad818807baf54edb04493cfaa33dd1271", null ],
    [ "PERFMON_INIT_FAILED", "d7/d87/perfmon__messages_8cc.html#a4fd475408d37d7dc1c2857f6937715c3", null ],
    [ "PERFMON_INIT_OK", "d7/d87/perfmon__messages_8cc.html#a04043be075b330ae6de556fb073f54f6", null ]
];