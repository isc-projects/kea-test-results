var classisc_1_1dns_1_1Message =
[
    [ "HeaderFlag", "d7/d08/classisc_1_1dns_1_1Message.html#a88e9b7ba970ae433c92d8c7ca6c578e7", [
      [ "HEADERFLAG_QR", "d7/d08/classisc_1_1dns_1_1Message.html#a88e9b7ba970ae433c92d8c7ca6c578e7acfdeb6e3aafad98bc4d3a024e0dd864f", null ],
      [ "HEADERFLAG_AA", "d7/d08/classisc_1_1dns_1_1Message.html#a88e9b7ba970ae433c92d8c7ca6c578e7a9aea0c152d08db16aacba39e124db035", null ],
      [ "HEADERFLAG_TC", "d7/d08/classisc_1_1dns_1_1Message.html#a88e9b7ba970ae433c92d8c7ca6c578e7a46a2f86bcf08d941e251ab07a7e62f6c", null ],
      [ "HEADERFLAG_RD", "d7/d08/classisc_1_1dns_1_1Message.html#a88e9b7ba970ae433c92d8c7ca6c578e7a2db8a29151080197cae99099ec464155", null ],
      [ "HEADERFLAG_RA", "d7/d08/classisc_1_1dns_1_1Message.html#a88e9b7ba970ae433c92d8c7ca6c578e7ac6b2e7b9f033b2ac5cef6909b6badc76", null ],
      [ "HEADERFLAG_AD", "d7/d08/classisc_1_1dns_1_1Message.html#a88e9b7ba970ae433c92d8c7ca6c578e7af724176ccf9075f255f3a0b78da5decb", null ],
      [ "HEADERFLAG_CD", "d7/d08/classisc_1_1dns_1_1Message.html#a88e9b7ba970ae433c92d8c7ca6c578e7acf578d6ef8b5c324f08ccac9a55fe36f", null ]
    ] ],
    [ "Mode", "d7/d08/classisc_1_1dns_1_1Message.html#a5fdeddb4a5625c5e5f2f4148d456bc54", [
      [ "PARSE", "d7/d08/classisc_1_1dns_1_1Message.html#a5fdeddb4a5625c5e5f2f4148d456bc54ad2f59f34c71d0f20b9dc067e6a39e7dd", null ],
      [ "RENDER", "d7/d08/classisc_1_1dns_1_1Message.html#a5fdeddb4a5625c5e5f2f4148d456bc54a6352c0b9ef4b3826d841145b6996f6b1", null ]
    ] ],
    [ "ParseOptions", "d7/d08/classisc_1_1dns_1_1Message.html#a5ca92744465dea672dbbc7b36767016f", [
      [ "PARSE_DEFAULT", "d7/d08/classisc_1_1dns_1_1Message.html#a5ca92744465dea672dbbc7b36767016fa42dd970936fb2ee45721940ccb8fc66a", null ],
      [ "PRESERVE_ORDER", "d7/d08/classisc_1_1dns_1_1Message.html#a5ca92744465dea672dbbc7b36767016fafa940d957ed5eda66c8e1aed28a23265", null ]
    ] ],
    [ "Section", "d7/d08/classisc_1_1dns_1_1Message.html#a4d8a9f6e369632ec0a3f7223193da066", [
      [ "SECTION_QUESTION", "d7/d08/classisc_1_1dns_1_1Message.html#a4d8a9f6e369632ec0a3f7223193da066ad254abd6ba9cfbe9d8351a7d5927873c", null ],
      [ "SECTION_ANSWER", "d7/d08/classisc_1_1dns_1_1Message.html#a4d8a9f6e369632ec0a3f7223193da066aaee0adb56c8e0dfbe9c4a72d0fd48ad2", null ],
      [ "SECTION_AUTHORITY", "d7/d08/classisc_1_1dns_1_1Message.html#a4d8a9f6e369632ec0a3f7223193da066a84b2bc93a7843da37e38d2fb15d90d13", null ],
      [ "SECTION_ADDITIONAL", "d7/d08/classisc_1_1dns_1_1Message.html#a4d8a9f6e369632ec0a3f7223193da066a19fb01e80cfa2e220b01c9180ff7a3c7", null ]
    ] ],
    [ "Message", "d7/d08/classisc_1_1dns_1_1Message.html#af740263d7f21d41e6bc926aa3b2bd7df", null ],
    [ "~Message", "d7/d08/classisc_1_1dns_1_1Message.html#a2a37857c3ea72e8d42509425cafc96df", null ],
    [ "addQuestion", "d7/d08/classisc_1_1dns_1_1Message.html#a90d085d011da1f46da2fc97e4e2364fa", null ],
    [ "addQuestion", "d7/d08/classisc_1_1dns_1_1Message.html#aee58ee8df4744eec50e9e99e68250794", null ],
    [ "addRRset", "d7/d08/classisc_1_1dns_1_1Message.html#a8383dd2998cefe2b2fa1b82262619ade", null ],
    [ "appendSection", "d7/d08/classisc_1_1dns_1_1Message.html#ae573a000e4814b2eabed100f3fd69c81", null ],
    [ "beginQuestion", "d7/d08/classisc_1_1dns_1_1Message.html#a035ef4216408aac9409ac7c246051b63", null ],
    [ "beginSection", "d7/d08/classisc_1_1dns_1_1Message.html#af2f10e6ea9790e72e18f20e58b08e255", null ],
    [ "clear", "d7/d08/classisc_1_1dns_1_1Message.html#a2da397944e5a94bfe102465eac153642", null ],
    [ "clearSection", "d7/d08/classisc_1_1dns_1_1Message.html#a3ff4387b0d380f0261f80957a099d4ed", null ],
    [ "endQuestion", "d7/d08/classisc_1_1dns_1_1Message.html#a03bfe0662ae16c237bdca984319b2339", null ],
    [ "endSection", "d7/d08/classisc_1_1dns_1_1Message.html#a93b0ead6a2ce184ef05f9ee9aa851e39", null ],
    [ "fromWire", "d7/d08/classisc_1_1dns_1_1Message.html#a87a76875769c0fe991c0011bb1d8c12c", null ],
    [ "getEDNS", "d7/d08/classisc_1_1dns_1_1Message.html#ab7c1d448c0ba11194affedbe20766f42", null ],
    [ "getHeaderFlag", "d7/d08/classisc_1_1dns_1_1Message.html#a23244ce55065c433e10662d6f8109726", null ],
    [ "getOpcode", "d7/d08/classisc_1_1dns_1_1Message.html#ab9c0ce1b97341195d67d3784c3682f74", null ],
    [ "getQid", "d7/d08/classisc_1_1dns_1_1Message.html#af0c3ba5e15a63499ff59ef3bc444461f", null ],
    [ "getRcode", "d7/d08/classisc_1_1dns_1_1Message.html#a17ee1647bf028ce9d14adab4fe475cec", null ],
    [ "getRRCount", "d7/d08/classisc_1_1dns_1_1Message.html#a683b32f80e0266b049d18006c465e497", null ],
    [ "getTSIGRecord", "d7/d08/classisc_1_1dns_1_1Message.html#a97e9d05aa871bbb6607ac138af76c528", null ],
    [ "hasRRset", "d7/d08/classisc_1_1dns_1_1Message.html#ae089df9a847330d1019eeb70d3c450b3", null ],
    [ "hasRRset", "d7/d08/classisc_1_1dns_1_1Message.html#ad498e31d2a0e6aba650b768a8cb91d64", null ],
    [ "makeResponse", "d7/d08/classisc_1_1dns_1_1Message.html#aabdbe1c7c55abf91d05a3bff7c45621a", null ],
    [ "parseHeader", "d7/d08/classisc_1_1dns_1_1Message.html#ac70aad0d1c1ee9e19b6053cf2e4c96e6", null ],
    [ "removeRRset", "d7/d08/classisc_1_1dns_1_1Message.html#a794f2dc86075da3707004a9708fda455", null ],
    [ "setEDNS", "d7/d08/classisc_1_1dns_1_1Message.html#afcab76522f17792fb775b871f8a0fa49", null ],
    [ "setHeaderFlag", "d7/d08/classisc_1_1dns_1_1Message.html#aaeeff4b07b3066500f0102fa3fe6e285", null ],
    [ "setOpcode", "d7/d08/classisc_1_1dns_1_1Message.html#a6f46e93c26ddd25553291cb686c70b83", null ],
    [ "setQid", "d7/d08/classisc_1_1dns_1_1Message.html#af0718489eed49a8b02ef046ca1ee519a", null ],
    [ "setRcode", "d7/d08/classisc_1_1dns_1_1Message.html#af6e0ed87b38b5b5c7902b49a985aa48d", null ],
    [ "toText", "d7/d08/classisc_1_1dns_1_1Message.html#a9db7584d0d1c334185be7cfdefb21a70", null ],
    [ "toWire", "d7/d08/classisc_1_1dns_1_1Message.html#a0917440782c53ce9a763f200a2790359", null ]
];