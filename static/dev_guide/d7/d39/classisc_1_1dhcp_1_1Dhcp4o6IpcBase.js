var classisc_1_1dhcp_1_1Dhcp4o6IpcBase =
[
    [ "EndpointType", "d7/d39/classisc_1_1dhcp_1_1Dhcp4o6IpcBase.html#a37b95632ba1f839d0057cb8f7002fbc1", [
      [ "ENDPOINT_TYPE_V4", "d7/d39/classisc_1_1dhcp_1_1Dhcp4o6IpcBase.html#a37b95632ba1f839d0057cb8f7002fbc1acec1963242535de867ba9465be02d192", null ],
      [ "ENDPOINT_TYPE_V6", "d7/d39/classisc_1_1dhcp_1_1Dhcp4o6IpcBase.html#a37b95632ba1f839d0057cb8f7002fbc1adaa99669dbb75ca211888284aa06b799", null ]
    ] ],
    [ "Dhcp4o6IpcBase", "d7/d39/classisc_1_1dhcp_1_1Dhcp4o6IpcBase.html#a5ed6611d383d466e9344c9273dd197fa", null ],
    [ "~Dhcp4o6IpcBase", "d7/d39/classisc_1_1dhcp_1_1Dhcp4o6IpcBase.html#a196120173d61bdf8302f94b2837f2479", null ],
    [ "close", "d7/d39/classisc_1_1dhcp_1_1Dhcp4o6IpcBase.html#a440e2cf66b75b69ee276c2e5d5a42ec6", null ],
    [ "open", "d7/d39/classisc_1_1dhcp_1_1Dhcp4o6IpcBase.html#a4f331996c7c21af8ba453aa93bb8de1b", null ],
    [ "open", "d7/d39/classisc_1_1dhcp_1_1Dhcp4o6IpcBase.html#ab3bc969a3f35198825c420962df4e110", null ],
    [ "receive", "d7/d39/classisc_1_1dhcp_1_1Dhcp4o6IpcBase.html#a2730b57671ed56bebbfff4638d146594", null ],
    [ "send", "d7/d39/classisc_1_1dhcp_1_1Dhcp4o6IpcBase.html#a12620909628ab2cbe3bfcc69d5f53c27", null ],
    [ "port_", "d7/d39/classisc_1_1dhcp_1_1Dhcp4o6IpcBase.html#a326f93c399522cbcb8465368f6f5faaf", null ],
    [ "socket_fd_", "d7/d39/classisc_1_1dhcp_1_1Dhcp4o6IpcBase.html#aca64ac45f9ef99944c485930b923d5c4", null ]
];