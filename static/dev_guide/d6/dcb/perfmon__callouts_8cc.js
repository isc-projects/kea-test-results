var perfmon__callouts_8cc =
[
    [ "dhcp4_srv_configured", "d6/dcb/perfmon__callouts_8cc.html#a41d15fea592d19a1352b8eb8f8dba62b", null ],
    [ "dhcp6_srv_configured", "d6/dcb/perfmon__callouts_8cc.html#a28f8367f62e3fb48589b25dfdb17e34c", null ],
    [ "load", "d6/dcb/perfmon__callouts_8cc.html#aee8b202ef89ff8ca162af1970e698a15", null ],
    [ "multi_threading_compatible", "d6/dcb/perfmon__callouts_8cc.html#a68b452e3e12b48fc86172cd3d1403e62", null ],
    [ "perfmon_control", "d6/dcb/perfmon__callouts_8cc.html#a65c690a81f532994b1b55d2f9e4eb773", null ],
    [ "perfmon_get_all_durations", "d6/dcb/perfmon__callouts_8cc.html#aefd1f2e2ab31361e4532463608cd6df8", null ],
    [ "pkt4_send", "d6/dcb/perfmon__callouts_8cc.html#aa93fc0b15047f5d8370265d45912358c", null ],
    [ "pkt6_send", "d6/dcb/perfmon__callouts_8cc.html#aa576e2e4893f875ff4247cd28b700a99", null ],
    [ "unload", "d6/dcb/perfmon__callouts_8cc.html#aa949c8965ae04fc913e940015c5b60d9", null ],
    [ "mgr", "d6/dcb/perfmon__callouts_8cc.html#aefdba4874fa54c3a4eced25cb30b1c33", null ]
];