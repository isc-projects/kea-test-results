var structisc_1_1asiodns_1_1IOFetchData =
[
    [ "IOFetchData", "d6/da3/structisc_1_1asiodns_1_1IOFetchData.html#af340f8d3170c55868685301419e2e064", null ],
    [ "~IOFetchData", "d6/da3/structisc_1_1asiodns_1_1IOFetchData.html#aad0f0c1c9696f12821e1972fbd2ec218", null ],
    [ "responseOK", "d6/da3/structisc_1_1asiodns_1_1IOFetchData.html#ade9c9779c95f1156dd8c4094bdf4b758", null ],
    [ "callback", "d6/da3/structisc_1_1asiodns_1_1IOFetchData.html#a1d6433b8ccdb96d3d54ca9ae1636d344", null ],
    [ "cumulative", "d6/da3/structisc_1_1asiodns_1_1IOFetchData.html#a8c4d15c1e197097d703b3684c2b7f91a", null ],
    [ "expected", "d6/da3/structisc_1_1asiodns_1_1IOFetchData.html#a7084dee43f468dda764ccdff1384c6c6", null ],
    [ "io_service_", "d6/da3/structisc_1_1asiodns_1_1IOFetchData.html#a531ee70c1d7e8a032408b53b9697f578", null ],
    [ "msgbuf", "d6/da3/structisc_1_1asiodns_1_1IOFetchData.html#a61f9283a2a02fa057530d07c396f2d82", null ],
    [ "offset", "d6/da3/structisc_1_1asiodns_1_1IOFetchData.html#af6074001600ab9cb0f9719c8b6487781", null ],
    [ "origin", "d6/da3/structisc_1_1asiodns_1_1IOFetchData.html#abc8c065c725217479d88931fdf59f105", null ],
    [ "packet", "d6/da3/structisc_1_1asiodns_1_1IOFetchData.html#afbb8ac8ffc442a510d3805cd3c2ef098", null ],
    [ "protocol", "d6/da3/structisc_1_1asiodns_1_1IOFetchData.html#a643e077f9fecdb7ecbd39ffb5992e0b3", null ],
    [ "qid", "d6/da3/structisc_1_1asiodns_1_1IOFetchData.html#a36cf9c915143adab3818e9422604f1ff", null ],
    [ "received", "d6/da3/structisc_1_1asiodns_1_1IOFetchData.html#a64d42a83667e9c413e889ee9f15cde8c", null ],
    [ "remote_rcv", "d6/da3/structisc_1_1asiodns_1_1IOFetchData.html#aa37714ec030958e8ce8fa7cb836545f5", null ],
    [ "remote_snd", "d6/da3/structisc_1_1asiodns_1_1IOFetchData.html#a258edbf9332743454d0b96cbdd9e0ce0", null ],
    [ "socket", "d6/da3/structisc_1_1asiodns_1_1IOFetchData.html#a848b180197fc5103fa01173c85f84af0", null ],
    [ "staging", "d6/da3/structisc_1_1asiodns_1_1IOFetchData.html#a704f257427bacfd0cfe67dd676a3a539", null ],
    [ "stopped", "d6/da3/structisc_1_1asiodns_1_1IOFetchData.html#a5f27c26fe549adeb860d244f88882c7f", null ],
    [ "timeout", "d6/da3/structisc_1_1asiodns_1_1IOFetchData.html#a383dd4a4b0a8dcf7ff53f90705f871e5", null ],
    [ "timer", "d6/da3/structisc_1_1asiodns_1_1IOFetchData.html#a96d823afc2725a2c2cb83edd1b7afeba", null ]
];