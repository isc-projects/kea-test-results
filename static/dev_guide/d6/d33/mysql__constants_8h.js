var mysql__constants_8h =
[
    [ "my_bool", "d6/d33/mysql__constants_8h.html#a93134d868729cd8a0285a31f5e713c17", null ],
    [ "my_bools", "d6/d33/mysql__constants_8h.html#abd6b8b97ace44ff68738d19699e368ac", null ],
    [ "MLM_FALSE", "d6/d33/mysql__constants_8h.html#a0c2b3ae51e30ba2d0d088560b856d043", null ],
    [ "MLM_MYSQL_FETCH_FAILURE", "d6/d33/mysql__constants_8h.html#a948e5e0b1b84d2a28c8a15c0c40f0cec", null ],
    [ "MLM_MYSQL_FETCH_SUCCESS", "d6/d33/mysql__constants_8h.html#ae7ebccd3cb9b56a814bbda47af16f85e", null ],
    [ "MLM_TRUE", "d6/d33/mysql__constants_8h.html#a8f2d163f35a69f03d46ab41aa70e3517", null ],
    [ "MYSQL_SCHEMA_VERSION_MAJOR", "d6/d33/mysql__constants_8h.html#a2db0f4aae1fb6aacd603e13de70bf70a", null ],
    [ "MYSQL_SCHEMA_VERSION_MINOR", "d6/d33/mysql__constants_8h.html#a7cce45a12ffb7fc3326825854fd3de97", null ]
];