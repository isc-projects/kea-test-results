var classisc_1_1ha_1_1HAConfig_1_1PeerConfig =
[
    [ "Role", "d6/d2d/classisc_1_1ha_1_1HAConfig_1_1PeerConfig.html#a9aee6b3fdb29e1db07d1cbf4af539786", [
      [ "PRIMARY", "d6/d2d/classisc_1_1ha_1_1HAConfig_1_1PeerConfig.html#a9aee6b3fdb29e1db07d1cbf4af539786a5480b38a71dd13ec2eeb5eac741253bd", null ],
      [ "SECONDARY", "d6/d2d/classisc_1_1ha_1_1HAConfig_1_1PeerConfig.html#a9aee6b3fdb29e1db07d1cbf4af539786a5e9774aed70782c7fb29db4362b5502a", null ],
      [ "STANDBY", "d6/d2d/classisc_1_1ha_1_1HAConfig_1_1PeerConfig.html#a9aee6b3fdb29e1db07d1cbf4af539786a136c37d3e2529ec8b44b0e308c595a1d", null ],
      [ "BACKUP", "d6/d2d/classisc_1_1ha_1_1HAConfig_1_1PeerConfig.html#a9aee6b3fdb29e1db07d1cbf4af539786a7883fc38b9f49469f545c6a18b3c24b8", null ]
    ] ],
    [ "PeerConfig", "d6/d2d/classisc_1_1ha_1_1HAConfig_1_1PeerConfig.html#a133776a8b7b7d151acbf1f0c69efd641", null ],
    [ "addBasicAuthHttpHeader", "d6/d2d/classisc_1_1ha_1_1HAConfig_1_1PeerConfig.html#a442f9a4115df1e0a9818d404168051a9", null ],
    [ "getBasicAuth", "d6/d2d/classisc_1_1ha_1_1HAConfig_1_1PeerConfig.html#aefd2529ce58449d30f75c9cb33780694", null ],
    [ "getBasicAuth", "d6/d2d/classisc_1_1ha_1_1HAConfig_1_1PeerConfig.html#aaa1abc12d30a03a1f1e931a18a6b15f9", null ],
    [ "getCertFile", "d6/d2d/classisc_1_1ha_1_1HAConfig_1_1PeerConfig.html#aeb8bed3730e023cd88c3e0016853bdd7", null ],
    [ "getKeyFile", "d6/d2d/classisc_1_1ha_1_1HAConfig_1_1PeerConfig.html#a491c9c03746de7c09795c8831ab51d5f", null ],
    [ "getLogLabel", "d6/d2d/classisc_1_1ha_1_1HAConfig_1_1PeerConfig.html#a068bbbe21a37440a31809494f34099de", null ],
    [ "getName", "d6/d2d/classisc_1_1ha_1_1HAConfig_1_1PeerConfig.html#ab3411007e9dfc4a1abd5f1831f85d992", null ],
    [ "getRole", "d6/d2d/classisc_1_1ha_1_1HAConfig_1_1PeerConfig.html#a0c171ad19a4415ac9ac9625cf62d36b8", null ],
    [ "getTlsContext", "d6/d2d/classisc_1_1ha_1_1HAConfig_1_1PeerConfig.html#abbc21c003bc9a72cf7c7eb803efdf032", null ],
    [ "getTrustAnchor", "d6/d2d/classisc_1_1ha_1_1HAConfig_1_1PeerConfig.html#a1e96be387f957bcc94a0b9bd374c8973", null ],
    [ "getUrl", "d6/d2d/classisc_1_1ha_1_1HAConfig_1_1PeerConfig.html#af5273a112981519577d4d422d9a7a9cf", null ],
    [ "isAutoFailover", "d6/d2d/classisc_1_1ha_1_1HAConfig_1_1PeerConfig.html#a44cf896d63caef5fdfefe02557238373", null ],
    [ "setAutoFailover", "d6/d2d/classisc_1_1ha_1_1HAConfig_1_1PeerConfig.html#a29e79f3478205773a4a0edcecaffb6de", null ],
    [ "setCertFile", "d6/d2d/classisc_1_1ha_1_1HAConfig_1_1PeerConfig.html#af8b56f62faad14b087e34d272e526ae7", null ],
    [ "setKeyFile", "d6/d2d/classisc_1_1ha_1_1HAConfig_1_1PeerConfig.html#a275502e2ce87e5a4d14e88d47dfd998c", null ],
    [ "setName", "d6/d2d/classisc_1_1ha_1_1HAConfig_1_1PeerConfig.html#adf8909cef3a16d0616e40e90ac068128", null ],
    [ "setRole", "d6/d2d/classisc_1_1ha_1_1HAConfig_1_1PeerConfig.html#a9f01728e55184f728e3e43fad4f2bddd", null ],
    [ "setTrustAnchor", "d6/d2d/classisc_1_1ha_1_1HAConfig_1_1PeerConfig.html#af69983373fe299a14195a33fa571205d", null ],
    [ "setUrl", "d6/d2d/classisc_1_1ha_1_1HAConfig_1_1PeerConfig.html#a40e599c2d3a481262886caaa5895ea11", null ],
    [ "tls_context_", "d6/d2d/classisc_1_1ha_1_1HAConfig_1_1PeerConfig.html#a5b57c2946b387b87caa08b5c276bb0e0", null ]
];