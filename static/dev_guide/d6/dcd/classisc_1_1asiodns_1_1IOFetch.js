var classisc_1_1asiodns_1_1IOFetch =
[
    [ "Callback", "d8/d32/classisc_1_1asiodns_1_1IOFetch_1_1Callback.html", "d8/d32/classisc_1_1asiodns_1_1IOFetch_1_1Callback" ],
    [ "Origin", "d6/dcd/classisc_1_1asiodns_1_1IOFetch.html#acd1942b57536555ddb4032536b0bf6c3", [
      [ "NONE", "d6/dcd/classisc_1_1asiodns_1_1IOFetch.html#acd1942b57536555ddb4032536b0bf6c3ae3953a4bfa4f9094879a0b0ce44821da", null ],
      [ "OPEN", "d6/dcd/classisc_1_1asiodns_1_1IOFetch.html#acd1942b57536555ddb4032536b0bf6c3aa26cac07a31f563aed8f87e0240d3f91", null ],
      [ "SEND", "d6/dcd/classisc_1_1asiodns_1_1IOFetch.html#acd1942b57536555ddb4032536b0bf6c3a4f46b3a4bb97b6a0463df9af413090ce", null ],
      [ "RECEIVE", "d6/dcd/classisc_1_1asiodns_1_1IOFetch.html#acd1942b57536555ddb4032536b0bf6c3a74bc197bfafe192426f7ff330502123e", null ],
      [ "CLOSE", "d6/dcd/classisc_1_1asiodns_1_1IOFetch.html#acd1942b57536555ddb4032536b0bf6c3abbdc32dcb10c472bdc0417e45ad29546", null ]
    ] ],
    [ "Protocol", "d6/dcd/classisc_1_1asiodns_1_1IOFetch.html#ad32e28dfef5420d59afd0c8eb481b53d", [
      [ "UDP", "d6/dcd/classisc_1_1asiodns_1_1IOFetch.html#ad32e28dfef5420d59afd0c8eb481b53dafa4c4afecd4cf8f7322952b8be55f093", null ],
      [ "TCP", "d6/dcd/classisc_1_1asiodns_1_1IOFetch.html#ad32e28dfef5420d59afd0c8eb481b53da3df6ae3ef85548513ae1bbc38ee7fadb", null ]
    ] ],
    [ "Result", "d6/dcd/classisc_1_1asiodns_1_1IOFetch.html#ac06cbc456e8304ed920f1028a11a9cc4", [
      [ "SUCCESS", "d6/dcd/classisc_1_1asiodns_1_1IOFetch.html#ac06cbc456e8304ed920f1028a11a9cc4ae006b42b4cee46a06fccd8dca1b24823", null ],
      [ "TIME_OUT", "d6/dcd/classisc_1_1asiodns_1_1IOFetch.html#ac06cbc456e8304ed920f1028a11a9cc4adf0190c4560536429ce6383b5febd304", null ],
      [ "STOPPED", "d6/dcd/classisc_1_1asiodns_1_1IOFetch.html#ac06cbc456e8304ed920f1028a11a9cc4a6126f782bfda2983b7a83b46f42669fc", null ],
      [ "NOTSET", "d6/dcd/classisc_1_1asiodns_1_1IOFetch.html#ac06cbc456e8304ed920f1028a11a9cc4a0a95a46607d1a5bdc3ae95ee7986ae5f", null ]
    ] ],
    [ "IOFetch", "d6/dcd/classisc_1_1asiodns_1_1IOFetch.html#a9c230a969f878f6662d4f1f557d13810", null ],
    [ "IOFetch", "d6/dcd/classisc_1_1asiodns_1_1IOFetch.html#a3bf64a2d41013f2392447b8e76934cee", null ],
    [ "IOFetch", "d6/dcd/classisc_1_1asiodns_1_1IOFetch.html#a56c0c15f46cc4aa78c604fc5ed6f6084", null ],
    [ "getProtocol", "d6/dcd/classisc_1_1asiodns_1_1IOFetch.html#ad9cc5a6a0b3c3ac22765c21aef6e4b9c", null ],
    [ "operator()", "d6/dcd/classisc_1_1asiodns_1_1IOFetch.html#a9eabf652e478bf1f8063d91a9343aaa7", null ],
    [ "stop", "d6/dcd/classisc_1_1asiodns_1_1IOFetch.html#ad827a4317ed1ab48403ff8bcae693251", null ]
];