var classisc_1_1asiolink_1_1DummyAsioSocket =
[
    [ "DummyAsioSocket", "d6/d07/classisc_1_1asiolink_1_1DummyAsioSocket.html#a8cdefbb32c0935263751ef6c6bdb958f", null ],
    [ "asyncReceive", "d6/d07/classisc_1_1asiolink_1_1DummyAsioSocket.html#a478f290b0af71cfeaf58e95d06c5865a", null ],
    [ "asyncSend", "d6/d07/classisc_1_1asiolink_1_1DummyAsioSocket.html#a93acba1108c589ad1e9acb3bea5aff17", null ],
    [ "cancel", "d6/d07/classisc_1_1asiolink_1_1DummyAsioSocket.html#a8a07babd1efa09200e09778aad8f07d5", null ],
    [ "close", "d6/d07/classisc_1_1asiolink_1_1DummyAsioSocket.html#a9679df26f3a78d16226f6db39a0d3711", null ],
    [ "getNative", "d6/d07/classisc_1_1asiolink_1_1DummyAsioSocket.html#a99fe832baba2b7570a722e34742dff52", null ],
    [ "getProtocol", "d6/d07/classisc_1_1asiolink_1_1DummyAsioSocket.html#a35c79b6529a38cdc90801e4ea49737cf", null ],
    [ "isOpenSynchronous", "d6/d07/classisc_1_1asiolink_1_1DummyAsioSocket.html#ae1cf665a744e66bd8439fe8c761cfb8d", null ],
    [ "open", "d6/d07/classisc_1_1asiolink_1_1DummyAsioSocket.html#a04bfc95ac7116175949647fe80d10a40", null ],
    [ "receiveComplete", "d6/d07/classisc_1_1asiolink_1_1DummyAsioSocket.html#a159225fdc4910c18ce542c6b17fdae3f", null ]
];