var db__exceptions_8h =
[
    [ "isc::db::AmbiguousDatabase", "de/dcf/classisc_1_1db_1_1AmbiguousDatabase.html", "de/dcf/classisc_1_1db_1_1AmbiguousDatabase" ],
    [ "isc::db::DataTruncated", "d4/d41/classisc_1_1db_1_1DataTruncated.html", "d4/d41/classisc_1_1db_1_1DataTruncated" ],
    [ "isc::db::DbConfigError", "d4/db6/classisc_1_1db_1_1DbConfigError.html", "d4/db6/classisc_1_1db_1_1DbConfigError" ],
    [ "isc::db::DuplicateEntry", "d1/d94/classisc_1_1db_1_1DuplicateEntry.html", "d1/d94/classisc_1_1db_1_1DuplicateEntry" ],
    [ "isc::db::InvalidAddressFamily", "df/ddf/classisc_1_1db_1_1InvalidAddressFamily.html", "df/ddf/classisc_1_1db_1_1InvalidAddressFamily" ],
    [ "isc::db::InvalidRange", "de/d7f/classisc_1_1db_1_1InvalidRange.html", "de/d7f/classisc_1_1db_1_1InvalidRange" ],
    [ "isc::db::MultipleRecords", "d2/df9/classisc_1_1db_1_1MultipleRecords.html", "d2/df9/classisc_1_1db_1_1MultipleRecords" ],
    [ "isc::db::NoRowsAffected", "d2/d5f/classisc_1_1db_1_1NoRowsAffected.html", "d2/d5f/classisc_1_1db_1_1NoRowsAffected" ],
    [ "isc::db::NoSuchDatabase", "d7/db9/classisc_1_1db_1_1NoSuchDatabase.html", "d7/db9/classisc_1_1db_1_1NoSuchDatabase" ],
    [ "isc::db::NullKeyError", "d5/d90/classisc_1_1db_1_1NullKeyError.html", "d5/d90/classisc_1_1db_1_1NullKeyError" ],
    [ "isc::db::ReadOnlyDb", "d6/d31/classisc_1_1db_1_1ReadOnlyDb.html", "d6/d31/classisc_1_1db_1_1ReadOnlyDb" ]
];