var observation_8h =
[
    [ "isc::stats::InvalidStatType", "d6/d5c/classisc_1_1stats_1_1InvalidStatType.html", "d6/d5c/classisc_1_1stats_1_1InvalidStatType" ],
    [ "isc::stats::Observation", "d4/d18/classisc_1_1stats_1_1Observation.html", "d4/d18/classisc_1_1stats_1_1Observation" ],
    [ "BigIntegerSample", "d3/dab/observation_8h.html#gae465a8d4b552995fbacb37909a1848f7", null ],
    [ "DurationSample", "d3/dab/observation_8h.html#gaf1876ff2062b28e2fac21cfa9d135665", null ],
    [ "FloatSample", "d3/dab/observation_8h.html#ga1ef2ecdaebdf44672f83adfd5abd3a42", null ],
    [ "IntegerSample", "d3/dab/observation_8h.html#ga7074a266212c6c20ac7f6f98443e5c5f", null ],
    [ "ObservationPtr", "d3/dab/observation_8h.html#a628dfd5ba6148fe428852217f183e483", null ],
    [ "SampleClock", "d3/dab/observation_8h.html#a6caf14e77d993146f5dc4b9e755942f5", null ],
    [ "StatsDuration", "d3/dab/observation_8h.html#adab8a849f86284157fd876647f8e8365", null ],
    [ "StringSample", "d3/dab/observation_8h.html#gaa6607b8f8dc7b81a61bb5decf7a1eddf", null ],
    [ "toSeconds", "d3/dab/observation_8h.html#ab1440d7d4f5dc058ddd63552f6baabeb", null ]
];