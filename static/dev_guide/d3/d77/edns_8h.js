var edns_8h =
[
    [ "isc::dns::EDNS", "d8/dd5/classisc_1_1dns_1_1EDNS.html", "d8/dd5/classisc_1_1dns_1_1EDNS" ],
    [ "ConstEDNSPtr", "d3/d77/edns_8h.html#a9ef4e96967edb7a91e38340f6a95493f", null ],
    [ "EDNSPtr", "d3/d77/edns_8h.html#abb653c8647c8cd3df3f0d24a878d5f74", null ],
    [ "createEDNSFromRR", "d3/d77/edns_8h.html#a678840dbf6e614ebe94af41c90a180c4", null ],
    [ "operator<<", "d3/d77/edns_8h.html#a626a91dfe29dd00b2b2434f11c554758", null ]
];