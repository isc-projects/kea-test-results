var iface__mgr_8h =
[
    [ "isc::dhcp::Iface", "d3/d7b/classisc_1_1dhcp_1_1Iface.html", "d3/d7b/classisc_1_1dhcp_1_1Iface" ],
    [ "isc::dhcp::IfaceCollection", "dc/d07/classisc_1_1dhcp_1_1IfaceCollection.html", "dc/d07/classisc_1_1dhcp_1_1IfaceCollection" ],
    [ "isc::dhcp::IfaceDetectError", "dd/d86/classisc_1_1dhcp_1_1IfaceDetectError.html", "dd/d86/classisc_1_1dhcp_1_1IfaceDetectError" ],
    [ "isc::dhcp::IfaceMgr", "de/dfd/classisc_1_1dhcp_1_1IfaceMgr.html", "de/dfd/classisc_1_1dhcp_1_1IfaceMgr" ],
    [ "isc::dhcp::IfaceNotFound", "da/d4a/classisc_1_1dhcp_1_1IfaceNotFound.html", "da/d4a/classisc_1_1dhcp_1_1IfaceNotFound" ],
    [ "isc::dhcp::PacketFilterChangeDenied", "d9/d20/classisc_1_1dhcp_1_1PacketFilterChangeDenied.html", "d9/d20/classisc_1_1dhcp_1_1PacketFilterChangeDenied" ],
    [ "isc::dhcp::SignalInterruptOnSelect", "d2/d6c/classisc_1_1dhcp_1_1SignalInterruptOnSelect.html", "d2/d6c/classisc_1_1dhcp_1_1SignalInterruptOnSelect" ],
    [ "isc::dhcp::IfaceMgr::SocketCallbackInfo", "de/d25/structisc_1_1dhcp_1_1IfaceMgr_1_1SocketCallbackInfo.html", "de/d25/structisc_1_1dhcp_1_1IfaceMgr_1_1SocketCallbackInfo" ],
    [ "isc::dhcp::SocketConfigError", "de/de8/classisc_1_1dhcp_1_1SocketConfigError.html", "de/de8/classisc_1_1dhcp_1_1SocketConfigError" ],
    [ "isc::dhcp::SocketNotFound", "d6/d6f/classisc_1_1dhcp_1_1SocketNotFound.html", "d6/d6f/classisc_1_1dhcp_1_1SocketNotFound" ],
    [ "isc::dhcp::SocketReadError", "d8/d9d/classisc_1_1dhcp_1_1SocketReadError.html", "d8/d9d/classisc_1_1dhcp_1_1SocketReadError" ],
    [ "isc::dhcp::SocketWriteError", "df/d16/classisc_1_1dhcp_1_1SocketWriteError.html", "df/d16/classisc_1_1dhcp_1_1SocketWriteError" ],
    [ "BoundAddresses", "d3/d53/iface__mgr_8h.html#a43b436e412436d806cccc8fde6260873", null ],
    [ "IfaceMgrErrorMsgCallback", "d3/d53/iface__mgr_8h.html#a77157abef00df4985e670a2fc889f6b3", null ],
    [ "IfaceMgrPtr", "d3/d53/iface__mgr_8h.html#a0f8c1485948f1b586cde5d010989cd43", null ],
    [ "IfacePtr", "d3/d53/iface__mgr_8h.html#a41f1851d1d97c5ae02b97d8b23842e68", null ]
];