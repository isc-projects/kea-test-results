var namespaceisc_1_1log_1_1internal =
[
    [ "BufferAppender", "da/d5b/classisc_1_1log_1_1internal_1_1BufferAppender.html", "da/d5b/classisc_1_1log_1_1internal_1_1BufferAppender" ],
    [ "LogBufferAddAfterFlush", "d7/d06/classisc_1_1log_1_1internal_1_1LogBufferAddAfterFlush.html", "d7/d06/classisc_1_1log_1_1internal_1_1LogBufferAddAfterFlush" ],
    [ "LevelAndEvent", "d3/dc1/namespaceisc_1_1log_1_1internal.html#a51414c7e66c813a1a2d15f816f874fe4", null ],
    [ "LogEventList", "d3/dc1/namespaceisc_1_1log_1_1internal.html#a67b311a8f6fc90f211d0375dbeab3ebb", null ],
    [ "LogEventPtr", "d3/dc1/namespaceisc_1_1log_1_1internal.html#a9dfe457e1a90cdbcc7d1774c9cefeeac", null ]
];