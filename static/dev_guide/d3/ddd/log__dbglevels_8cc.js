var log__dbglevels_8cc =
[
    [ "DBGLVL_COMMAND", "d3/ddd/log__dbglevels_8cc.html#a38a7c0dc9f67a2b741a500127debbd74", null ],
    [ "DBGLVL_COMMAND_DATA", "d3/ddd/log__dbglevels_8cc.html#af6c61f3defdeb029b3713babd2c3f8a6", null ],
    [ "DBGLVL_PKT_HANDLING", "d3/ddd/log__dbglevels_8cc.html#ae539f2cce7424c479e8021a1e0107f1d", null ],
    [ "DBGLVL_START_SHUT", "d3/ddd/log__dbglevels_8cc.html#a4bac1257e6d32c32df315e66ddb05ae3", null ],
    [ "DBGLVL_TRACE_BASIC", "d3/ddd/log__dbglevels_8cc.html#a0c989fce20e7a7026321f4ff823a9927", null ],
    [ "DBGLVL_TRACE_BASIC_DATA", "d3/ddd/log__dbglevels_8cc.html#aea2b3a001010feb6d51d304c0ed8251d", null ],
    [ "DBGLVL_TRACE_DETAIL", "d3/ddd/log__dbglevels_8cc.html#afc1bf379c9a877b463d668a1ffc073c9", null ],
    [ "DBGLVL_TRACE_DETAIL_DATA", "d3/ddd/log__dbglevels_8cc.html#ab84485d3b816a6e8bf96b777ff0e7903", null ],
    [ "DBGLVL_TRACE_MAX", "d3/ddd/log__dbglevels_8cc.html#a63558ef6a0c4a9414a37588e061663a6", null ],
    [ "DBGLVL_TRACE_TECHNICAL", "d3/ddd/log__dbglevels_8cc.html#a564f3604ec7e246c9e2fa3874da46106", null ],
    [ "DBGLVL_TRACE_TECHNICAL_DATA", "d3/ddd/log__dbglevels_8cc.html#a7295c916e706b8dbd0ba645172b29779", null ]
];