var namespaceisc_1_1dhcp__ddns =
[
    [ "D2Dhcid", "d0/d16/classisc_1_1dhcp__ddns_1_1D2Dhcid.html", "d0/d16/classisc_1_1dhcp__ddns_1_1D2Dhcid" ],
    [ "DhcidRdataComputeError", "db/d20/classisc_1_1dhcp__ddns_1_1DhcidRdataComputeError.html", "db/d20/classisc_1_1dhcp__ddns_1_1DhcidRdataComputeError" ],
    [ "NameChangeListener", "d2/dfe/classisc_1_1dhcp__ddns_1_1NameChangeListener.html", "d2/dfe/classisc_1_1dhcp__ddns_1_1NameChangeListener" ],
    [ "NameChangeRequest", "db/ddb/classisc_1_1dhcp__ddns_1_1NameChangeRequest.html", "db/ddb/classisc_1_1dhcp__ddns_1_1NameChangeRequest" ],
    [ "NameChangeSender", "df/d1c/classisc_1_1dhcp__ddns_1_1NameChangeSender.html", "df/d1c/classisc_1_1dhcp__ddns_1_1NameChangeSender" ],
    [ "NameChangeUDPListener", "d8/d5a/classisc_1_1dhcp__ddns_1_1NameChangeUDPListener.html", "d8/d5a/classisc_1_1dhcp__ddns_1_1NameChangeUDPListener" ],
    [ "NameChangeUDPSender", "d1/d29/classisc_1_1dhcp__ddns_1_1NameChangeUDPSender.html", "d1/d29/classisc_1_1dhcp__ddns_1_1NameChangeUDPSender" ],
    [ "NcrListenerError", "d5/d25/classisc_1_1dhcp__ddns_1_1NcrListenerError.html", "d5/d25/classisc_1_1dhcp__ddns_1_1NcrListenerError" ],
    [ "NcrListenerOpenError", "de/d8f/classisc_1_1dhcp__ddns_1_1NcrListenerOpenError.html", "de/d8f/classisc_1_1dhcp__ddns_1_1NcrListenerOpenError" ],
    [ "NcrListenerReceiveError", "d4/de0/classisc_1_1dhcp__ddns_1_1NcrListenerReceiveError.html", "d4/de0/classisc_1_1dhcp__ddns_1_1NcrListenerReceiveError" ],
    [ "NcrMessageError", "de/d6c/classisc_1_1dhcp__ddns_1_1NcrMessageError.html", "de/d6c/classisc_1_1dhcp__ddns_1_1NcrMessageError" ],
    [ "NcrSenderError", "df/d84/classisc_1_1dhcp__ddns_1_1NcrSenderError.html", "df/d84/classisc_1_1dhcp__ddns_1_1NcrSenderError" ],
    [ "NcrSenderOpenError", "d9/dcc/classisc_1_1dhcp__ddns_1_1NcrSenderOpenError.html", "d9/dcc/classisc_1_1dhcp__ddns_1_1NcrSenderOpenError" ],
    [ "NcrSenderQueueFull", "d7/d95/classisc_1_1dhcp__ddns_1_1NcrSenderQueueFull.html", "d7/d95/classisc_1_1dhcp__ddns_1_1NcrSenderQueueFull" ],
    [ "NcrSenderSendError", "df/d75/classisc_1_1dhcp__ddns_1_1NcrSenderSendError.html", "df/d75/classisc_1_1dhcp__ddns_1_1NcrSenderSendError" ],
    [ "NcrUDPError", "d0/db9/classisc_1_1dhcp__ddns_1_1NcrUDPError.html", "d0/db9/classisc_1_1dhcp__ddns_1_1NcrUDPError" ],
    [ "UDPCallback", "d1/d09/classisc_1_1dhcp__ddns_1_1UDPCallback.html", "d1/d09/classisc_1_1dhcp__ddns_1_1UDPCallback" ],
    [ "ElementMap", "d3/dde/namespaceisc_1_1dhcp__ddns.html#a9c3d3ce652d65f3398df2c2bf8ac71a1", null ],
    [ "NameChangeListenerPtr", "d3/dde/namespaceisc_1_1dhcp__ddns.html#a70560723da6ab6879d96e837ce5188f9", null ],
    [ "NameChangeRequestPtr", "d3/dde/namespaceisc_1_1dhcp__ddns.html#ac5f4308ca496ce92b428ea2189610f1f", null ],
    [ "NameChangeSenderPtr", "d3/dde/namespaceisc_1_1dhcp__ddns.html#ab389567d5f3bfb53e94ccb7c55572217", null ],
    [ "NameChangeUDPSocket", "d3/dde/namespaceisc_1_1dhcp__ddns.html#ab1f317c3561e507b6694dea9f09d64bd", null ],
    [ "RawBufferPtr", "d3/dde/namespaceisc_1_1dhcp__ddns.html#ad1c7f977aeffc6eac7f7315911265a2c", null ],
    [ "UDPCompletionHandler", "d3/dde/namespaceisc_1_1dhcp__ddns.html#a7a2d68c90a92e0b522e8d1e46c88e752", null ],
    [ "UDPEndpointPtr", "d3/dde/namespaceisc_1_1dhcp__ddns.html#ad299975d1eff05600d49203e0f385e77", null ],
    [ "ConflictResolutionMode", "d3/dde/namespaceisc_1_1dhcp__ddns.html#a61084b3a6c2942ac9e9dc5745d2279e4", [
      [ "CHECK_WITH_DHCID", "d3/dde/namespaceisc_1_1dhcp__ddns.html#a61084b3a6c2942ac9e9dc5745d2279e4a18051479dae5702ab66bf8f0c0fc4922", null ],
      [ "NO_CHECK_WITH_DHCID", "d3/dde/namespaceisc_1_1dhcp__ddns.html#a61084b3a6c2942ac9e9dc5745d2279e4a6490d1210378b7ce961215c4db3ab79e", null ],
      [ "CHECK_EXISTS_WITH_DHCID", "d3/dde/namespaceisc_1_1dhcp__ddns.html#a61084b3a6c2942ac9e9dc5745d2279e4a49c212afb702783eaac9f6364cea80da", null ],
      [ "NO_CHECK_WITHOUT_DHCID", "d3/dde/namespaceisc_1_1dhcp__ddns.html#a61084b3a6c2942ac9e9dc5745d2279e4a340a0a6b1994a3d37c6a6a6bd4b9b606", null ]
    ] ],
    [ "NameChangeFormat", "d3/dde/namespaceisc_1_1dhcp__ddns.html#a4321dbe1e9795dac659540585fd4665c", [
      [ "FMT_JSON", "d3/dde/namespaceisc_1_1dhcp__ddns.html#a4321dbe1e9795dac659540585fd4665ca79cf6aa4d4e01ead71c831db74f3c906", null ]
    ] ],
    [ "NameChangeProtocol", "d3/dde/namespaceisc_1_1dhcp__ddns.html#a7eaf9fc79304f2c7c303f08da6ec5dce", [
      [ "NCR_UDP", "d3/dde/namespaceisc_1_1dhcp__ddns.html#a7eaf9fc79304f2c7c303f08da6ec5dcea402a6288a659edcfc8873f808b5c00f6", null ],
      [ "NCR_TCP", "d3/dde/namespaceisc_1_1dhcp__ddns.html#a7eaf9fc79304f2c7c303f08da6ec5dcea3a4ca1baef96479f9cf90ce0bd30bccb", null ]
    ] ],
    [ "NameChangeStatus", "d3/dde/namespaceisc_1_1dhcp__ddns.html#a59671b5a2d0eba11011ea1ee009018d1", [
      [ "ST_NEW", "d3/dde/namespaceisc_1_1dhcp__ddns.html#a59671b5a2d0eba11011ea1ee009018d1ad38b709f7c2f2d7191f45a0ef8e9a5de", null ],
      [ "ST_PENDING", "d3/dde/namespaceisc_1_1dhcp__ddns.html#a59671b5a2d0eba11011ea1ee009018d1a0a05b94a9712b8c40083658898e5f1a3", null ],
      [ "ST_COMPLETED", "d3/dde/namespaceisc_1_1dhcp__ddns.html#a59671b5a2d0eba11011ea1ee009018d1a5464def1adc9a31577a338c7b179c96d", null ],
      [ "ST_FAILED", "d3/dde/namespaceisc_1_1dhcp__ddns.html#a59671b5a2d0eba11011ea1ee009018d1a60c11f07ab71ce007834a8e5590a933d", null ]
    ] ],
    [ "NameChangeType", "d3/dde/namespaceisc_1_1dhcp__ddns.html#ad748d4121eb47fcd12456bf5c510cac0", [
      [ "CHG_ADD", "d3/dde/namespaceisc_1_1dhcp__ddns.html#ad748d4121eb47fcd12456bf5c510cac0a06d3baade751891d46db2e6bee6f5193", null ],
      [ "CHG_REMOVE", "d3/dde/namespaceisc_1_1dhcp__ddns.html#ad748d4121eb47fcd12456bf5c510cac0a0e6ef52783c714b980704aab317cbad7", null ]
    ] ],
    [ "ConflictResolutionModeToString", "d3/dde/namespaceisc_1_1dhcp__ddns.html#ac1f701204280b084574de6f28c42f4a3", null ],
    [ "ncrFormatToString", "d3/dde/namespaceisc_1_1dhcp__ddns.html#acf934bebb232293aad63caed2c9aa592", null ],
    [ "ncrProtocolToString", "d3/dde/namespaceisc_1_1dhcp__ddns.html#a940a71481b7df31ef40d17b4599da7bf", null ],
    [ "operator<<", "d3/dde/namespaceisc_1_1dhcp__ddns.html#aa50120ee65b3cbe55ea81ff084e0936a", null ],
    [ "StringToConflictResolutionMode", "d3/dde/namespaceisc_1_1dhcp__ddns.html#ab3d4640036c6dd92a79158a12f82c2f6", null ],
    [ "stringToNcrFormat", "d3/dde/namespaceisc_1_1dhcp__ddns.html#aa699e6a47a4d9b3c71fe149cc0e7144b", null ],
    [ "stringToNcrProtocol", "d3/dde/namespaceisc_1_1dhcp__ddns.html#a29863aac5a4e0154c9c687f86bd9190c", null ],
    [ "DHCP_DDNS_INVALID_NCR", "d3/dde/namespaceisc_1_1dhcp__ddns.html#aca4c2110dc6a26a5071ec7fdb331e762", null ],
    [ "dhcp_ddns_logger", "d3/dde/namespaceisc_1_1dhcp__ddns.html#a266ad6dd641161eedec4fb5fb341ccf4", null ],
    [ "DHCP_DDNS_NCR_FLUSH_IO_ERROR", "d3/dde/namespaceisc_1_1dhcp__ddns.html#a6141c1e9dcc95237274b85363f4a7f96", null ],
    [ "DHCP_DDNS_NCR_LISTEN_CLOSE_ERROR", "d3/dde/namespaceisc_1_1dhcp__ddns.html#ad3d623440d914bbf3ad335efa6739347", null ],
    [ "DHCP_DDNS_NCR_RECV_NEXT_ERROR", "d3/dde/namespaceisc_1_1dhcp__ddns.html#afedd944cf53cc84d6da4bb45637f2e24", null ],
    [ "DHCP_DDNS_NCR_SEND_CLOSE_ERROR", "d3/dde/namespaceisc_1_1dhcp__ddns.html#a33264abec408f0b2d91132a080e7df8c", null ],
    [ "DHCP_DDNS_NCR_SEND_NEXT_ERROR", "d3/dde/namespaceisc_1_1dhcp__ddns.html#a42c839ed52cefa2b5262fcb5618b9cb6", null ],
    [ "DHCP_DDNS_NCR_UDP_CLEAR_READY_ERROR", "d3/dde/namespaceisc_1_1dhcp__ddns.html#a767892fc3e8bd66102214e45f2f12047", null ],
    [ "DHCP_DDNS_NCR_UDP_RECV_CANCELED", "d3/dde/namespaceisc_1_1dhcp__ddns.html#aa2a2bf94334ca91223ab15bda83c3704", null ],
    [ "DHCP_DDNS_NCR_UDP_RECV_ERROR", "d3/dde/namespaceisc_1_1dhcp__ddns.html#a11cbdd811a109743e2bcafc00c559cf6", null ],
    [ "DHCP_DDNS_NCR_UDP_SEND_CANCELED", "d3/dde/namespaceisc_1_1dhcp__ddns.html#a6bf435b61bdbc5315bf7da3f23ccea70", null ],
    [ "DHCP_DDNS_NCR_UDP_SEND_ERROR", "d3/dde/namespaceisc_1_1dhcp__ddns.html#a71e0d330d467694e7b92c6f3d5b4be8a", null ],
    [ "DHCP_DDNS_UDP_SENDER_WATCH_SOCKET_CLOSE_ERROR", "d3/dde/namespaceisc_1_1dhcp__ddns.html#af1dcedf7d2e1e230d4b36fe919176c8d", null ],
    [ "DHCP_DDNS_UNCAUGHT_NCR_RECV_HANDLER_ERROR", "d3/dde/namespaceisc_1_1dhcp__ddns.html#aa7038ac2181339a853595317a4641aa2", null ],
    [ "DHCP_DDNS_UNCAUGHT_NCR_SEND_HANDLER_ERROR", "d3/dde/namespaceisc_1_1dhcp__ddns.html#a9fa9cd317c808c7917f5b957079068b0", null ]
];