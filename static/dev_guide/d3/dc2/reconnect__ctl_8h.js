var reconnect__ctl_8h =
[
    [ "isc::util::ReconnectCtl", "da/da7/classisc_1_1util_1_1ReconnectCtl.html", "da/da7/classisc_1_1util_1_1ReconnectCtl" ],
    [ "ReconnectCtlPtr", "d3/dc2/reconnect__ctl_8h.html#afc572d6750f5f4367de8ffd8acc29e1b", null ],
    [ "OnFailAction", "d3/dc2/reconnect__ctl_8h.html#adc27b6348ed38b30187240913237d95e", [
      [ "STOP_RETRY_EXIT", "d3/dc2/reconnect__ctl_8h.html#adc27b6348ed38b30187240913237d95eaedb101fd25585bed31115629cd6db6d9", null ],
      [ "SERVE_RETRY_EXIT", "d3/dc2/reconnect__ctl_8h.html#adc27b6348ed38b30187240913237d95eab71c3bea53ef0bf3f1c2b2c9bef0529c", null ],
      [ "SERVE_RETRY_CONTINUE", "d3/dc2/reconnect__ctl_8h.html#adc27b6348ed38b30187240913237d95eae1332f6e2f453cd50b82ca953c4e5dfa", null ]
    ] ]
];