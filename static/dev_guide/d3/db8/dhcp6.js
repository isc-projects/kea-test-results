var dhcp6 =
[
    [ "Configuration Parsers in DHCPv6", "d3/db8/dhcp6.html#dhcpv6ConfigParser", null ],
    [ "Configuration Parser for DHCPv6 (bison)", "d3/db8/dhcp6.html#dhcpv6ConfigParserBison", null ],
    [ "Parsing Partial Configuration in DHCPv6", "d3/db8/dhcp6.html#dhcpv6ConfigSubParser", null ],
    [ "Configuration Files Inclusion", "d3/db8/dhcp6.html#dhcp6ParserIncludes", null ],
    [ "Avoiding syntactical conflicts in parsers", "d3/db8/dhcp6.html#dhcp6ParserConflicts", null ],
    [ "DHCPv6 Configuration Inheritance", "d3/db8/dhcp6.html#dhcpv6ConfigInherit", null ],
    [ "DHCPv6 Server Support for the Dynamic DNS Updates", "d3/db8/dhcp6.html#dhcpv6DDNSIntegration", null ],
    [ "Custom functions to parse message options", "d3/db8/dhcp6.html#dhcpv6OptionsParse", null ],
    [ "DHCPv6 Client Classification", "d3/db8/dhcp6.html#dhcpv6Classifier", [
      [ "Simple Client Classification in DHCPv6", "d3/db8/dhcp6.html#dhcpv6ClassifierSimple", null ],
      [ "Full Client Classification in DHCPv6", "d3/db8/dhcp6.html#dhcpv6ClassifierFull", null ],
      [ "How client classification information is used in DHCPv6", "d3/db8/dhcp6.html#dhcpv6ClassifierUsage", null ]
    ] ],
    [ "Configuration backend for DHCPv6", "d3/db8/dhcp6.html#dhcpv6ConfigBackend", null ],
    [ "Reconfiguring DHCPv6 server with SIGHUP signal", "d3/db8/dhcp6.html#dhcpv6SignalBasedReconfiguration", null ],
    [ "Other DHCPv6 topics", "d3/db8/dhcp6.html#dhcpv6Other", null ],
    [ "DHCPv4-over-DHCPv6 DHCPv6 Server Side", "dc/d30/dhcpv4o6Dhcp6.html", [
      [ "DHCPv6-to-DHCPv4 Inter Process Communication", "dc/d30/dhcpv4o6Dhcp6.html#dhcp6to4Ipc", null ],
      [ "DHCPv6-to-DHCPv4 Packet Processing", "dc/d30/dhcpv4o6Dhcp6.html#dhcp6to4Process", null ]
    ] ]
];