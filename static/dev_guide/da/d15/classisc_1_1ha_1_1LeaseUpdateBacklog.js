var classisc_1_1ha_1_1LeaseUpdateBacklog =
[
    [ "OpType", "da/d15/classisc_1_1ha_1_1LeaseUpdateBacklog.html#a72b0b2ba270a3d1d444b8fd72d66a2c8", [
      [ "ADD", "da/d15/classisc_1_1ha_1_1LeaseUpdateBacklog.html#a72b0b2ba270a3d1d444b8fd72d66a2c8ae81e155a94eb8f33b36c64286feef999", null ],
      [ "DELETE", "da/d15/classisc_1_1ha_1_1LeaseUpdateBacklog.html#a72b0b2ba270a3d1d444b8fd72d66a2c8ac6017ebe97ac3c046f35a4169a4d9b2b", null ]
    ] ],
    [ "LeaseUpdateBacklog", "da/d15/classisc_1_1ha_1_1LeaseUpdateBacklog.html#af58148cd48d9d6dec331d2210aad18a9", null ],
    [ "clear", "da/d15/classisc_1_1ha_1_1LeaseUpdateBacklog.html#a81d1f35937907db004dfe8ca4b75eda4", null ],
    [ "pop", "da/d15/classisc_1_1ha_1_1LeaseUpdateBacklog.html#a196c8c5a23bab82ca2436077185efea1", null ],
    [ "push", "da/d15/classisc_1_1ha_1_1LeaseUpdateBacklog.html#a668ae6fc94097d998e53ae8d5b0f9d35", null ],
    [ "size", "da/d15/classisc_1_1ha_1_1LeaseUpdateBacklog.html#a22bcc4c480589259cff54c6746a1d0a0", null ],
    [ "wasOverflown", "da/d15/classisc_1_1ha_1_1LeaseUpdateBacklog.html#a86cdaa182b047fabdfaa8d7cf8d64df9", null ]
];