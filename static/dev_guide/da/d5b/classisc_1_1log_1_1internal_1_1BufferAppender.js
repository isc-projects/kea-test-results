var classisc_1_1log_1_1internal_1_1BufferAppender =
[
    [ "BufferAppender", "da/d5b/classisc_1_1log_1_1internal_1_1BufferAppender.html#ade3b155a45edf0f37da0dcbe71d6c629", null ],
    [ "~BufferAppender", "da/d5b/classisc_1_1log_1_1internal_1_1BufferAppender.html#a91739c5ad9afffcb3888bfdc6b7e9766", null ],
    [ "append", "da/d5b/classisc_1_1log_1_1internal_1_1BufferAppender.html#a9dbd1fa55817b5f97370cba796e90f86", null ],
    [ "close", "da/d5b/classisc_1_1log_1_1internal_1_1BufferAppender.html#ad1af437c0221bf20957320835caf2b7b", null ],
    [ "flush", "da/d5b/classisc_1_1log_1_1internal_1_1BufferAppender.html#aafca5793781af874eb7080d949e25758", null ],
    [ "getBufferSize", "da/d5b/classisc_1_1log_1_1internal_1_1BufferAppender.html#a55c06df27dd748dc190a25e848777fce", null ]
];