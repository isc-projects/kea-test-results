var communication__state_8h =
[
    [ "isc::ha::CommunicationState", "dd/d15/classisc_1_1ha_1_1CommunicationState.html", "dd/d15/classisc_1_1ha_1_1CommunicationState" ],
    [ "isc::ha::CommunicationState4", "d0/d05/classisc_1_1ha_1_1CommunicationState4.html", "d0/d05/classisc_1_1ha_1_1CommunicationState4" ],
    [ "isc::ha::CommunicationState6", "d2/d0d/classisc_1_1ha_1_1CommunicationState6.html", "d2/d0d/classisc_1_1ha_1_1CommunicationState6" ],
    [ "isc::ha::CommunicationState4::ConnectingClient4", "d3/d7f/structisc_1_1ha_1_1CommunicationState4_1_1ConnectingClient4.html", "d3/d7f/structisc_1_1ha_1_1CommunicationState4_1_1ConnectingClient4" ],
    [ "isc::ha::CommunicationState6::ConnectingClient6", "dc/d2f/structisc_1_1ha_1_1CommunicationState6_1_1ConnectingClient6.html", "dc/d2f/structisc_1_1ha_1_1CommunicationState6_1_1ConnectingClient6" ],
    [ "isc::ha::CommunicationState4::RejectedClient4", "db/dc8/structisc_1_1ha_1_1CommunicationState4_1_1RejectedClient4.html", "db/dc8/structisc_1_1ha_1_1CommunicationState4_1_1RejectedClient4" ],
    [ "isc::ha::CommunicationState6::RejectedClient6", "df/d6e/structisc_1_1ha_1_1CommunicationState6_1_1RejectedClient6.html", "df/d6e/structisc_1_1ha_1_1CommunicationState6_1_1RejectedClient6" ],
    [ "CommunicationState4Ptr", "da/d2a/communication__state_8h.html#a7fb058d00356663e58126dbba83cfb47", null ],
    [ "CommunicationState6Ptr", "da/d2a/communication__state_8h.html#aeb8ed8d74c6f9e23d1c069bb39d5ef8d", null ],
    [ "CommunicationStatePtr", "da/d2a/communication__state_8h.html#a1176a2bd1584f31f2020793d4936f4d6", null ]
];