var ncr__udp_8h =
[
    [ "isc::dhcp_ddns::UDPCallback::Data", "d4/d5d/structisc_1_1dhcp__ddns_1_1UDPCallback_1_1Data.html", "d4/d5d/structisc_1_1dhcp__ddns_1_1UDPCallback_1_1Data" ],
    [ "isc::dhcp_ddns::NameChangeUDPListener", "d8/d5a/classisc_1_1dhcp__ddns_1_1NameChangeUDPListener.html", "d8/d5a/classisc_1_1dhcp__ddns_1_1NameChangeUDPListener" ],
    [ "isc::dhcp_ddns::NameChangeUDPSender", "d1/d29/classisc_1_1dhcp__ddns_1_1NameChangeUDPSender.html", "d1/d29/classisc_1_1dhcp__ddns_1_1NameChangeUDPSender" ],
    [ "isc::dhcp_ddns::NcrUDPError", "d0/db9/classisc_1_1dhcp__ddns_1_1NcrUDPError.html", "d0/db9/classisc_1_1dhcp__ddns_1_1NcrUDPError" ],
    [ "isc::dhcp_ddns::UDPCallback", "d1/d09/classisc_1_1dhcp__ddns_1_1UDPCallback.html", "d1/d09/classisc_1_1dhcp__ddns_1_1UDPCallback" ],
    [ "NameChangeUDPSocket", "da/df3/ncr__udp_8h.html#ab1f317c3561e507b6694dea9f09d64bd", null ],
    [ "RawBufferPtr", "da/df3/ncr__udp_8h.html#ad1c7f977aeffc6eac7f7315911265a2c", null ],
    [ "UDPCompletionHandler", "da/df3/ncr__udp_8h.html#a7a2d68c90a92e0b522e8d1e46c88e752", null ],
    [ "UDPEndpointPtr", "da/df3/ncr__udp_8h.html#ad299975d1eff05600d49203e0f385e77", null ]
];