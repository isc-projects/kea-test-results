var ncr__msg_8h =
[
    [ "isc::dhcp_ddns::D2Dhcid", "d0/d16/classisc_1_1dhcp__ddns_1_1D2Dhcid.html", "d0/d16/classisc_1_1dhcp__ddns_1_1D2Dhcid" ],
    [ "isc::dhcp_ddns::DhcidRdataComputeError", "db/d20/classisc_1_1dhcp__ddns_1_1DhcidRdataComputeError.html", "db/d20/classisc_1_1dhcp__ddns_1_1DhcidRdataComputeError" ],
    [ "isc::dhcp_ddns::NameChangeRequest", "db/ddb/classisc_1_1dhcp__ddns_1_1NameChangeRequest.html", "db/ddb/classisc_1_1dhcp__ddns_1_1NameChangeRequest" ],
    [ "isc::dhcp_ddns::NcrMessageError", "de/d6c/classisc_1_1dhcp__ddns_1_1NcrMessageError.html", "de/d6c/classisc_1_1dhcp__ddns_1_1NcrMessageError" ],
    [ "ElementMap", "da/df2/ncr__msg_8h.html#a9c3d3ce652d65f3398df2c2bf8ac71a1", null ],
    [ "NameChangeRequestPtr", "da/df2/ncr__msg_8h.html#ac5f4308ca496ce92b428ea2189610f1f", null ],
    [ "ConflictResolutionMode", "da/df2/ncr__msg_8h.html#a61084b3a6c2942ac9e9dc5745d2279e4", [
      [ "CHECK_WITH_DHCID", "da/df2/ncr__msg_8h.html#a61084b3a6c2942ac9e9dc5745d2279e4a18051479dae5702ab66bf8f0c0fc4922", null ],
      [ "NO_CHECK_WITH_DHCID", "da/df2/ncr__msg_8h.html#a61084b3a6c2942ac9e9dc5745d2279e4a6490d1210378b7ce961215c4db3ab79e", null ],
      [ "CHECK_EXISTS_WITH_DHCID", "da/df2/ncr__msg_8h.html#a61084b3a6c2942ac9e9dc5745d2279e4a49c212afb702783eaac9f6364cea80da", null ],
      [ "NO_CHECK_WITHOUT_DHCID", "da/df2/ncr__msg_8h.html#a61084b3a6c2942ac9e9dc5745d2279e4a340a0a6b1994a3d37c6a6a6bd4b9b606", null ]
    ] ],
    [ "NameChangeFormat", "da/df2/ncr__msg_8h.html#a4321dbe1e9795dac659540585fd4665c", [
      [ "FMT_JSON", "da/df2/ncr__msg_8h.html#a4321dbe1e9795dac659540585fd4665ca79cf6aa4d4e01ead71c831db74f3c906", null ]
    ] ],
    [ "NameChangeStatus", "da/df2/ncr__msg_8h.html#a59671b5a2d0eba11011ea1ee009018d1", [
      [ "ST_NEW", "da/df2/ncr__msg_8h.html#a59671b5a2d0eba11011ea1ee009018d1ad38b709f7c2f2d7191f45a0ef8e9a5de", null ],
      [ "ST_PENDING", "da/df2/ncr__msg_8h.html#a59671b5a2d0eba11011ea1ee009018d1a0a05b94a9712b8c40083658898e5f1a3", null ],
      [ "ST_COMPLETED", "da/df2/ncr__msg_8h.html#a59671b5a2d0eba11011ea1ee009018d1a5464def1adc9a31577a338c7b179c96d", null ],
      [ "ST_FAILED", "da/df2/ncr__msg_8h.html#a59671b5a2d0eba11011ea1ee009018d1a60c11f07ab71ce007834a8e5590a933d", null ]
    ] ],
    [ "NameChangeType", "da/df2/ncr__msg_8h.html#ad748d4121eb47fcd12456bf5c510cac0", [
      [ "CHG_ADD", "da/df2/ncr__msg_8h.html#ad748d4121eb47fcd12456bf5c510cac0a06d3baade751891d46db2e6bee6f5193", null ],
      [ "CHG_REMOVE", "da/df2/ncr__msg_8h.html#ad748d4121eb47fcd12456bf5c510cac0a0e6ef52783c714b980704aab317cbad7", null ]
    ] ],
    [ "ConflictResolutionModeToString", "da/df2/ncr__msg_8h.html#ac1f701204280b084574de6f28c42f4a3", null ],
    [ "ncrFormatToString", "da/df2/ncr__msg_8h.html#acf934bebb232293aad63caed2c9aa592", null ],
    [ "operator<<", "da/df2/ncr__msg_8h.html#aa50120ee65b3cbe55ea81ff084e0936a", null ],
    [ "StringToConflictResolutionMode", "da/df2/ncr__msg_8h.html#ab3d4640036c6dd92a79158a12f82c2f6", null ],
    [ "stringToNcrFormat", "da/df2/ncr__msg_8h.html#aa699e6a47a4d9b3c71fe149cc0e7144b", null ]
];