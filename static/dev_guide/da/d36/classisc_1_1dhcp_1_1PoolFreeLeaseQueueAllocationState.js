var classisc_1_1dhcp_1_1PoolFreeLeaseQueueAllocationState =
[
    [ "PoolFreeLeaseQueueAllocationState", "da/d36/classisc_1_1dhcp_1_1PoolFreeLeaseQueueAllocationState.html#a9d7744fa647d10ef9b80f412cbaf2fda", null ],
    [ "addFreeLease", "da/d36/classisc_1_1dhcp_1_1PoolFreeLeaseQueueAllocationState.html#ab0ca04c87724244318400476d44d0855", null ],
    [ "deleteFreeLease", "da/d36/classisc_1_1dhcp_1_1PoolFreeLeaseQueueAllocationState.html#aaa7084894901f2e3973636fd2b73009c", null ],
    [ "exhausted", "da/d36/classisc_1_1dhcp_1_1PoolFreeLeaseQueueAllocationState.html#aa3b7c1dc545584f1a4f499023be5ebdc", null ],
    [ "getFreeLeaseCount", "da/d36/classisc_1_1dhcp_1_1PoolFreeLeaseQueueAllocationState.html#af880f86db13974ae7c06c13c44efbeaa", null ],
    [ "offerFreeLease", "da/d36/classisc_1_1dhcp_1_1PoolFreeLeaseQueueAllocationState.html#a4ac9b19652b800e6501225057d44dfe1", null ]
];