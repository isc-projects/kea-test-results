var classisc_1_1dhcp_1_1PoolIterativeAllocationState =
[
    [ "PoolIterativeAllocationState", "da/de9/classisc_1_1dhcp_1_1PoolIterativeAllocationState.html#ac2434e29685c228a0c34bb4be9772484", null ],
    [ "getLastAllocated", "da/de9/classisc_1_1dhcp_1_1PoolIterativeAllocationState.html#a4ecd3e2ae489eae823504f6dc89ed62e", null ],
    [ "isLastAllocatedValid", "da/de9/classisc_1_1dhcp_1_1PoolIterativeAllocationState.html#a77f5ab788705d28411c058bd5bab7052", null ],
    [ "resetLastAllocated", "da/de9/classisc_1_1dhcp_1_1PoolIterativeAllocationState.html#a0827532cc7045e808b31992b295eb8bf", null ],
    [ "setLastAllocated", "da/de9/classisc_1_1dhcp_1_1PoolIterativeAllocationState.html#a2b9c05384d8e40310adad61d297444d6", null ]
];