var audit__entry_8h =
[
    [ "isc::db::AuditEntry", "d8/d73/classisc_1_1db_1_1AuditEntry.html", "d8/d73/classisc_1_1db_1_1AuditEntry" ],
    [ "isc::db::AuditEntryModificationTimeIdTag", "d1/d56/structisc_1_1db_1_1AuditEntryModificationTimeIdTag.html", null ],
    [ "isc::db::AuditEntryObjectIdTag", "df/d87/structisc_1_1db_1_1AuditEntryObjectIdTag.html", null ],
    [ "isc::db::AuditEntryObjectTypeTag", "d3/dfa/structisc_1_1db_1_1AuditEntryObjectTypeTag.html", null ],
    [ "AuditEntryCollection", "da/d76/audit__entry_8h.html#a71dbfd905fc4867b73df320fd59f2890", null ],
    [ "AuditEntryCollectionPtr", "da/d76/audit__entry_8h.html#aab59951fa7d6818d648ff3df79b8e703", null ],
    [ "AuditEntryPtr", "da/d76/audit__entry_8h.html#a4db9c7e356113edacb535fc9c57f7f7e", null ]
];