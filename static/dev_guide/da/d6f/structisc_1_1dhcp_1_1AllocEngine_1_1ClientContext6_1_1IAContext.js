var structisc_1_1dhcp_1_1AllocEngine_1_1ClientContext6_1_1IAContext =
[
    [ "IAContext", "da/d6f/structisc_1_1dhcp_1_1AllocEngine_1_1ClientContext6_1_1IAContext.html#a001cb45aa13d0d0630a32ac30e858a0d", null ],
    [ "addHint", "da/d6f/structisc_1_1dhcp_1_1AllocEngine_1_1ClientContext6_1_1IAContext.html#abc9b2a2dafd0c5cd313b11ad31c7e772", null ],
    [ "addHint", "da/d6f/structisc_1_1dhcp_1_1AllocEngine_1_1ClientContext6_1_1IAContext.html#a1c2805af3bc8b593841d9ed20ef6bf52", null ],
    [ "addHint", "da/d6f/structisc_1_1dhcp_1_1AllocEngine_1_1ClientContext6_1_1IAContext.html#ae6b966171f57fe9c5d8388b52ea7746e", null ],
    [ "addNewResource", "da/d6f/structisc_1_1dhcp_1_1AllocEngine_1_1ClientContext6_1_1IAContext.html#ac5f2b3862fffbe9e219693401a2894fa", null ],
    [ "isNewResource", "da/d6f/structisc_1_1dhcp_1_1AllocEngine_1_1ClientContext6_1_1IAContext.html#a78b03c6d1167293f594993e086702ee6", null ],
    [ "changed_leases_", "da/d6f/structisc_1_1dhcp_1_1AllocEngine_1_1ClientContext6_1_1IAContext.html#a8450cc392a7f7e451053a0b65912dcc7", null ],
    [ "hints_", "da/d6f/structisc_1_1dhcp_1_1AllocEngine_1_1ClientContext6_1_1IAContext.html#ad478db89d6a53af98f844f30692c14ab", null ],
    [ "ia_rsp_", "da/d6f/structisc_1_1dhcp_1_1AllocEngine_1_1ClientContext6_1_1IAContext.html#a0e0f364478cf340c1e2d480b14d34fde", null ],
    [ "iaid_", "da/d6f/structisc_1_1dhcp_1_1AllocEngine_1_1ClientContext6_1_1IAContext.html#adfdb040454b3df6c656e7239be692df5", null ],
    [ "new_resources_", "da/d6f/structisc_1_1dhcp_1_1AllocEngine_1_1ClientContext6_1_1IAContext.html#a307a0ad6dcf175296243e09f28280da8", null ],
    [ "old_leases_", "da/d6f/structisc_1_1dhcp_1_1AllocEngine_1_1ClientContext6_1_1IAContext.html#a024c561a5f7d42e3fdd66899be7c4bfb", null ],
    [ "reused_leases_", "da/d6f/structisc_1_1dhcp_1_1AllocEngine_1_1ClientContext6_1_1IAContext.html#ac6e1fe577b02504a119a0549a593cf91", null ],
    [ "type_", "da/d6f/structisc_1_1dhcp_1_1AllocEngine_1_1ClientContext6_1_1IAContext.html#a1a6cd65850334316c7faf501a4a183f9", null ]
];