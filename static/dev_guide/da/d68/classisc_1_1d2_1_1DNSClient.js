var classisc_1_1d2_1_1DNSClient =
[
    [ "Callback", "d8/d3e/classisc_1_1d2_1_1DNSClient_1_1Callback.html", "d8/d3e/classisc_1_1d2_1_1DNSClient_1_1Callback" ],
    [ "Protocol", "da/d68/classisc_1_1d2_1_1DNSClient.html#af8e5960bf71f2d19f04518f6cd7ce598", [
      [ "UDP", "da/d68/classisc_1_1d2_1_1DNSClient.html#af8e5960bf71f2d19f04518f6cd7ce598aeea8f456d6ad8824c35ecc57e969ec52", null ],
      [ "TCP", "da/d68/classisc_1_1d2_1_1DNSClient.html#af8e5960bf71f2d19f04518f6cd7ce598a3aa8ca0c69abee4459d34758e5516235", null ]
    ] ],
    [ "Status", "da/d68/classisc_1_1d2_1_1DNSClient.html#acb7342d4ceca436580de3cf5e0a0db3c", [
      [ "SUCCESS", "da/d68/classisc_1_1d2_1_1DNSClient.html#acb7342d4ceca436580de3cf5e0a0db3cad0328b449f239f88f74b899acb61c943", null ],
      [ "TIMEOUT", "da/d68/classisc_1_1d2_1_1DNSClient.html#acb7342d4ceca436580de3cf5e0a0db3ca690a32ba65bf3868197869d99729d014", null ],
      [ "IO_STOPPED", "da/d68/classisc_1_1d2_1_1DNSClient.html#acb7342d4ceca436580de3cf5e0a0db3ca52ae8e50863bbf614f7b96fbbb6eaf7b", null ],
      [ "INVALID_RESPONSE", "da/d68/classisc_1_1d2_1_1DNSClient.html#acb7342d4ceca436580de3cf5e0a0db3cac7ada20005b5bd664fb86b52e0e30b7c", null ],
      [ "OTHER", "da/d68/classisc_1_1d2_1_1DNSClient.html#acb7342d4ceca436580de3cf5e0a0db3ca8ec11966ec20555cf832eaddb23294ae", null ]
    ] ],
    [ "DNSClient", "da/d68/classisc_1_1d2_1_1DNSClient.html#a49ad1454312d39d2faaacbdbc589492d", null ],
    [ "~DNSClient", "da/d68/classisc_1_1d2_1_1DNSClient.html#a1170223f9d0484afe2d82cda6b23afa3", null ],
    [ "doUpdate", "da/d68/classisc_1_1d2_1_1DNSClient.html#a095ad352ba4d2501dc26627f26753db2", null ],
    [ "stop", "da/d68/classisc_1_1d2_1_1DNSClient.html#a555c5f84aac9b2d9fedd3ea2ae770a9a", null ]
];