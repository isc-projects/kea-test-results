#!/bin/sh

set -eu

origin=$(git remote -v | grep -E '^origin' | head -n 1 | cut -f 2 | cut -d ' ' -f 1)
this_branch=$(git rev-parse --abbrev-ref HEAD)

git remote update
if test -z "$(git diff origin/${this_branch})"; then
  printf 'No changes relative to remote branch which is good.\n'
  printf 'If you get errors during push, it might be that Jenkins or someone else pushed something so you need to "git reset --hard origin/%s" and try again.\n' "${this_branch}"
else
  printf 'Changes found relative to remote branch. Either this script messed something up, or someone else pushed to this repository. You can check with "git diff --name-only origin/%s".\n' "${this_branch}"
fi

rm -rf static/dev_guide_bak static/tests_status_bak
cp -r static/dev_guide static/dev_guide_bak
cp -r static/tests_status static/tests_status_bak
git filter-repo --force --invert-paths --path static/dev_guide --path static/tests_status
git remote add origin "${origin}"
git remote update
mv static/dev_guide_bak static/dev_guide
mv static/tests_status_bak static/tests_status
git add static/dev_guide static/tests_status
git commit --author 'Jenkins <jenkins@isc.org>' --message 'Cleaned up git history.'
git branch --set-upstream-to="origin/${this_branch}"

printf 'Check that there are no changes with "git diff --name-only origin/%s".\n' "${this_branch}"
printf 'If all is well, you can now run "git push --force origin/%s".\n' "${this_branch}"
